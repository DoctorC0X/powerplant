﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Scenes 
{
    public static KeyValuePair<int, string> splash              = new KeyValuePair<int, string>(0, "0_Splash");
    public static KeyValuePair<int, string> intro               = new KeyValuePair<int, string>(1, "1_Intro");
    public static KeyValuePair<int, string> loading             = new KeyValuePair<int, string>(2, "2_Loading");
    public static KeyValuePair<int, string> main                = new KeyValuePair<int, string>(3, "3_Main");
    public static KeyValuePair<int, string> shop                = new KeyValuePair<int, string>(4, "4_Shop");
    public static KeyValuePair<int, string> progressInit        = new KeyValuePair<int, string>(5, "ProgressInit");
}
