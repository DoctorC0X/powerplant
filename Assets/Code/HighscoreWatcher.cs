﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using EventSystem;

public class HighscoreWatcher : MonoBehaviour
{
    public float startHeight;
    public float currentHeight;
    public float lastHeight;

    public Sprite hsMarkSprite;
    public SpriteRenderer hsMarkRenderer;

    public GameObject hsMarkGo;

    public float heighCheckDelay;
    private float heightCheckTimer;

    public void Init()
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;

        startHeight = transform.position.y;
        heightCheckTimer = heighCheckDelay;

        currentHeight = 0;
        lastHeight = 0;

        PlantModel.MainPlant.OnPlantBroke += HeightCheck;

        /*
        if (heightRound.Record > 0)
        {
            hsMarkGo = new GameObject("Best------------------");
            hsMarkRenderer = hsMarkGo.AddComponent<SpriteRenderer>();
            hsMarkRenderer.sprite = hsMarkSprite;
            hsMarkGo.transform.position = new Vector3(this.transform.position.x, heightRound.Record + startHeight, this.transform.position.z);
        } 
        */
    }

	// Update is called once per frame
	void Update () 
    {
        if (!GameManager.Instance.paused && GameManager.Instance.gameActive && !PlantModel.MainPlant.Broken)
        {
            heightCheckTimer -= Time.deltaTime;

            currentHeight = transform.position.y - startHeight;

            if (heightCheckTimer <= 0)
            {
                heightCheckTimer = heighCheckDelay;

                HeightCheck();
            }
        }
	}

    private void HeightCheck()
    {
        int difference = Mathf.RoundToInt(currentHeight - lastHeight);

        if (difference > 0)
        {
            EventManager.Instance.CallEvent(EventData.ID.HEIGHT, new EventData.ParamBase() { increase = difference });

            lastHeight = currentHeight;
        }
    }
}
