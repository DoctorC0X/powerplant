﻿using UnityEngine;
using System.Collections;

public interface GameOverListener
{
    void GameOver();
}
