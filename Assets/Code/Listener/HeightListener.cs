﻿using UnityEngine;
using System.Collections;

public interface HeightListener 
{
    void HeightReached(float height);
}
