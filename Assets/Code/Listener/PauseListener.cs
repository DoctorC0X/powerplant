﻿using UnityEngine;
using System.Collections;

public interface PauseListener
{
    void SetPause(bool pause);

    bool IsPaused();
}
