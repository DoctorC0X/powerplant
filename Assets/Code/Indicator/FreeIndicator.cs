﻿using UnityEngine;
using System.Collections;

public class FreeIndicator : MonoBehaviour 
{
    public Transform target;

    public GameObject pointer;
    public float pointerHeight;

    void Start()
    {
        if (pointer == null)
        {
            pointer = transform.GetChild(0).gameObject;

            pointerHeight = pointer.transform.localPosition.y;
        }
    }

	// Update is called once per frame
	void Update () 
    {
        if (target != null)
        {
            Vector3 dir = target.position - transform.position;
            transform.up = new Vector3(dir.x, dir.y, 0);

            if (dir.sqrMagnitude > (pointerHeight * pointerHeight))
                pointer.transform.localPosition = new Vector3(pointer.transform.localPosition.x, pointerHeight, pointer.transform.localPosition.z);
            else
                pointer.transform.localPosition = new Vector3(pointer.transform.localPosition.x, Mathf.Clamp(dir.magnitude, 0, pointerHeight), pointer.transform.localPosition.z);
        }
	}
}
