﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using EventSystem;

public class IndicatorManager : MonoBehaviour 
{
    private static IndicatorManager _instance;

    public static IndicatorManager Instance
    {
        get { return _instance; }
    }

    public bool _show;

    // PlantFace is calling if its out of every camera frustum
    public bool Show
    {
        get { return _show; }
        set
        {
            if (_show != value)
            {
                _show = value && Activated;

                if (indicator != null)
                    indicator.SetActive(_show);
            }
        }
    }

    public bool _activated;

    public bool Activated
    {
        get { return _activated; }
        set
        {
            _activated = value;

            if (!_activated)
                Show = false;
        }
    }

    public PlantFace face;

    public GameObject indicator;

    public GameObject indicatorFrame;
    public Image indicatorPic;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            DestroyImmediate(this);
        }
        else
        {
            _instance = this;
        }
    }

	// Use this for initialization
	void Start () 
    {
        Activated = false;

        EventManager.Instance.AddHandler(EventData.ID.GAMESTART, () => Activated = true);

        face = PlantModel.MainPlant.plantFace;

        face.visibility.OnVisibilityChange += (bool visible) => Show = !visible;

        indicatorPic.sprite = face.sRenderer.sprite;
	}
	
	void Update () 
    {
        if (Show)
        {
            // get vector from camera to face
            Vector3 dir = (face.transform.position - Camera.main.transform.position).normalized;

            // transform it to canvas system
            dir = transform.InverseTransformDirection(dir);

            Rect r = GetComponent<RectTransform>().rect;

            indicator.transform.localPosition = new Vector3(dir.x * (r.width / 2), dir.y * (r.height / 2), indicator.transform.localPosition.z);

            dir = new Vector3(dir.x, dir.y, 0).normalized;

            // make the indicator point at face
            indicatorFrame.transform.up = dir;

            // the face icon rotation should be the same as the original face
            indicatorPic.transform.rotation = face.transform.rotation;
        }
	}
}
