﻿using UnityEngine;
using Progress.Missions;
using EventSystem;
using System;

public class PlayerProfile : MonoBehaviour
{
    private static PlayerProfile _instance;

    public static PlayerProfile Instance
    {
        get { return _instance; }
    }

    public enum Plant { BLOMSTER = 0 };
    public Plant currentPlant;

    private int StarDust
    {
        get { return ((PlayerProfileData)PersistenceManager.Instance.GetData(gameObject.name)).starDust; }
        set { ((PlayerProfileData)PersistenceManager.Instance.GetData(gameObject.name)).starDust = value; }
    }

    private bool MasterSkillUnlocked
    {
        get { return ((PlayerProfileData)PersistenceManager.Instance.GetData(gameObject.name)).masterSkillUnlocked; }
        set { ((PlayerProfileData)PersistenceManager.Instance.GetData(gameObject.name)).masterSkillUnlocked = value; }
    }

    public event Action<int, int> OnStarDustBalanceChange;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
            return;
        }
        else
        {
            _instance = this;
        }

        if (PersistenceManager.Instance != null && !PersistenceManager.Instance.IsRegistered(gameObject.name))
            PersistenceManager.Instance.SetData(gameObject.name, new PlayerProfileData());
    }

    void Start()
    {
        if (EventManager.Instance != null)
        {
            EventManager.Instance.AddHandler(EventData.ID.MISSION_CLAIMED, (EventData.ParamBase b) =>
                {
                    Mission mission = ((EventData.ParamMission)b).mission;

                    DepositStarDust(mission.GetReward());
                });
        }
    }

    public int GetStarDustBalance()
    {
        return StarDust;
    }

    public void DepositStarDust(int amount)
    {
        amount = Mathf.Clamp(amount, 0, int.MaxValue);

        StarDust += amount;

        if (OnStarDustBalanceChange != null)
            OnStarDustBalanceChange(StarDust - amount, StarDust);
    }

    public bool DebitStarDust(int amount)
    {
        amount = Mathf.Clamp(amount, 0, int.MaxValue);

        if (StarDust - amount >= 0)
        {
            StarDust -= amount;

            if (OnStarDustBalanceChange != null)
                OnStarDustBalanceChange(StarDust + amount, StarDust);

            return true;
        }

        return false;
    }

    public bool IsMasterSkillUnlocked()
    {
        return MasterSkillUnlocked;
    }

    public void UnlockMasterSkill()
    {
        MasterSkillUnlocked = true;
        PlantModel.MainPlant.skillManager.CheckUnlock();
    }
}

[Serializable]
public class PlayerProfileData : PersistentData
{
    public int starDust;
    public bool masterSkillUnlocked;

    public PlayerProfileData()
    {
        starDust = 0;
        masterSkillUnlocked = false;
    }
}