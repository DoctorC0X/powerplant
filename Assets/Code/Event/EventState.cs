﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml.Serialization;
using System.Xml;
using Progress.IO;

namespace EventSystem
{
    public class EventState : EventReceiver
    {
        public EventSubState[] substates;

        public enum ActivateType { AND, OR };
        public ActivateType type;

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set
            {
                if (_active != value)
                {
                    _active = value;

                    if (OnStateActiveChange != null)
                        OnStateActiveChange(value);
                }
            }
        }

        public Action<bool> OnStateActiveChange;

        public EventState(EventSubState[] substates)
        {
            this.substates = substates;
        }

        public void Init()
        {
            for (int i = 0; i < substates.Length; i++)
            {
                substates[i].Init();
                substates[i].OnStateActiveChange += (bool b) => CheckStateActive();
            }

            if (substates.Length <= 0)
                Active = true;
        }

        private void CheckStateActive()
        {
            switch (type)
            {
                case ActivateType.AND:
                    {
                        for (int i = 0; i < substates.Length; i++)
                        {
                            if (!substates[i].Active)
                            {
                                Active = false;
                                return;
                            }
                        }

                        Active = true;
                        break;
                    }

                case ActivateType.OR:
                    {
                        for (int i = 0; i < substates.Length; i++)
                        {
                            if (substates[i].Active)
                            {
                                Active = true;
                                return;
                            }
                        }

                        Active = false;
                        break;
                    }
            }
        }

        public void Register()
        {
            for (int i = 0; i < substates.Length; i++)
                substates[i].Register();
        }

        public void Deregister()
        {
            for (int i = 0; i < substates.Length; i++)
                substates[i].Deregister();
        }
    }
}