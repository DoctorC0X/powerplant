﻿using System;
using System.Xml;
using Uninspired.Core.IO.Xml;

namespace EventSystem
{
    [Serializable]
    public class EventHandlerShape : EventHandler
    {
        public StarDustShapeData.Type desiredShapeType;

        public EventHandlerShape(EventData.ID id)
            : base(id)
        {
        }

        public override void HandleEvent(EventData.ParamBase param)
        {
            EventData.ParamShape shapeParam = (EventData.ParamShape)param;

            if (shapeParam.shape.type == desiredShapeType)
                base.HandleEvent(param);
        }

        public override void Register()
        {
            if (id != EventData.ID.STARDUST_BONUS)
                throw new ArgumentException("event id " + id + " has nothing to do with shapes");

            base.Register();
        }

        public override void ReadGameFile(XmlNode node, XmlNodeSelector selector)
        {
            base.ReadGameFile(node, selector);

            desiredShapeType = selector.SelectSingleByXPath(node, "shapeId").AsEnum<StarDustShapeData.Type>();
        }
    }
}