﻿using System;
using System.Xml;
using Uninspired.Core.IO.Xml;

namespace EventSystem
{
    [Serializable]
    public class EventHandlerBullet : EventHandler
    {
        public Bullet.Type desiredBulletType;

        public EventHandlerBullet(EventData.ID id)
            : base(id)
        {
        }

        public override void HandleEvent(EventData.ParamBase param)
        {
            EventData.ParamBullet bulletParam = (EventData.ParamBullet)param;

            if (bulletParam.bullet.type == desiredBulletType)
                base.HandleEvent(param);
        }

        public override void Register()
        {
            if (id != EventData.ID.BULLET_DODGE && id != EventData.ID.BULLET_HIT)
                throw new ArgumentException("event id " + id + " has nothing to do with bullets");

            base.Register();
        }

        public override void ReadGameFile(XmlNode node, XmlNodeSelector selector)
        {
            base.ReadGameFile(node, selector);

            desiredBulletType = selector.SelectSingleByXPath(node, "bulletId").AsEnum<Bullet.Type>();
        }
    }
}