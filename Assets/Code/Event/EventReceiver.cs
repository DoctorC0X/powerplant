﻿using UnityEngine;
using System.Collections;

namespace EventSystem
{
    public interface EventReceiver
    {
        void Register();
        void Deregister();
    }
}