﻿using UnityEngine;
using System.Collections;
using System;
using Progress.Missions;

// this file contains all custom data needed to run the global event system
// when integrating to a new game, change this files values accordingly

namespace EventSystem
{
    public class EventData
    {
        public enum ID
        {
            NONE,
            GAMESTART,
            TIME,
            TRAVELLED,
            HEIGHT,
            PIECES,
            BULLET_DODGE,
            BULLET_HIT,
            CRITTER_LANDED_LEFT,
            CRITTER_LANDED_RIGHT,
            CRITTER_STARTED_LEFT,
            CRITTER_STARTED_RIGHT,
            CRITTER_DELIVERED,
            STARDUST_BONUS,
            STARDUST_COLLECTED,
            SCREEN_EXIT,
            SCREEN_SWITCH,
            SCREEN_ENTER,
            MISSION_COMPLETED,
            MISSION_CLAIMED,
            XP_GAIN,
            INPUT_SLIDER_ENTER,
            INPUT_SLIDER_EXIT,
            INPUT_BUTTON_ENTER,
            INPUT_BUTTON_EXIT,
            MEDAL_UNLOCKED,
            GROUNDTOUCH_ENTER,
            GROUNDTOUCH_EXIT,
            STARPIECE_COLLECTED,
            STARPIECES_DELIVERED,
            TRAFFICZONE_ENTER,
            TRAFFICZONE_EXIT,
        }

        public class ParamBase
        {
            private ID id;
            public int increase;

            public ID ID
            {
                get { return id; }
                set { id = value; }
            }
        }

        public class ParamCritter : ParamBase
        {
            public Critter critter;
        }

        public class ParamBullet : ParamBase
        {
            public Bullet bullet;
        }

        public class ParamShape : ParamBase
        {
            public StarDustShape shape;
        }

        public class ParamMission : ParamBase
        {
            public Mission mission;
        }

        public class ParamStarDustCollect : ParamBase
        {
            public int dotValue;
        }

        public class ParamTrafficzone : ParamBase
        {
            public TrafficzoneSpawner.Side side;
        }
    }
}