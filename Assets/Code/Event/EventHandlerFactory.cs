﻿using System;
using System.Xml;
using Progress.IO;
using Uninspired.Core.IO.Xml;

namespace EventSystem
{
    public class EventHandlerFactory
    {
        public EventHandler Create(XmlNode node)
        {
            ProgressSelectorFactory selectorFactory = new ProgressSelectorFactory();
            XmlNodeSelector selector = selectorFactory.Create(node.OwnerDocument.NameTable);

            EventData.ID id = selector.SelectSingleByXPath(node, "id").AsEnum<EventData.ID>();

            EventHandler handler;

            switch (node.Attributes[0].Value)
            {
                case "standardEvent":
                    {
                        handler = new EventHandler(id);
                        break;
                    }
                case "bulletEvent":
                    {
                        handler = new EventHandlerBullet(id);
                        break;
                    }
                case "shapeEvent":
                    {
                        handler = new EventHandlerShape(id);
                        break;
                    }
                case "critterEvent":
                    {
                        handler = new EventHandlerCritter(id);
                        break;
                    }
                case "trafficzoneEvent":
                    {
                        handler = new EventHandlerTrafficzone(id);
                        break;
                    }
                default:
                    {
                        throw new ArgumentException("Unknown type from xml " + node.Attributes[0].Value);
                    }
            }

            handler.ReadGameFile(node, selector);

            return handler;
        }
    }
}