﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace EventSystem
{
    public class EventManager : MonoBehaviour
    {
        private static EventManager _instance;
        public static EventManager Instance
        {
            get { return _instance; }
        }

        private Dictionary<EventData.ID, Action<EventData.ParamBase>> handler;
        private Dictionary<EventData.ID, Action> handler_noParam;
        private Action preCall;

        private Dictionary<EventData.ID, List<EventHandler>> eventReceiver;

        void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
                return;
            }
            else
            {
                _instance = this;
            }

            handler = new Dictionary<EventData.ID, Action<EventData.ParamBase>>();
            handler_noParam = new Dictionary<EventData.ID, Action>();

            eventReceiver = new Dictionary<EventData.ID, List<EventHandler>>();

            foreach (EventData.ID current in Enum.GetValues(typeof(EventData.ID)))
            {
                handler.Add(current, null);
                handler_noParam.Add(current, null);
                eventReceiver.Add(current, new List<EventHandler>());
            }
        }

        public void CallEvent(EventData.ID id, EventData.ParamBase param)
        {
            if (id != EventData.ID.NONE)
            {
                if (preCall != null)
                {
                    preCall();
                    preCall = null;
                }

                param.ID = id;

                if (handler[id] != null)
                    handler[id](param);

                if (handler_noParam[id] != null)
                    handler_noParam[id]();

                for (int i = 0; i < eventReceiver[id].Count; i++)
                    eventReceiver[id][i].HandleEvent(param);
            }
        }

        public void CallEvent(EventData.ID pID)
        {
            CallEvent(pID, new EventData.ParamBase() { increase = 1 });
        }

        public void AddHandler(EventData.ID id, Action<EventData.ParamBase> action)
        {
            if (id != EventData.ID.NONE)
                preCall += () => handler[id] += action;
        }

        public void RemoveHandler(EventData.ID id, Action<EventData.ParamBase> action)
        {
            if (id != EventData.ID.NONE)
                preCall += () => handler[id] -= action;
        }

        public void AddHandler(EventData.ID id, Action action)
        {
            if (id != EventData.ID.NONE)
                preCall += () => handler_noParam[id] += action;
        }

        public void RemoveHandler(EventData.ID id, Action action)
        {
            if (id != EventData.ID.NONE)
                preCall += () => handler_noParam[id] -= action;
        }

        public void AddHandler(EventData.ID id, EventHandler er)
        {
            eventReceiver[id].Add(er);
        }

        public void RemoveHandler(EventData.ID id, EventHandler er)
        {
            eventReceiver[id].Remove(er);
        }
    }
}