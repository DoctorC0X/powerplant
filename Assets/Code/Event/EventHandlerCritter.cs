﻿using System;
using System.Xml;
using Uninspired.Core.IO.Xml;

namespace EventSystem
{
    [Serializable]
    public class EventHandlerCritter : EventHandler
    {
        public CritterData.Type desiredCritterType;

        public EventHandlerCritter(EventData.ID id)
            : base(id)
        {
        }

        public override void HandleEvent(EventData.ParamBase param)
        {
            EventData.ParamCritter critterParam = (EventData.ParamCritter)param;

            if (desiredCritterType == critterParam.critter.cData.type)
                base.HandleEvent(param);
        }

        public override void Register()
        {
            if (id != EventData.ID.CRITTER_LANDED_LEFT && id != EventData.ID.CRITTER_LANDED_RIGHT &&
                id != EventData.ID.CRITTER_STARTED_LEFT && id != EventData.ID.CRITTER_STARTED_RIGHT)
                throw new ArgumentException("event id " + id + " has nothing to do with critter");

            base.Register();
        }

        public override void ReadGameFile(XmlNode node, XmlNodeSelector selector)
        {
            base.ReadGameFile(node, selector);

            desiredCritterType = selector.SelectSingleByXPath(node, "critterId").AsEnum<CritterData.Type>();
        }
    }
}