﻿using System.Xml;
using System.Collections.Generic;
using Uninspired.Core.IO.Xml;

namespace EventSystem
{
    public class EventStateFactory
    {
        public EventState Create(XmlNode parent, XmlNodeSelector selector)
        {
            EventState.ActivateType type = selector.SelectSingleByXPath(parent, "activateType").AsEnum<EventState.ActivateType>();

            List<EventSubState> substates = new List<EventSubState>();
            EventSubstateFactory substateFactory = new EventSubstateFactory();

            XmlNodeWrapper[] substateNodes = selector.SelectByXPath(parent, "state");
            for (int i = 0; i < substateNodes.Length; i++)
            {
                EventSubState ess = substateFactory.Create(substateNodes[i].Node, selector);

                substates.Add(ess);
            }

            return new EventState(substates.ToArray());
        }
    }

    public class EventSubstateFactory
    {
        public EventSubState Create(XmlNode parent, XmlNodeSelector selector)
        {
            EventHandlerFactory ehFactory = new EventHandlerFactory();

            EventHandler enterEvent = ehFactory.Create(selector.SelectSingleByXPath(parent, "enterID").Node);
            EventHandler exitEvent = ehFactory.Create(selector.SelectSingleByXPath(parent, "exitID").Node);

            return new EventSubState(enterEvent, exitEvent);
        }
    }
}