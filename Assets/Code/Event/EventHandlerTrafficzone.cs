﻿using System;
using System.Xml;
using Uninspired.Core.IO.Xml;

namespace EventSystem
{
    [Serializable]
    public class EventHandlerTrafficzone : EventHandler
    {
        public TrafficzoneSpawner.Side desiredSide;

        public EventHandlerTrafficzone(EventData.ID id)
            : base(id)
        {
        }

        public override void HandleEvent(EventData.ParamBase param)
        {
            EventData.ParamTrafficzone trafficParam = (EventData.ParamTrafficzone)param;

            if (trafficParam.side == desiredSide)
                base.HandleEvent(param);
        }

        public override void Register()
        {
            if (id != EventData.ID.TRAFFICZONE_ENTER || id != EventData.ID.TRAFFICZONE_EXIT)
                throw new ArgumentException("event id " + id + " has nothing to do with trafficzones");

            base.Register();
        }

        public override void ReadGameFile(XmlNode node, XmlNodeSelector selector)
        {
            base.ReadGameFile(node, selector);

            desiredSide = selector.SelectSingleByXPath(node, "trafficzoneSide").AsEnum<TrafficzoneSpawner.Side>();
        }
    }
}