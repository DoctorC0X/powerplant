﻿using System;
using System.Xml;
using Progress.IO;

namespace EventSystem
{
    public class EventSubState : EventReceiver
    {
        public EventHandler enterEvent;
        public EventHandler exitEvent;

        public Action<bool> OnStateActiveChange;

        private bool _active;

        public bool Active
        {
            get { return _active; }
            set
            {
                if (_active != value)
                {
                    _active = value;

                    if (OnStateActiveChange != null)
                        OnStateActiveChange(value);
                }
            }
        }

        public EventSubState(EventHandler enter, EventHandler exit)
        {
            enterEvent = enter;
            exitEvent = exit;
        }

        public void Init()
        {
            if (enterEvent.id == EventData.ID.NONE && exitEvent.id == EventData.ID.NONE)
                Active = true;

            enterEvent.SetOnEventAction(() => Active = true);
            exitEvent.SetOnEventAction(() => Active = false);
        }

        public void Register()
        {
            enterEvent.Register();
            exitEvent.Register();
        }

        public void Deregister()
        {
            enterEvent.Deregister();
            exitEvent.Deregister();
        }
    }
}