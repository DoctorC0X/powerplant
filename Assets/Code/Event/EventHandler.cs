﻿using System;
using System.Xml;
using Uninspired.Core.IO.Xml;

namespace EventSystem
{
    public class EventHandler : EventReceiver
    {
        public EventData.ID id;

        public Action<EventData.ParamBase> eventAction;

        public Action eventAction_noParam;

        public EventHandler(EventData.ID id)
        {
            this.id = id;
        }

        public virtual void HandleEvent(EventData.ParamBase param)
        {
            if (eventAction != null)
                eventAction(param);

            if (eventAction_noParam != null)
                eventAction_noParam();
        }

        public virtual void SetOnEventAction(Action<EventData.ParamBase> action)
        {
            eventAction = action;
        }

        public virtual void SetOnEventAction(Action action)
        {
            eventAction_noParam = action;
        }

        public virtual void Register()
        {
            EventManager.Instance.AddHandler(id, this);
        }

        public virtual void Deregister()
        {
            EventManager.Instance.RemoveHandler(id, this);
        }

        public virtual void ReadGameFile(XmlNode node, XmlNodeSelector selector)
        {
        }
    }
}