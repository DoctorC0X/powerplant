﻿using UnityEngine;
using System.Collections;
using EventSystem;

public class EventIDString : ScriptableObject
{
    public EventData.ID id;

    public string noun;
    public string activity;
}
