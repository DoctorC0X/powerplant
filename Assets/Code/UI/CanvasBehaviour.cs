﻿using UnityEngine;
using System.Collections;

public class CanvasBehaviour : MonoBehaviour 
{
    public float zPos = -9;

    public MenuCamera.CamPosition myPos;

	// Use this for initialization
	void Start () 
    {
        this.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
        this.transform.localPosition = new Vector3(0, 0, zPos);
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
