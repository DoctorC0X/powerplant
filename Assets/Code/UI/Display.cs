﻿using UnityEngine;
using System.Collections;
using System;

public class Display : MonoBehaviour, PauseListener
{
    private RectTransform rTransform;

    public GameObject starDustCounter;
    public GameObject heightDisplay;

    public GameObject starShape;

    public float initialWidth;
    public float sizeIncrease;

    private float startWidth;
    private float targetWidth;

    public float interpolationDuration;

    private bool interpolating;
    public bool open;
    private float passedTime;

    private Action OnReached;

    public bool paused;

	// Use this for initialization
	void Awake () 
    {
        rTransform = GetComponent<RectTransform>();
        initialWidth = rTransform.sizeDelta.x;

        open = false;
	}

    void Start()
    {
        GameManager.Instance.pauseListener.Add(this);
    }

    void Update()
    {
        if (interpolating)
        {
            passedTime += Time.deltaTime;

            float fraction = passedTime / interpolationDuration;

            rTransform.sizeDelta = new Vector2(Mathf.Lerp(startWidth, targetWidth, fraction), rTransform.sizeDelta.y);

            if (fraction >= 1)
            {
                interpolating = false;

                if (OnReached != null)
                    OnReached();
            }
        }
    }

    public void Open()
    {
        if (!open && !interpolating)
        {
            interpolating = true;
            passedTime = 0;

            OnReached = () => open = true;

            startWidth = initialWidth;
            targetWidth = initialWidth + sizeIncrease;
        }
    }

    public void Close()
    {
        if (open && !interpolating)
        {
            interpolating = true;
            passedTime = 0;

            OnReached = () => open = false;

            startWidth = initialWidth + sizeIncrease;
            targetWidth = initialWidth;
        }
    }

    public void Toggle()
    {
        if (!interpolating)
        {
            if (open)
                Close();
            else
                Open();
        }
    }

    public void SetPause(bool pause)
    {
        paused = pause;

        heightDisplay.SetActive(!pause);

        if (open)
            starShape.SetActive(!pause);
    }

    public bool IsPaused()
    {
        return paused;
    }
}
