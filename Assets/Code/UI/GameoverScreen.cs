﻿using UnityEngine;
using System.Collections;
using Progress.Facts;

public class GameoverScreen : MonoBehaviour, GameOverListener
{
    public GameObject packagePrefab;

    public FactGenerator factGen;

    void Start()
    {
        GameManager.Instance.gameOverListener.Add(this);
    }

    public void GameOver()
    {
        string fact = factGen.GenerateFact();

        packagePrefab = Instantiate(packagePrefab) as GameObject;
        packagePrefab.transform.parent = transform;
        packagePrefab.transform.localPosition = Vector3.zero;
        packagePrefab.transform.localScale = Vector3.one;

        packagePrefab.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);

        packagePrefab.GetComponent<GameoverScreenPackage>().factText.text = fact;
    }
}
