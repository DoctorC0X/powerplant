﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Progress;
using EventSystem;

public class StatsView : MonoBehaviour 
{
    public GameObject statsShowerPrefab;

    public bool SpawnOnStart;

    public List<StatsShower> statsShower;

    public float startHeight;
    public float gap;

    public float displayDelay;

    public List<EventData.ID> ids;

    private float statsShowerHeight;

    // Use this for initialization
    void Start()
    {
        statsShowerHeight = statsShowerPrefab.GetComponent<RectTransform>().rect.height;

        if (SpawnOnStart)
        {
            for (int i = 0; i < ids.Count; i++)
            {
                Add(ids[i]);
            }

            Display();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private GameObject Spawn()
    {
        GameObject newShower = Instantiate(statsShowerPrefab) as GameObject;

        newShower.transform.parent = this.transform;

        newShower.transform.localPosition = new Vector3(0, startHeight - statsShower.Count * (gap + statsShowerHeight), 0);
        newShower.transform.rotation = Quaternion.identity;
        newShower.transform.localScale = Vector3.one;

        return newShower;
    }

    public GameObject Add(string title, int value, int record)
    {
        GameObject newShower = Spawn();

        newShower.GetComponent<StatsShower>().Init(title, displayDelay, value, record);

        statsShower.Add(newShower.GetComponent<StatsShower>());

        return newShower;
    }

    public GameObject Add(string title, int value)
    {
        GameObject newShower = Spawn();

        newShower.GetComponent<StatsShower>().Init(title, displayDelay, value);

        statsShower.Add(newShower.GetComponent<StatsShower>());

        return newShower;
    }

    public GameObject Add(EventData.ID id)
    {
        return Add(id.ToString(), ValueManager.Instance.Values[id].currentRound);
    }

    public void Display()
    {
        int d = 0;
        foreach (StatsShower current in statsShower)
        {
            current.Display(displayDelay * d);
            d++;
        }
    }

    public void SetTitleOffset(float offset)
    {
        foreach (StatsShower current in statsShower)
        {
            current.TitleOffset = offset;
        }
    }
}
