﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class SpriteButton : BasicButton 
{
    public Image target;

    public Sprite normal;
    public Sprite pressed;

    public override void Start()
    {
        base.Start();

        OnButtonDown += SetPressed;
        OnButtonUp += SetNormal;
    }

    private void SetSprite(Sprite s)
    {
        target.sprite = s;
    }

    private void SetNormal()
    {
        SetSprite(normal);
    }

    private void SetPressed()
    {
        SetSprite(pressed);
    }

    public override void SetInteractable(bool active)
    {
        base.SetInteractable(active);

        target.enabled = active;
    }
}
