﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ColorButton : BasicButton
{
    public Image target;

    public Color normal;
    public Color pressed;
    public Color locked;

    public override void Start()
    {
        base.Start();

        OnButtonDown += SetPressed;
        OnButtonUp += SetNormal;
    }

    public void SetColor(Color c)
    {
        target.color = c;
    }

    private void SetNormal()
    {
        SetColor(normal);
    }

    private void SetPressed()
    {
        SetColor(pressed);
    }

    private void SetLocked()
    {
        SetColor(locked);
    }

    public override void SetInteractable(bool active)
    {
        base.SetInteractable(active);

        if (active)
            SetNormal();
        else
            SetLocked();
    }
}
