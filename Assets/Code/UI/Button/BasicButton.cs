﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class BasicButton : MonoBehaviour 
{
    public event Action OnButtonDown;
    public event Action OnButtonUp;

    public bool interactable;

    public virtual void Start()
    {
        SetInteractable(interactable);
    }

    public virtual void ButtonDown()
    {
        if (OnButtonDown != null && interactable)
            OnButtonDown();
    }

    public virtual void ButtonUp()
    {
        if (OnButtonUp != null && interactable)
            OnButtonUp();
    }

    public virtual void SetInteractable(bool active)
    {
        interactable = active;
    }
}
