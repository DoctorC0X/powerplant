﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using EventSystem;
using Progress.Missions;

public class StarDustCollectCounter : Counter 
{
    private static StarDustCollectCounter _instance;

    public static StarDustCollectCounter Instance
    {
        get { return _instance; }
    }

    public GameObject icon;

    private int amount;

    public override void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            _instance = this;
        }

        base.Awake();
    }

	public override void Start()
    {
        base.Start();

        amount = PlayerProfile.Instance.GetStarDustBalance();

        PlayerProfile.Instance.OnStarDustBalanceChange += (int oldA, int newA) => UpdateText(newA);

        /*
        if (EventManager.Instance != null)
        {
            EventManager.Instance.AddHandler(EventData.ID.STARDUST_COLLECTED, () =>
                {
                    amount++;

                    UpdateText();
                });

            EventManager.Instance.AddHandler(EventData.ID.MISSION_CLAIMED, (EventData.ParamBase b) =>
            {
                Mission mission = ((EventData.ParamMission)b).mission;

                amount += mission.reward_starDust;

                UpdateText();
            });
        }
        */

        UpdateText(PlayerProfile.Instance.GetStarDustBalance());
	}

    public override void Update()
    {
        if (currentAction != null)
            currentAction();
    }

    public void UpdateText(int amount)
    {
        UpdateText(amount.ToString());
    }
}
