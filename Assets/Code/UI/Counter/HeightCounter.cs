﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HeightCounter : MonoBehaviour
{
    public Text text;

    void Update()
    {
        text.text = ((int)PlantModel.MainPlant.highscoreWatcher.currentHeight).ToString();
    }
}
