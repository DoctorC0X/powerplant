﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Counter : MonoBehaviour
{
    public Text text;

    public Color normalColor;
    public Color collectColor;

    public int normalSize;
    public int collectSize;

    private float timePassed;
    public float toNormalDuration;
    public float toCollectDuration;
    public float waitDuration;

    public Action currentAction;

    public virtual void Awake()
    {
        if (text == null)
            text = GetComponent<Text>();
    }

    public virtual void Start()
    {

    }

    // Update is called once per frame
    public virtual void Update()
    {
        if (currentAction != null && !GameManager.Instance.paused)
            currentAction();
    }

    public virtual void UpdateText(string text, bool ignoreEffect = false)
    {
        this.text.text = text;

        if(!ignoreEffect)
            OnValueChanged();
    }

    public void OnValueChanged()
    {
        timePassed = 0;
        currentAction = SwitchToCollect;
    }

    public void SwitchToCollect()
    {
        timePassed += Time.deltaTime;

        float fraction = timePassed / toCollectDuration;

        text.color = Color.Lerp(normalColor, collectColor, fraction);
        text.fontSize = (int)Mathf.Lerp(normalSize, collectSize, fraction);

        if (fraction >= 1)
        {
            timePassed = 0;
            currentAction = Wait;
        }
    }

    public void Wait()
    {
        timePassed += Time.deltaTime;

        if (timePassed >= waitDuration)
        {
            timePassed = 0;
            currentAction = SwitchToNormal;
        }
    }

    public void SwitchToNormal()
    {
        timePassed += Time.deltaTime;

        float fraction = timePassed / toNormalDuration;

        text.color = Color.Lerp(collectColor, normalColor, fraction);
        text.fontSize = (int)Mathf.Lerp(collectSize, normalSize, fraction);

        if (fraction >= 1)
        {
            currentAction = null;
        }
    }
}
