﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FpsCounter : MonoBehaviour 
{
    public Text text;

    public float frameCount = 0;
    public float dt = 0.0f;
    public float fps = 0.0f;
    public float updateRate = 4.0f;  // 4 updates per sec.

	// Use this for initialization
	void Start () 
    {
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        frameCount++;
        dt += Time.deltaTime;
        if (dt > 1.0f / updateRate)
        {
            fps = frameCount / dt;
            frameCount = 0;
            dt -= 1.0f / updateRate;

            fps = Mathf.RoundToInt(fps);

            text.text = "fps " + fps;
        }
	}
}
