﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpeechBubble : MonoBehaviour 
{
    public Text text;

    public Vector2 AnchoredPos
    {
        get 
        { 
            return GetComponent<RectTransform>().anchoredPosition;
        }
        set
        {
            GetComponent<RectTransform>().anchoredPosition = value;
        }
    }

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }
}
