﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Progress.Facts;

public class GameoverScreenPackage : MonoBehaviour
{
    public GameObject fader;
    public GameObject dialog;

    public Text factText;

    public bool showing;

    public void Start()
    {
        SetShow(true);
    }

    public void SetShow(bool s)
    {
        showing = s;

        fader.SetActive(showing);
        dialog.SetActive(showing);
    }

    public void LoadGameScene()
    {
        GameManager.Instance.LoadGameScene();
    }
}
