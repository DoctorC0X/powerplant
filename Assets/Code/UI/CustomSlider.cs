﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using EventSystem;

public class CustomSlider : MonoBehaviour 
{
    private float _value;

    public GameObject background;
    public RectTransform knob;

    private float knobMinPos;
    private float knobMaxPos;

    public float screenScale;

    public bool enableClickStart;

    public float Value
    {
        get { return _value; }
        set
        {
            _value = Mathf.Clamp(value, -1, 1);

            SetRelativeKnobX(TransformToKnob(_value));
        }
    }

    void Awake()
    {
        knobMinPos = GetComponent<RectTransform>().sizeDelta.x * -0.5f;
        knobMaxPos = GetComponent<RectTransform>().sizeDelta.x *  0.5f;

        screenScale = 1 + GetComponent<RectTransform>().sizeDelta.x / Screen.width;

    }

    public void OnPointerDrag(BaseEventData data)
    {
        PointerEventData pData = (PointerEventData)data;

        Value = Mathf.Clamp(Value + ((pData.delta.x / knobMaxPos) * screenScale), -1, 1);
    }

    public void OnPointerDown(BaseEventData data)
    {
        if (enableClickStart)
        {
            PointerEventData pData = (PointerEventData)data;

            Vector2 localPoint;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(), pData.position, Camera.main, out localPoint);

            Value = TransformToValue(GetKnobXFraction(localPoint.x));
        }

        EventManager.Instance.CallEvent(EventData.ID.INPUT_SLIDER_ENTER);
    }

    public void OnPointerUp(BaseEventData data)
    {
        Value = 0;

        EventManager.Instance.CallEvent(EventData.ID.INPUT_SLIDER_EXIT);
    }

    // set the slider knob between left(0) and right(1) [mid(0.5)]
    public void SetRelativeKnobX(float fraction)
    {
        knob.anchoredPosition = new Vector2(Mathf.Lerp(knobMinPos, knobMaxPos, fraction), knob.anchoredPosition.y);
    }

    // calculate the knob fraction based on the x pos. left(0) - right(1)
    public float GetKnobXFraction(float x)
    {
        x = Mathf.Clamp(x, knobMinPos, knobMaxPos);

        x = (x - knobMinPos) / (knobMaxPos - knobMinPos);

        return x;
    }

    public float TransformToKnob(float value)
    {
        return Mathf.Clamp((value + 1) / 2, 0, 1);
    }

    public float TransformToValue(float value)
    {
        return Mathf.Clamp((value * 2) - 1, -1, 1);
    }
}
