﻿using UnityEngine;
using System.Collections;

public class MenuParent : MonoBehaviour 
{
	// Use this for initialization
	void Start () 
    {
	
	}

	// Update is called once per frame
	void Update () 
    {
	
	}

    public void SetPause(bool pause)
    {
        GameManager.Instance.SetPause(pause, true);
    }

    public void RestartGame()
    {
        GameManager.Instance.LoadGameScene();
    }
}
