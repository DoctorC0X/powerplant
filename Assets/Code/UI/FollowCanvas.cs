﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using EventSystem;

public class FollowCanvas : MonoBehaviour
{
    private static FollowCanvas _instance;

    public static FollowCanvas Instance
    {
        get { return _instance; }
    }

    public GameObject backgroundCanvas;

    public GameObject pauseButton;
    public GameObject muteButton;

    public GameObject indicatorManager;

    public Display display;

    public BasicButton growButton;
    public BasicButton skillButton;
    public BasicButton busButton;

    public CustomSlider growSlider;

    public PauseScreen pauseScreen;

    public Transform topLeft;
    public Transform botRight;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            DestroyImmediate(this.gameObject);
            return;
        }
        else
        {
            _instance = this;
        }
    }

    void Start()
    {
        EventManager.Instance.AddHandler(EventData.ID.GAMESTART, () =>
        {
            Transform gameButtons = growButton.transform.parent;

            gameButtons.parent = transform;
            gameButtons.SetSiblingIndex(11);
        });
    }

    public Vector2 ClampToCanvas(Vector2 pos)
    {
        float x = Mathf.Clamp(pos.x, topLeft.transform.position.x, botRight.transform.position.x);
        float y = Mathf.Clamp(pos.y, botRight.transform.position.y, topLeft.transform.position.y);

        return new Vector2(x, y);
    }

    public void LoadGameScene()
    {
        GameManager.Instance.LoadGameScene();
    }

    public void LoadPreloadScene()
    {
        GameManager.Instance.LoadPreloadScene();
    }
}
