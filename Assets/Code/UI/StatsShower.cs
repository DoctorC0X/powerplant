﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatsShower : MonoBehaviour 
{
    public Text statName;
    public Text statValue;
    public Text statBest;

    private bool _show;
    public bool Show
    {
        get { return _show; }
        set
        {
            _show = value;

            statName.enabled = _show;
            statValue.enabled = _show;

            if(useBest)
                statBest.enabled = _show;
        }
    }

    public AnimationCurve increaseCurve;

    public float increaseDuration;
    public int targetInt;

    private float startTime;

    private bool useBest;

    public void Init(string name, float increaseDuration, int targetValue)
    {
        Show = false;

        statName.text = name;
        
        useBest = false;
        statBest.enabled = false;
        //Destroy(statBest.gameObject);

        this.targetInt = targetValue;
        this.increaseDuration = increaseDuration;
    }

    public void Init(string name, float increaseDuration, int targetValue, int best)
    {
        Init(name, increaseDuration, targetValue);

        useBest = true;
        statBest.enabled = true;
        statBest.text = "(" + best + ")";
    }

    public void Display(float delay)
    {
        Coworker worker = this.gameObject.AddComponent<Coworker>();
        worker.StartDelayedAction(delay, null, Display);
    }

    public void Display()
    {
        Show = true;

        startTime = Time.time;

        Coworker increaser = this.gameObject.AddComponent<Coworker>();
        increaser.StartDelayedAction(increaseDuration, () => UpdateValue(0, targetInt, startTime, increaseDuration), null);
    }

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {

	}

    private void UpdateValue(int start, int end, float startTime, float duration)
    {
        float fraction = (Time.time - startTime) / duration;

        fraction = increaseCurve.Evaluate(fraction);

        float value = Mathf.Lerp(start, end, fraction);

        statValue.text = Mathf.RoundToInt(value).ToString();
    }

    public float TitleOffset
    {
        get
        {
            return statName.GetComponent<RectTransform>().anchoredPosition3D.x;
        }
        set
        {
            Vector3 pos = statName.GetComponent<RectTransform>().anchoredPosition3D;
            statName.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(value, pos.y, pos.z);
        }
    }
}
