﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MuteButton : MonoBehaviour 
{
    public Text buttonText;

    private bool _muted;
    public bool Muted
    {
        get { return _muted; }
        set
        {
            if (value)
                buttonText.text = "Unmute";
            else
                buttonText.text = "Mute";

            _muted = value;
        }
    }

    void Awake()
    {
        buttonText = this.GetComponentInChildren<Text>();
    }

    void Start()
    {
        Muted = AudioSources.Instance.IsMuted();
    }

    public void SetMuted(bool m)
    {
        Muted = m;
        AudioSources.Instance.SetMuted(m);
    }

    public void ToggleSound()
    {
        SetMuted(!Muted);
    }
}
