﻿using UnityEngine;
using System.Collections;
using Uninspired.Core.Interpolation.Movement;
using System.Collections.Generic;
using System;

public class GridData : MonoBehaviour 
{
    public InterpolatedMovement gridSleigh;

    public int pageColumns = 1;
    public int pageRows = 1;

    public float pageGapSize;
    public float tileGapSize;

    public enum ScrollDirection { UP_DOWN, LEFT_RIGHT };
    public ScrollDirection scrollDir;

    [Header("Auto generated values")]
    public Vector2 pageSize;
    public Vector2 tileSize;

    public Vector2 pageDistance;

    public event Action OnCalculationsDone;

    void Start()
    {
        pageSize = GetComponent<RectTransform>().rect.size;

        float tileWidth = pageSize.x;
        tileWidth -= (pageColumns + 1) * tileGapSize;
        tileWidth /= pageColumns;

        float tileHeight = pageSize.y;
        tileHeight -= (pageRows + 1) * tileGapSize;
        tileHeight /= pageRows;

        tileSize = new Vector2(tileWidth, tileHeight);

        if (OnCalculationsDone != null)
            OnCalculationsDone();
    }
}
