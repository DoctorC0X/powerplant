﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(GridView))]
public class GridViewEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        /*
        if (Application.isPlaying)
        {
            EditorGUILayout.HelpBox("When changing the above values, hit 'Rebuild' to apply them. Note that they wont be saved when exiting play mode.", MessageType.Info);

            if (GUILayout.Button("Rebuild"))
            {
                ((GridModel)target).Rebuild();
            }
        }
        */
    }
}
