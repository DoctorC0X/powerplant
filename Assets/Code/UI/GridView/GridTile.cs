﻿using UnityEngine;
using System.Collections;

public class GridTile : MonoBehaviour 
{
    private GridPage page;
    private RectTransform rectTransform;

    public void Init(GridPage page, Vector2 pos, Vector2 size)
    {
        this.page = page;

        transform.parent = page.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;

        rectTransform = Utility.LazyGetComponent<RectTransform>(gameObject);
        SetSize(size);
        SetPos(pos);
    }

    public void SetSize(Vector2 size)
    {
        rectTransform.sizeDelta = size;
    }

    public void SetPos(Vector2 pos)
    {
        rectTransform.anchoredPosition = pos;
    }

    public void RemoveTile()
    {
        Destroy(gameObject);

        page.Refresh();
    }
}
