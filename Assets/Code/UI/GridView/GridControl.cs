﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Uninspired.Core.Interpolation.Movement;
using System.Collections.Generic;

public class GridControl : MonoBehaviour 
{
    private GridData data;
    private GridView gridView;

    private List<Vector2> scrollPositions;
    
    private int activePageindex;

    public Button[] forwardButtons;
    public Button[] backwardButtons;

    public bool hideInactivePages;

    public int ActivePageIndex
    {
        get { return activePageindex; }
    }

    void Awake()
    {
        scrollPositions = new List<Vector2>();

        data = GetComponent<GridData>();
        gridView = GetComponent<GridView>();

        gridView.onPageAdded += RefreshButtonInteractability;
        gridView.onPageAdded += CalcPageDistance;
    }

    void Start()
    {
        RefreshButtonInteractability();
    }

    public void MoveToPage(int pageIndex)
    {
        Vector3 target = transform.position + (Vector3)(pageIndex * data.pageDistance);

        data.gridSleigh.Target = new IMTarget(target);
        data.gridSleigh.onReachedOnce += RefreshPageVisibility;
        data.gridSleigh.Begin();

        activePageindex = pageIndex;

        gridView.pages[pageIndex].gameObject.SetActive(true);
    }

    private void RefreshPageVisibility()
    {
        if (hideInactivePages)
        {
            for (int i = 0; i < gridView.pages.Count; i++)
            {
                if (i == activePageindex)
                    gridView.pages[i].gameObject.SetActive(true);
                else
                    gridView.pages[i].gameObject.SetActive(false);
            }
        }
    }

    private void CalcPageDistance()
    {
        if(gridView.pages.Count == 2)
        {
            data.pageDistance = gridView.transform.position - gridView.pages[1].transform.position;
            gridView.onPageAdded -= CalcPageDistance;
        }
    }

    public bool IsScrollForwardPossible()
    {
        return (ActivePageIndex + 1) < gridView.pages.Count;
    }

    public bool IsScrollBackwardPossible()
    {
        return (ActivePageIndex - 1) >= 0;
    }

    public void ScrollForward()
    {
        if (IsScrollForwardPossible())
        {
            MoveToPage(ActivePageIndex + 1);
            RefreshButtonInteractability();
        }
    }

    public void ScrollBackward()
    {
        if (IsScrollBackwardPossible())
        {
            MoveToPage(ActivePageIndex - 1);
            RefreshButtonInteractability();
        }
    }

    private void RefreshButtonInteractability()
    {
        for (int i = 0; i < forwardButtons.Length; i++)
        {
            if(forwardButtons[i] != null)
                forwardButtons[i].interactable = IsScrollForwardPossible();
        }

        for (int i = 0; i < backwardButtons.Length; i++)
        {
            if(backwardButtons[i] != null)
                backwardButtons[i].interactable = IsScrollBackwardPossible();
        }
    }
}
