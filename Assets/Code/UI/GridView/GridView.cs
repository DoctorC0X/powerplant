﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Uninspired.Core.Interpolation.Movement;
using System;

public class GridView : MonoBehaviour 
{
    public GridData data;

    public List<GridPage> pages;

    public event Action onTileAdded;
    public event Action onPageAdded;

    void Awake()
    {
        data = GetComponent<GridData>();
        data.OnCalculationsDone += Refresh;
    }

    private RectTransform GetRectTransformLazy(GameObject go)
    {
        RectTransform rt = go.GetComponent<RectTransform>();

        if (rt == null)
            rt = go.AddComponent<RectTransform>();

        return rt;
    }

    public void AddTile(GameObject tileGo, GameObject pagePrefab = null)
    {
        GridTile tile = Utility.LazyGetComponent<GridTile>(tileGo);

        if (pages.Count == 0 || pages[pages.Count - 1].Capacity <= 0)
            AddPage(pagePrefab);

        GridPage lastPage = pages[pages.Count - 1];

        if (pages.Count == 1)
            data.pageDistance = transform.position - lastPage.transform.position;

        lastPage.AddTile(tile);
        
        if (onTileAdded != null)
            onTileAdded();
    }

    private void AddPage(GameObject pagePrefab)
    {
        GameObject pageGo = null;

        if (pagePrefab != null)
            pageGo = Instantiate<GameObject>(pagePrefab);
        else
            pageGo = new GameObject("GridPage");

        GridPage page = Utility.LazyGetComponent<GridPage>(pageGo);

        page.Init(this, data, CalcPagePos(pages.Count), data.pageSize);

        pages.Add(page);

        if (onPageAdded != null)
            onPageAdded();
    }

    private Vector2 CalcPagePos(int pageIndex)
    {
        if (data.scrollDir == GridData.ScrollDirection.UP_DOWN)
        {
            return new Vector2(0, -pageIndex * (data.pageSize.y + data.pageGapSize));
        }
        else
        {
            return new Vector2(pageIndex * (data.pageSize.x + data.pageGapSize), 0);
        }
    }

    public void Refresh()
    {
        for (int i = 0; i < pages.Count; i++)
        {
            if (pages[i] == null)
            {
                pages.RemoveAt(i);

                continue;
            }
            else
            {
                pages[i].SetPos(CalcPagePos(i));
                pages[i].SetSize(data.pageSize);

                pages[i].Refresh();
            }
        }
    }
}
