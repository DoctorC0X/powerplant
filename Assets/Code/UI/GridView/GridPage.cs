﻿using UnityEngine;
using System.Collections;
using Uninspired.Core.Interpolation.Movement;
using System.Collections.Generic;
using System;

public class GridPage : MonoBehaviour 
{
    private GridData data;
    private GridView view;

    public List<GridTile> tiles;
    private int capacity;

    private RectTransform rectTransform;

    public int Capacity
    {
        get { return capacity; }
    }

    public void Init(GridView view, GridData data, Vector2 pos, Vector2 size)
    {
        this.data = data;
        this.view = view;
        this.tiles = new List<GridTile>();
        this.capacity = data.pageColumns * data.pageRows;

        transform.parent = data.gridSleigh.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;

        rectTransform = Utility.LazyGetComponent<RectTransform>(gameObject);
        rectTransform.anchoredPosition = pos;
        rectTransform.sizeDelta = size;
    }

    public void AddTile(GridTile tile)
    {
        if (Capacity <= 0)
            throw new IndexOutOfRangeException();

        tile.Init(this, CalcTilePos(tiles.Count), data.tileSize);

        tiles.Add(tile);
        capacity--;
    }

    private Vector2 CalcTilePos(int tileIndex)
    {
        float x = 0;
        float y = 0;

        if (data.pageColumns > 1)
        {
            float width = (data.tileGapSize + data.tileSize.x);
            float firstPos = (-data.pageSize.x + width + data.tileGapSize) / 2;

            x = firstPos + (GetColumnIndex(tileIndex) * width);
        }

        if (data.pageRows > 1)
        {
            float height = (data.tileGapSize + data.tileSize.y);
            float firstPos = (data.pageSize.y - height - data.tileGapSize) / 2;

            y = firstPos - (GetRowIndex(tileIndex) * height);
        }

        return new Vector2(x, y);
    }

    public void RemovePage()
    {
        Destroy(gameObject);

        view.Refresh();
    }

    public void Refresh()
    {
        if (tiles.Count > 0)
        {
            for (int i = 0; i < tiles.Count; i++)
            {
                if (tiles[i] == null)
                {
                    tiles.RemoveAt(i);
                    continue;
                }
                else
                {
                    tiles[i].SetPos(CalcTilePos(i));
                    tiles[i].SetSize(data.tileSize);
                }
            }
        }
        else
        {
            RemovePage();
        }
    }

    private int GetRowIndex(int tileIndex)
    {
        return tileIndex / data.pageColumns;
    }

    private int GetColumnIndex(int tileIndex)
    {
        return tileIndex % data.pageColumns;
    }

    public void SetSize(Vector2 size)
    {
        rectTransform.sizeDelta = size;
    }

    public void SetPos(Vector2 pos)
    {
        rectTransform.anchoredPosition = pos;
    }
}
