﻿using UnityEngine;
using System.Collections;
using EventSystem;

public class ScreenFlipButton : MonoBehaviour
{

	// Use this for initialization
	void Start () 
    {
        EventManager.Instance.AddHandler(EventData.ID.GAMESTART, () => Destroy(gameObject));
	}

    public void Toggle()
    {
        if (Screen.orientation == ScreenOrientation.LandscapeLeft)
        {
            Screen.orientation = ScreenOrientation.LandscapeRight;
        }
        else if (Screen.orientation == ScreenOrientation.LandscapeRight)
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
        }
    }
}
