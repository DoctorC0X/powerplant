﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Progress.Missions;

public class PauseScreen : MonoBehaviour, PauseListener
{
    public Image fader;

    public Text title;

    public Button restart;
    public Button resume;

    public MissionPicker missionPicker;

    public bool paused;

	void Start () 
    {
        GameManager.Instance.pauseListener.Add(this);
        SetPause(GameManager.Instance.paused);
	}

    public void SetPause(bool pause)
    {
        paused = pause;

        fader.enabled = paused;
        title.enabled = paused;
        restart.gameObject.SetActive(paused);
        resume.gameObject.SetActive(paused);
        missionPicker.SetShow(paused);
    }

    public bool IsPaused()
    {
        return paused;
    }

    public void Resume()
    {
        GameManager.Instance.SetPause(false);
    }

    public void Restart()
    {
        GameManager.Instance.LoadGameScene();
    }
}
