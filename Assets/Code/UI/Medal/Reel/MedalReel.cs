﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Progress;

public class MedalReel : MonoBehaviour 
{
    public GoalManager goalManager;
    public GridView gridView;

    public GameObject medalTilePrefab;
    public GameObject medalPagePrefab;

    void Start()
    {
        foreach (KeyValuePair<string, Goal> current in goalManager.Goals)
        {
            GameObject tileGo = Instantiate<GameObject>(medalTilePrefab);

            tileGo.GetComponent<MedalTile>().Init(current.Value);

            gridView.AddTile(tileGo, medalPagePrefab);
        }
    }
}
