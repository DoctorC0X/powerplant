﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Progress;

public class MedalTile : MonoBehaviour 
{
    public Goal medal;

    public Text title;
    public Text description;
    public Image image;

    public Sprite locked;
    public Sprite notFound;

    public void Init(Goal t)
    {
        medal = t;

        title.text = medal.GetTitle();
        description.text = medal.GetDescription();

        if (medal.IsUnlocked())
        {
            if (medal.GetSprite() != null)
            {
                image.sprite = medal.GetSprite();
            }
            else
            {
                image.sprite = notFound;
            }
        }
        else
        {
            image.sprite = locked;
        }
    }
}
