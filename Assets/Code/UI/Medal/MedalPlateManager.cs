﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Progress;

public class MedalPlateManager : MonoBehaviour 
{
    private static MedalPlateManager _instance;

    public static MedalPlateManager Instance
    {
        get { return _instance; }
    }

    public GoalManager medalManager;

    public GameObject medalPlatePrefab;

    public float moveDuration;

    public float stayTimer;

    public Vector3 firstPos;

    private float plateWidth;
    public float plateHeight;
    public float heightGap;

    public int maxPlates;
    private int counter;

    private bool displayFull;

    private List<string> queue;

    private MedalPlate lastSpawned;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(this);
            return;
        }
        else
        {
            _instance = this;
        }

        counter = 0;
        displayFull = false;

        plateWidth = medalPlatePrefab.GetComponent<RectTransform>().rect.width;
        plateHeight = medalPlatePrefab.GetComponent<RectTransform>().rect.height;

        queue = new List<string>();
    }

	// Use this for initialization
	void Start () 
    {
        medalManager.OnUnlocked += SpawnMedalPlate;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!displayFull && queue.Count > 0)
        {
            for(int i = 0; i < queue.Count; i++)
            {
                SpawnMedalPlate(queue[i]);
                queue.RemoveAt(i);
            }
        }
	}

    public void SpawnMedalPlate(string title)
    {
        if (!displayFull)
        {
            GameObject newPlateGO = Instantiate(medalPlatePrefab) as GameObject;
            newPlateGO.transform.SetParent(transform, false);
            newPlateGO.transform.localScale = Vector3.one;

            MedalPlate plate = newPlateGO.GetComponent<MedalPlate>();

            Vector3 pos = new Vector3();

            if (lastSpawned != null)
            {
                float height = lastSpawned.GetComponent<RectTransform>().anchoredPosition3D.y + plateHeight + heightGap;

                pos = new Vector3(firstPos.x, height, firstPos.z);
                lastSpawned.upNext = plate;
            }
            else
            {
                pos = firstPos;
            }

            plate.Init(title, pos + new Vector3(plateWidth + 5, 0, 0), pos, moveDuration, stayTimer);

            plate.OnExited += OnMedalExit;

            lastSpawned = plate;

            counter++;

            if (counter >= maxPlates)
                displayFull = true;
        }
        else
        {
            queue.Add(title);
        }
    }

    public void SpawnMedalPlate(Goal m)
    {
        SpawnMedalPlate(m.GetTitle());
    }

    public void OnMedalExit()
    {
        counter--;

        if (displayFull && counter <= 6)
        {
            displayFull = false;
        }
    }
}
