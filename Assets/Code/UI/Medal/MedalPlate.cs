﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class MedalPlate : MonoBehaviour 
{
    public Text trophyTitle;

    public float moveDuration;

    public float waitDuration;

    public bool targetReached;

    private float passedTime;

    public Vector3 start;
    public Vector3 target;

    public MedalPlate upNext;

    private event Action NextAction;

    public event Action OnEntered;
    public event Action OnExited;

	// Use this for initialization
	void Start () 
    {
        OnEntered += Enter;
        OnExited += Exit;

        NextAction = OnEntered;
	}

    public void Init(string title, Vector3 startPos, Vector3 targetPos, float speed, float waitDuration)
    {
        trophyTitle.text = title;

        Init(startPos, targetPos, speed, waitDuration);
    }

    public void Init(Vector3 startPos, Vector3 targetPos, float moveDuration, float waitDuration)
    {
        start = startPos;
        target = targetPos;

        this.moveDuration = moveDuration;
        this.waitDuration = waitDuration;

        passedTime = 0;

        targetReached = false;
    }

	// Update is called once per frame
	void Update () 
    {
        if (!targetReached)
        {
            passedTime += Time.deltaTime;

            float fraction = passedTime / moveDuration;

            //this.transform.position = Vector3.Lerp(start, target, fraction);
            this.GetComponent<RectTransform>().anchoredPosition3D = Vector3.Lerp(start, target, fraction);

            if (fraction > 1)
            {
                targetReached = true;

                NextAction();
            }
        }
	}

    public IEnumerator Countdown()
    {
        yield return new WaitForSeconds(waitDuration);

        RectTransform rectT = this.GetComponent<RectTransform>();

        Init(rectT.anchoredPosition3D, rectT.anchoredPosition3D + new Vector3(rectT.rect.width + 10, 0, 0), moveDuration, 0);
    }

    public void Enter()
    {
        StartCoroutine(Countdown());

        NextAction = OnExited;
    }

    public void Exit()
    {
        if (upNext != null)
            upNext.RecursiveGetDown();

        Destroy(this.gameObject);
    }

    private void RecursiveGetDown()
    {
        float heightDifference = MedalPlateManager.Instance.plateHeight + MedalPlateManager.Instance.heightGap;

        if (!targetReached)
        {
            start -= new Vector3(0, heightDifference, 0);
            target -= new Vector3(0, heightDifference, 0);
        }
        else
        {
            this.GetComponent<RectTransform>().anchoredPosition3D -= new Vector3(0, heightDifference, 0);
        }

        if (upNext != null)
            upNext.RecursiveGetDown();
    }
}
