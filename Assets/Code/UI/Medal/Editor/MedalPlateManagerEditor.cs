﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(MedalPlateManager))]
public class MedalPlateManagerEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (Application.isPlaying && GUILayout.Button("Spawn Test Plate"))
        {
            ((MedalPlateManager)target).SpawnMedalPlate("TEST");
        }
    }
}
