﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;

public class Accumulator : MonoBehaviour 
{
    public TitleValueLabel totalLabel;
    public TitleValueLabel operandLabel;

    public long total;
    public float currentOperand;
    public Operator currentOperator;

    private Queue<string> titles;
    private Queue<float> operands;
    private Queue<Operator> operators;

    public bool activated;

    public enum State { NULL, WAIT, NEXT, DISPLAY, OPERATE, DONE, COMPLETED };
    public State currentState;

    public enum Operator { ADD, MULTIPLY };

    public float operateDuration;
    private float operateTimePassed;
    private float totalStart;
    private float operandStart;

    public float textUpdateTimerMax;
    public float textUpdateTimer;

    public event Action OnDisplay;
    public event Action OnNext;
    public event Action OnCompleted;

    private Coroutine waitRoutine;

    void Awake()
    {
        totalLabel.gameObject.SetActive(false);
        operandLabel.gameObject.SetActive(false);

        titles = new Queue<string>();
        operands = new Queue<float>();
        operators = new Queue<Operator>();
    }

    public void Init(string totalTitle, long totalValue)
    {
        SetTotal(totalTitle, totalValue);

        totalLabel.gameObject.SetActive(true);

        OnCompleted = null;
        currentState = State.NULL;
    }

    public void Begin()
    {
        SwitchState(State.NEXT);
    }
	
	// Update is called once per frame
	void Update () 
    {
        switch (currentState)
        {
            case State.NULL:
                {
                    break;
                }
            case State.NEXT:
                {
                    break;
                }
            case State.DISPLAY:
                {
                    break;
                }
            case State.OPERATE:
                {
                    operateTimePassed += Time.deltaTime;
                    textUpdateTimer -= Time.deltaTime;

                    float fraction = operateTimePassed / operateDuration;

                    Operate(fraction);

                    if (textUpdateTimer <= 0)
                    {
                        textUpdateTimer = textUpdateTimerMax;

                        SetLabelsToCurrent();
                    }

                    if (fraction >= 1)
                    {
                        SwitchState(State.DONE);
                    }

                    break;
                }
            case State.DONE:
                {
                    break;
                }
            case State.COMPLETED:
                {
                    break;
                }
            default:
                {
                    Debug.LogError("Unknown state in Accumualtor");
                    break;
                }
        }
	}

    public void SwitchState(State desiredState)
    {
        if (desiredState == currentState)
            return;

        switch (desiredState)
        {
            case State.NULL:
                {
                    break;
                }
            case State.NEXT:
                {
                    if (OnNext != null)
                        OnNext();

                    operandLabel.gameObject.SetActive(false);

                    if (titles.Count <= 0)
                    {
                        SwitchStateDelayed(1, State.COMPLETED);

                        break;
                    }

                    currentOperand = operands.Dequeue();
                    currentOperator = operators.Dequeue();

                    SetOperand(titles.Dequeue(), currentOperand);

                    totalStart = total;
                    operandStart = currentOperand;

                    SwitchStateDelayed(1, State.DISPLAY);

                    break;
                }
            case State.DISPLAY:
                {
                    operandLabel.gameObject.SetActive(true);

                    if (OnDisplay != null)
                        OnDisplay();

                    SwitchStateDelayed(1, State.OPERATE);

                    break;
                }
            case State.OPERATE:
                {
                    operateTimePassed = 0;

                    break;
                }
            case State.DONE:
                {
                    Operate(1);

                    //currentOperand = 0;

                    SetLabelsToCurrent();

                    SwitchStateDelayed(1, State.NEXT);
                    break;
                }
            case State.COMPLETED:
                {
                    if (OnCompleted != null)
                        OnCompleted();

                    break;
                }
            default:
                {
                    Debug.LogError("Unknown state in Accumualtor");
                    break;
                }
        }

        currentState = desiredState;
    }

    private void Operate(float fraction)
    {
        switch (currentOperator)
        {
            case Operator.ADD:
                {
                    total = (long)Mathf.Lerp(totalStart, totalStart + operandStart, fraction);
                    currentOperand = Mathf.RoundToInt(Mathf.Lerp(operandStart, 0, fraction));

                    break;
                }
            case Operator.MULTIPLY:
                {
                    total = Mathf.RoundToInt(Mathf.Lerp(totalStart, totalStart * operandStart, fraction));

                    break;
                }
            default:
                {
                    Debug.LogError("Unknown Operator in Accumulator");
                    break;
                }
        }
    }

    private void SetLabelsToCurrent()
    {
        SetTotal(total);
        SetOperand(currentOperand);
    }

    public void SetTotal(string label, long value)
    {
        totalLabel.title.text = label;
        SetTotal(value);
    }

    public void SetTotal(long value)
    {
        total = value;

        SetTotalValue(totalLabel.value, value);
    }

    private void SetTotalValue(Text t, long value)
    {
        t.text = String.Format("{0:N0}", value);
    }

    public void SetOperand(string label, float value)
    {
        operandLabel.title.text = label;
        SetOperand(value);
    }

    public void SetOperand(float value)
    {
        currentOperand = value;

        SetOperandValue(operandLabel.value, value);
    }

    private void SetOperandValue(Text t, float value)
    {
        t.text = String.Format("{0:N0}", value);
    }

    public void StopSwitchStateDelayed()
    {
        if (waitRoutine != null)
            StopCoroutine(waitRoutine);
    }

    public void SwitchStateDelayed(float waitDuration, State desiredPhase)
    {
        StopSwitchStateDelayed();

        waitRoutine = StartCoroutine(WaitAndSwitch(waitDuration, desiredPhase));
    }

    public IEnumerator WaitAndSwitch(float waitDuration, State desiredPhase)
    {
        yield return new WaitForSeconds(waitDuration);

        SwitchState(desiredPhase);
    }

    public void Skip()
    {
        switch (currentState)
        {
            case State.NULL:
                {
                    break;
                }
            case State.NEXT:
                {
                    break;
                }
            case State.DISPLAY:
                {
                    StopSwitchStateDelayed();

                    SwitchState(State.DONE);

                    break;
                }
            case State.OPERATE:
                {
                    StopSwitchStateDelayed();

                    SwitchState(State.DONE);

                    break;
                }
            case State.DONE:
                {
                    break;
                }
            case State.COMPLETED:
                {
                    break;
                }
            default:
                {
                    Debug.LogError("Unknown state in Accumualtor");
                    break;
                }
        }
    }

    public void AddOperand(string title, float value, Operator op)
    {
        titles.Enqueue(title);
        operands.Enqueue(value);
        operators.Enqueue(op);
    }
}
