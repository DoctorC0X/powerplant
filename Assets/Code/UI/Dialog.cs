﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Dialog : MonoBehaviour 
{
    public Text title;
    public Text message;

    public Image fader;
    public Image background;

    public bool _useButtons;
    public bool UseButtons
    {
        get { return _useButtons; }
        set
        {
            _useButtons = value;

            yesButton.gameObject.SetActive(_useButtons);
            noButton.gameObject.SetActive(_useButtons);
        }
    }

    public Button yesButton;
    public Button noButton;

    public float tapLockDuration;

    private bool _useFader;
    public bool UseFader
    {
        get { return _useFader; }
        set
        {
            _useFader = value;

            fader.enabled = _useFader;
        }
    }

    public void Init(bool useButt, bool useFade)
    {
        UseButtons = useButt;
        UseFader = useFade;
    }
}
