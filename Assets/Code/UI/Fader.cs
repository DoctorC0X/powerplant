﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Fader : MonoBehaviour 
{
    private static Fader _instance;

    public static Fader Instance
    {
        get { return _instance; }
    }

    public Image faderImage;

    public float fadeSpeed;
    public bool fading;

    private float fadeStart;
    private float fadeEnd;

    private float passedTime;

    public event Action OnFadeEnd;

    public bool autostart;

    void Awake()
    {
        _instance = this;

        faderImage = GetComponent<Image>();
    }

    void Start()
    {
        if (autostart)
        {
            faderImage.color = Color.black;

            gameObject.AddComponent<Coworker>().StartDelayedAction(0.5f, null, FadeIn);
        }
    }

    public void FadeIn()
    {
        fading = true;
        fadeStart = faderImage.color.a;
        fadeEnd = 0;

        passedTime = 0;

        OnFadeEnd += () => faderImage.raycastTarget = false;
    }

    public void FadeOut()
    {
        fading = true;
        fadeStart = faderImage.color.a;
        fadeEnd = 1;

        passedTime = 0;

        faderImage.raycastTarget = true;
    }

    void Update()
    {
        if (fading)
        {
            passedTime += Time.deltaTime;

            float fraction = (fadeSpeed * passedTime) / Mathf.Abs(fadeEnd - fadeStart);

            Color old = faderImage.color;
            faderImage.color = new Color(old.r, old.g, old.b, Mathf.Lerp(fadeStart, fadeEnd, fraction));

            if (fraction >= 1)
            {
                fading = false;

                if (OnFadeEnd != null)
                {
                    OnFadeEnd();
                    OnFadeEnd = null;
                }
            }
        }
    }
}
