﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class LoadingScreen : MonoBehaviour 
{
    public Image fader;
    public float fadeSpeed;
    private float currentFadeSpeed;
    private float currentAlphaBorder;
    public bool fading;

    public Text loadingTitle;

    public int sceneIndex;

    private event Action onDone;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        this.transform.position = Camera.main.transform.position - new Vector3(0, 0, -1);
    }

	// Use this for initialization
	void Start () 
    {

	}
	
	// Update is called once per frame
	void Update () 
    {
        if (fading)
        {
            Color c = fader.color;
            fader.color = new Color(c.r, c.g, c.b, c.a + currentFadeSpeed);

            float distance = Mathf.Abs(fader.color.a - currentAlphaBorder);

            if (distance <= fadeSpeed)
            {
                fader.color = new Color(c.r, c.g, c.b, 1);

                fading = false;

                onDone();
            }
        }
	}

    public void FadeIn()
    {
        fading = true;
        currentFadeSpeed = fadeSpeed;
        currentAlphaBorder = 1;

        onDone = () => loadingTitle.enabled = true;
        onDone += () => StartCoroutine(LoadLevel());
    }

    public void FadeOut()
    {
        fading = true;
        currentFadeSpeed = -1 * fadeSpeed;
        currentAlphaBorder = 0;

        loadingTitle.enabled = false;

        onDone = () => Destroy(this.gameObject);
    }

    private IEnumerator LoadLevel()
    {
        AsyncOperation async = Application.LoadLevelAsync(sceneIndex);
        yield return async;

        yield return new WaitForSeconds(0.3f);

        FadeOut();
    }
}
