﻿using UnityEngine;
using System.Collections;

public class ViewportOffset : MonoBehaviour 
{
    public Vector2 offsetInCameraViewportSize;

	// Use this for initialization
	void Start () 
    {
        if (MenuCamera.Instance == null)
        {
            Debug.LogError("Viewport Offset couldnt find CameraBehaviour_Menu. So it disabled itself");
            this.gameObject.SetActive(false);
        }

        Vector2 offset = Utility.MultiplyComponents(offsetInCameraViewportSize, MenuCamera.Instance.viewPortSize);

        this.transform.position += new Vector3(offset.x, offset.y, this.transform.position.z);
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
