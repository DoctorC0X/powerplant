﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelBar : MonoBehaviour 
{
    public Text text;

    public RectTransform background;
    public RectTransform oldBar;
    public RectTransform newBar;

    private float totalWidth;

    private float maxValue;
    private float oldValue;
    private float newValue;

    public float newBarMoveSpeed;
    private float newBarTarget;
    private float passedTime;
    private bool interpolating;

    public void Init(float maxValue, float oldValue, float newValue, string text)
    {
        this.maxValue = maxValue;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.text.text = text;

        totalWidth = background.rect.width;

        SetOldBar(oldValue, maxValue);
        newBar.sizeDelta = new Vector2(0, 0);
    }

    void Update()
    {
        if (interpolating)
        {
            passedTime += Time.deltaTime;

            float fraction = (passedTime * newBarMoveSpeed) / newBarTarget;

            newBar.sizeDelta = new Vector2(Mathf.Lerp(0, newBarTarget, fraction), 0);

            if (fraction >= 1)
            {
                interpolating = false;
            }
        }
    }

    private void SetOldBar(float oldValue, float maxValue)
    {
        oldBar.sizeDelta = new Vector2(totalWidth * (oldValue/maxValue), 0);
    }

    public void StartInterpolateNewBar()
    {
        newBar.sizeDelta = new Vector2(0, 0);

        float fraction = newValue / maxValue;

        newBarTarget = (totalWidth * fraction) - oldBar.sizeDelta.x;
        passedTime = 0;
        interpolating = true;
    }
}
