﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IngredientCount : MonoBehaviour 
{
    public Ingredient ingredient;

    public Text text;

    public void Init(Ingredient i)
    {
        this.ingredient = i;

        if (text == null)
            text = GetComponent<Text>();

        ingredient.OnAmountChange += UpdateText;
        UpdateText();
    }

    public void UpdateText()
    {
        text.text = ingredient.Amount.ToString();
    }
}
