﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IngredientTile : MonoBehaviour 
{
    private IngredientSlot startSlot;
    private Image icon;

    public IngredientSlot currentSlot;
    public Ingredient ingredient;

    public Vector2 sizeDelta;

    void Awake()
    {
        sizeDelta = GetComponent<RectTransform>().sizeDelta;
    }

    public void Init(IngredientSlot start, Ingredient i)
    {
        startSlot = currentSlot = start;

        this.ingredient = i;

        this.icon = GetComponent<Image>();
        this.icon.sprite = this.ingredient.icon;
    }

    public void GoToStartSlot()
    {
        GoToSlot(startSlot);
    }

    public void GoToSlot(IngredientSlot s)
    {
        transform.parent = s.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;

        GetComponent<RectTransform>().sizeDelta = sizeDelta;

        currentSlot.currentTile = null;
        currentSlot = s;
        currentSlot.currentTile = this;        
    }
}
