﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IngredientView : MonoBehaviour 
{
    public GridView gridView;
    public GameObject slotPrefab;
    public GameObject tilePrefab;

    public IngredientSlot[] selectSlots;
    public IngredientSlot[] cauldronSlots;

	// Use this for initialization
	void Start () 
    {
        selectSlots = new IngredientSlot[IngredientManager.Instance.ingredients.Length];

        for (int i = 0; i < IngredientManager.Instance.ingredients.Length; i++)
        {
            Ingredient ingredient = IngredientManager.Instance.ingredients[i];

            IngredientSlot slot = CreateSlot();
            IngredientTile tile = CreateTile();

            gridView.AddTile(slot.gameObject);

            tile.Init(slot, ingredient);

            slot.Init(i, tile);
            slot.OnClickAction += OnSelectClick;

            slot.GetComponentInChildren<IngredientBuy>().Init(ingredient);
            slot.GetComponentInChildren<IngredientCount>().Init(ingredient);

            selectSlots[i] = slot;
        }

        for (int i = 0; i < cauldronSlots.Length; i++)
        {
            cauldronSlots[i].Init(i);
            cauldronSlots[i].OnClickAction += OnCauldronClick;
        }
	}

    private IngredientSlot CreateSlot()
    {
        GameObject slotGo = Instantiate<GameObject>(slotPrefab);

        return slotGo.GetComponent<IngredientSlot>();
    }

    private IngredientTile CreateTile()
    {
        GameObject tileGo = Instantiate<GameObject>(tilePrefab);
        return tileGo.GetComponent<IngredientTile>();
    }

    public void OnSelectClick(int index)
    {
        int slotIndex = IngredientManager.Instance.GetFreeCauldronSlot();

        if (slotIndex >= 0)
        {
            IngredientManager.Instance.PutInCauldronSlot(slotIndex, index);

            if(selectSlots[index].currentTile != null)
                selectSlots[index].currentTile.GoToSlot(cauldronSlots[slotIndex]);
        }
    }

    public void OnCauldronClick(int index)
    {
        if (cauldronSlots[index].currentTile != null)
        {
            IngredientManager.Instance.FreeCauldronSlot(index);

            cauldronSlots[index].currentTile.GoToStartSlot();
        }
    }
}
