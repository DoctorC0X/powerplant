﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class IngredientSlot : MonoBehaviour 
{
    public int index;

    public IngredientTile currentTile;

    public event Action<int> OnClickAction;

    public void Init(int index, IngredientTile tile = null)
    {
        this.index = index;

        if (tile != null)
        {
            this.currentTile = tile;

            currentTile.GoToStartSlot();
        }
    }

    public void OnClick()
    {
        if (OnClickAction != null)
            OnClickAction(index);
    }
}
