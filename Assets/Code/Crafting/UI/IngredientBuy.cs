﻿using UnityEngine;
using System.Collections;

public class IngredientBuy : MonoBehaviour 
{
    public Ingredient ingredient;

    public void Init(Ingredient i)
    {
        this.ingredient = i;

        if (!ingredient.buyable)
            gameObject.SetActive(false);
    }

    public void Buy()
    {
        IngredientManager.Instance.Buy(ingredient);
    }
}
