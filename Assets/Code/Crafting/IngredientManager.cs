﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class IngredientManager : MonoBehaviour 
{
    public static string gameFilePath = "Crafting/Ingredients";
    public static string saveFilePath = "IngredientSave";

    private static IngredientManager _instance;

    public static IngredientManager Instance
    {
        get { return _instance; }
    }

    public Ingredient[] ingredients;

    public int[,] recipes;

    public Ingredient[] cauldron;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
            return;
        }
        else
        {
            _instance = this;
        }

        // TODO: replace with preloader implementation
        ingredients = Resources.LoadAll<Ingredient>(gameFilePath);
        //

        recipes = new int[ingredients.Length, ingredients.Length];

        for (int i = 0; i < recipes.GetLength(0); i++)
            for (int j = 0; j < recipes.GetLength(1); j++)
                recipes[i, j] = -1;

        for (int i = 0; i < ingredients.Length; i++)
            ingredients[i].Init(i);

        for (int i = 0; i < ingredients.Length; i++)
        {
            if (ingredients[i].IsCraftable())
            {
                recipes[ingredients[i].child1.Index, ingredients[i].child2.Index] = ingredients[i].Index;
                recipes[ingredients[i].child2.Index, ingredients[i].child1.Index] = ingredients[i].Index;
            }
        }

        for (int i = 0; i < recipes.GetLength(0); i++)
            for (int j = 0; j < recipes.GetLength(1); j++)
                if(recipes[i,j] != null) Debug.Log(i + " " + j + " || " + recipes[i, j]);

        cauldron = new Ingredient[2];
    }

    public void Init(Ingredient[] ingredients)
    {
        this.ingredients = ingredients;
    }

    public int GetFreeCauldronSlot()
    {
        for (int i = 0; i < cauldron.Length; i++)
        {
            if (cauldron[i] == null)
            {
                return i;
            }
        }

        return -1;
    }

    public void PutInCauldronSlot(int slotIndex, int ingredientIndex)
    {
        cauldron[slotIndex] = ingredients[ingredientIndex];
    }

    public void FreeCauldronSlot(int slotIndex)
    {
        cauldron[slotIndex] = null;
    }

    public bool IsCraftable()
    {
        if (cauldron[0] != null && cauldron[1] != null)
        {
            if (cauldron[0].Amount > 0 && cauldron[1].Amount > 0)
            {
                try
                {
                    int result = recipes[cauldron[0].Index, cauldron[1].Index];

                    if(result != -1)
                        return true;
                }
                catch (IndexOutOfRangeException ioore)
                {
                    Debug.Log("Recipe " + cauldron[0].Index + ", " + cauldron[1].Index + " not found");
                }
            }
        }

        return false;
    }

    public void Craft()
    {
        if (IsCraftable())
        {
            cauldron[0].Amount--;
            cauldron[1].Amount--;

            int result = recipes[cauldron[0].Index, cauldron[1].Index];
            ingredients[result].Amount++;
        }
    }

    public void Buy(Ingredient i)
    {
        if (i.buyable)
        {
            PlayerProfile.Instance.DebitStarDust(i.starDustPrice);

            i.Amount++;
        }
    }

    public void Buy(int index)
    {
        Buy(ingredients[index]);
    }
}
