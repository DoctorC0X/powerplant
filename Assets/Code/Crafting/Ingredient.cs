﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class Ingredient : ScriptableObject 
{
    private int index;
    public string title;
    public int rank;

    public bool buyable;
    public int starDustPrice;

    protected List<Ingredient> parents;

    public Ingredient child1;
    public Ingredient child2;

    public Sprite icon;

    public event Action OnAmountChange;

    private int amount;

    public int Amount
    {
        get { return amount; }
        set
        {
            amount = value;

            if (OnAmountChange != null)
                OnAmountChange();
        }
    }

    public int Index
    {
        get { return index; }
    }

    void Awake()
    {
        parents = new List<Ingredient>();
    }

    public void Init(int index)
    {
        this.index = index;

        if(child1 != null)
            child1.parents.Add(this);

        if(child2 != null)
            child2.parents.Add(this);
    }

    public bool IsCraftable()
    {
        return (child1 != null && child2 != null);
    }
}
