﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using EventSystem;
using Progress;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    public static GameManager Instance
    {
        get { return instance; }
    }

    public List<GameOverListener> gameOverListener;

    public bool gameActive;
    public bool gameOver;
    public bool paused;

    public List<PauseListener> pauseListener;

    private float timeCount;

    public event Action OnSceneChange;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            DestroyImmediate(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        timeCount = 0;
        gameActive = false;

        pauseListener = new List<PauseListener>();
        gameOverListener = new List<GameOverListener>();
    }

	// Use this for initialization
	void Start () 
    {
        EventManager.Instance.AddHandler(EventData.ID.GAMESTART, (EventData.ParamBase b) => gameActive = true);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKey(KeyCode.Escape) && Application.isMobilePlatform)
        {
            Application.Quit();
        }

        if (gameActive && !paused)
        {
            timeCount += Time.deltaTime;

            if (timeCount >= 1)
            {
                timeCount = 0;

                EventManager.Instance.CallEvent(EventData.ID.TIME);
            }
        }
	}

    public void TriggerGameOver()
    {
        if (!gameOver)
        {
            gameOver = true;
            gameActive = false;

            Debug.Log("Game Over");

            Time.timeScale = 1;

            foreach (GameOverListener current in gameOverListener)
            {
                current.GameOver();
            }

            ValueManager.Instance.TriggerRoundEnd();

            PersistenceManager.Instance.Save();

            //CreateGameoverOverlay();
        }
    }

    public void LoadGameScene()
    {
        LoadScene(Scenes.main.Key);
    }

    public void LoadShopScene()
    {
        LoadScene(Scenes.shop.Key);
    }

    public void LoadPreloadScene()
    {
        MainSceneLoader.preMainScenesLoaded = false;

        LoadScene(Scenes.loading.Key);
    }

    private void LoadScene(int index)
    {
        gameOverListener = new List<GameOverListener>();

        if (OnSceneChange != null)
        {
            OnSceneChange();
            OnSceneChange = null;
        }

        if (Fader.Instance != null)
        {
            Fader.Instance.FadeOut();
            Fader.Instance.OnFadeEnd += () => SceneManager.LoadScene(index);
        }
        else
        {
            SceneManager.LoadScene(index);
        }
    }

    public void ClearAllData()
    {
        PlayerPrefs.DeleteAll();
    }

    public void SetPause(bool pause, bool useMenu)
    {
        if (paused != pause)
        {
            paused = pause;

            gameActive = !paused;

            foreach (PauseListener current in pauseListener)
            {
                current.SetPause(pause);
            }
        }
    }

    public void SetPause(bool pause)
    {
        SetPause(pause, true);
    }

    public void SetTimeScale(float scale)
    {
        Time.timeScale = scale;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private GameObject CreateOverlay(string path)
    {
        GameObject canvasGO;

        canvasGO = FollowCanvas.Instance.gameObject;

        GameObject overlay = Instantiate(Resources.Load(path)) as GameObject;
        overlay.transform.parent = canvasGO.transform;
        overlay.transform.localPosition = Vector3.zero;
        overlay.transform.localScale = Vector3.one;

        return overlay;
    }
}
