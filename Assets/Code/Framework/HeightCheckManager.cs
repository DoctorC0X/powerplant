﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using EventSystem;

public class HeightCheckManager : MonoBehaviour
{
    private static HeightCheckManager _instance;

    public static HeightCheckManager Instance
    {
        get { return _instance; }
    }

    // listeners are sorted by their height
    public SortedList<float, List<HeightListener>> heightListeners;

    private Action preCall;

    public float startY;

    public float CurrentFaceHeight
    {
        get { return PlantModel.MainPlant.plantFace.transform.position.y - startY; }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            _instance = this;
        }

        heightListeners = new SortedList<float, List<HeightListener>>(new HeightComparer());
    }

    void Start()
    {
        startY = PlantModel.MainPlant.transform.position.y;

        EventManager.Instance.AddHandler(EventData.ID.PIECES, OnGrowth);
    }

    public void OnGrowth(EventData.ParamBase param)
    {
        // remove obsolete
        if (preCall != null)
        {
            preCall();
            preCall = null;
        }

        // check height
        for(int i = 0; i < heightListeners.Count; i++)
        {
            if (heightListeners.Keys[i] < CurrentFaceHeight)
            {
                // call listener and then remove it
                CallListener(heightListeners.Keys[i]);
                RemoveAllFromHeight(heightListeners.Keys[i]);
            }
            else
            {
                Debug.DrawLine(new Vector3(0, PlantModel.MainPlant.plantFace.transform.position.y), new Vector3(0, heightListeners.Keys[i]), Color.green, 1);
                // no need to check other listeners, since their height will be greater (because of sorted list)
                break;
            }
        }
    }

    private void CallListener(float height)
    {
        foreach (HeightListener current in heightListeners[height])
        {
            current.HeightReached(height);
        }
    }

    public void AddWorldHeightListener(HeightListener listener, float height)
    {
        // if height is not registered yet
        if(!heightListeners.ContainsKey(height))
            heightListeners.Add(height, new List<HeightListener>());

        heightListeners[height].Add(listener);
    }

    // relative = face pos + height
    public void AddRelativeHeightListener(HeightListener listener, float height)
    {
        float worldHeight = CurrentFaceHeight + height;

        AddWorldHeightListener(listener, worldHeight);
    }

    public void RemoveAllFromHeight(float height)
    {
        preCall += () => heightListeners.Remove(height);
    }

    public void RemoveListener(HeightListener listener, float height)
    {
        preCall += () => heightListeners[height].Remove(listener);
    }
}

public class HeightComparer : IComparer<float>
{
    public int Compare(float x, float y)
    {
        return x.CompareTo(y);
    }
}
