﻿using UnityEngine;
using System.Collections;

public class ShadowCaster : MonoBehaviour 
{
    private SpriteRenderer spriteRenderer;

    public GameObject shadow;

    void Awake()
    {
        spriteRenderer = this.GetComponent<SpriteRenderer>();

        if (spriteRenderer == null)
        {
            Debug.Log("shadow caster hasn't found the spriteRenderer");
            this.enabled = false;
        }
    }

	void Start () 
    {
        CreateShadow();
	}
	
	void Update () 
    {
	
	}

    public void CreateShadow()
    {
        if (shadow != null)
            Destroy(shadow);

        shadow = new GameObject("shadow");

        shadow.transform.parent = this.transform;

        Vector3 lossyScale = this.transform.lossyScale;
        shadow.transform.localPosition = new Vector3(ShadowManager.Instance.shadowPos.x / lossyScale.x, ShadowManager.Instance.shadowPos.y / lossyScale.y, ShadowManager.Instance.shadowPos.z / lossyScale.z);

        shadow.transform.localEulerAngles = Vector3.zero;
        shadow.transform.localScale = Vector3.one;

        SpriteRenderer shadowRenderer = shadow.AddComponent<SpriteRenderer>();
        shadowRenderer.sprite = spriteRenderer.sprite;
        shadowRenderer.material = spriteRenderer.material;
        shadowRenderer.color = ShadowManager.Instance.shadowColor;
    }
}
