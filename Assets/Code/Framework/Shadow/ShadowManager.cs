﻿using UnityEngine;
using System.Collections;

public class ShadowManager : MonoBehaviour 
{
    private static ShadowManager _instance;

    public static ShadowManager Instance
    {
        get { return _instance; }
    }

    public Vector3 shadowPos;

    public Color shadowColor;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            DestroyImmediate(this.gameObject);
            return;
        }
        else
        {
            _instance = this;
        }
    }

	void Start () 
    {
	
	}
	
	void Update () 
    {
	
	}
}
