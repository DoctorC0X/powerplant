﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Uninspired.Core.IO;

[Serializable]
public class PersistentDataContainer 
{
    public SerializableDictionary<string, PersistentData> dataDict;

    public PersistentDataContainer()
    {
        dataDict = new SerializableDictionary<string, PersistentData>();
    }
}
