﻿using Progress;
using Progress.Missions;
using System;
using System.Xml.Serialization;

[Serializable]
[XmlInclude(typeof(PlayerProfileData))]
[XmlInclude(typeof(ValueData))]
[XmlInclude(typeof(GoalData))]
[XmlInclude(typeof(MissionData))]
public abstract class PersistentData 
{

}
