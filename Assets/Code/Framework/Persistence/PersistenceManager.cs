﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Uninspired.Core.IO.Xml;
using System.IO;
using System;

public class PersistenceManager : MonoBehaviour 
{
    private static PersistenceManager _instance;

    public static PersistenceManager Instance
    {
        get { return _instance; }
    }

    public static string savePath = "Save";

    private PersistentDataContainer container;

    public bool initialised = false;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            _instance = this;
        }

        DontDestroyOnLoad(gameObject);

        Load();

        initialised = true;
    }

    public bool IsRegistered(string name)
    {
        return container.dataDict.ContainsKey(name);
    }

    public PersistentData GetData(string name)
    {
        return container.dataDict[name];
    }

    public void SetData(string name, PersistentData data)
    {
        if (IsRegistered(name))
            container.dataDict[name] = data;
        else
            container.dataDict.Add(name, data);
    }

    void OnApplicationQuit()
    {
        Save();
    }

    public void Save()
    {
        XmlSaveSerializer.SerializeObject<PersistentDataContainer>(PersistenceManager.savePath, container);
    }

    public void Load()
    {
        try
        {
            container = XmlSaveSerializer.DeserializeObject<PersistentDataContainer>(PersistenceManager.savePath);
        }
        catch (Exception e)
        {
            container = new PersistentDataContainer();
        }
    }
}
