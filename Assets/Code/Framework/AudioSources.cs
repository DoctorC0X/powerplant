﻿using UnityEngine;
using System.Collections;

public class AudioSources : MonoBehaviour 
{
    private static AudioSources _instance;

    public static AudioSources Instance
    {
        get { return _instance; }
    }

    public string playerPrefKey;

    public AudioSource music;
    public AudioSource menuTransition;
    public AudioSource missionComplete;
    public AudioSource missionClaim;
    public AudioSource starPieceCollect;
    public AudioSource closedodge;
    public AudioSource starDustShapeBonus;
    public AudioSource[] starDustCollect;
    public AudioSource starDustRocket_launch;
    public AudioSource starDustRocket_explode;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
            return;
        }
        else
        {
            _instance = this;
        }

        SetMuted(IsMuted());
    }

    public void SetMuted(bool muted)
    {
        PlayerPrefs.SetInt(playerPrefKey, muted ? 1 : 0);

        Camera.main.GetComponent<AudioListener>().enabled = !muted;

        foreach (AudioSource source in GetComponentsInChildren<AudioSource>())
        {
            source.enabled = !muted;
        }
    }

    public bool IsMuted()
    {
        if(!PlayerPrefs.HasKey(playerPrefKey))
            PlayerPrefs.SetInt(playerPrefKey, 0);

        return PlayerPrefs.GetInt(playerPrefKey) == 1;
    }
}
