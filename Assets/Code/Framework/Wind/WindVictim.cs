﻿using UnityEngine;
using System.Collections;

public class WindVictim : MonoBehaviour, GameOverListener, PauseListener
{
    public float maxAcceleration;

    private bool paused;
    private Vector2 linearVelocity;
    private float angularVelocity;

    private bool gameOver;

	// Use this for initialization
	void Start () 
    {
        gameOver = false;

        if (GameManager.Instance != null)
        {
            GameManager.Instance.gameOverListener.Add(this);
            GameManager.Instance.pauseListener.Add(this);

            SetPause(GameManager.Instance.paused);
        }

        if (this.GetComponent<Rigidbody2D>() == null)
        {
            Debug.LogError("windVictim has no rigidbody2D!");
            this.enabled = false;
        }
	}

    void FixedUpdate()
    {
        if (!paused)
        {
            //apply wind force
            if (WindManager.Instance != null && this.GetComponent<Rigidbody2D>() != null && !gameOver)
            {
                Vector2 force = new Vector2(WindManager.Instance.currentWindMultiplier * maxAcceleration * this.GetComponent<Rigidbody2D>().mass, 0);

                if (this.GetComponent<Rigidbody2D>() != null && !float.IsNaN(force.x) && !float.IsNaN(force.y))
                {
                    this.GetComponent<Rigidbody2D>().AddForce(force);
                    Debug.DrawRay(this.transform.position, force / 100, Color.white);
                }
            }
        }
    }

	// Update is called once per frame
	void Update () 
    {
	
	}

    public void GameOver()
    {
        gameOver = true;
    }

    void OnDestroy()
    {
        if (GameManager.Instance != null)
        {
            GameManager.Instance.gameOverListener.Remove(this);
            GameManager.Instance.pauseListener.Remove(this);
        }
    }

    public void SetPause(bool pause)
    {
        paused = pause;

        if (pause)
        {
            Rigidbody2D rb = this.GetComponent<Rigidbody2D>();

            linearVelocity = rb.velocity;
            angularVelocity = rb.angularVelocity;

            rb.isKinematic = true;
        }
        else
        {
            Rigidbody2D rb = this.GetComponent<Rigidbody2D>();

            rb.isKinematic = false;

            rb.velocity = linearVelocity;
            rb.angularVelocity = angularVelocity;
        }
    }

    public bool IsPaused()
    {
        return paused;
    }
}
