﻿using UnityEngine;
using System.Collections;

public class WindManager : MonoBehaviour, PauseListener
{
    private static WindManager instance;

    public static WindManager Instance
    {
        get { return instance; }
    }

    public enum WindMode { DETERMINISTIC, RANDOM };
    public WindMode currentMode;

    public float timerToChange;
    public float timerToChangeMax;

    public float currentWindMultiplier;

    [Range(-1, 1)]
    public float windMultiplierMax;

    public float changeSpeed;

    public bool lockWind;

    private float newValue;
    private float oldValue;
    private float startTime;
    private float difference;
    private bool valueReached;

    private bool paused;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        changeSpeed = Mathf.Clamp(changeSpeed, 0.000001f, 1);
    }

    void Start()
    {
        timerToChange = 0;

        oldValue = 0;

        if (currentMode == WindMode.RANDOM)
        {
            newValue = Random.Range(-windMultiplierMax, windMultiplierMax);
        }
        else
        {
            newValue = Random.value > 0.5f ? windMultiplierMax : -windMultiplierMax;
        }

        difference = Mathf.Abs(newValue - oldValue);
        startTime = Time.time;
        valueReached = false;

        if (GameManager.Instance != null)
            GameManager.Instance.pauseListener.Add(this);
    }

	// Update is called once per frame
	void Update () 
    {
        if (!paused)
        {
            Debug.DrawRay(this.transform.position + new Vector3(0, -3, 0), new Vector3(currentWindMultiplier, 0, 0) / 100, Color.cyan);

            timerToChange += Time.deltaTime;

            if (timerToChange > timerToChangeMax && !lockWind)
            {
                SetWindDir(currentWindMultiplier * -1);
            }

            if (!valueReached)
            {
                float traveled = (Time.time - startTime) * changeSpeed;

                float fraction = traveled / difference;

                currentWindMultiplier = Mathf.Lerp(oldValue, newValue, fraction);

                if (fraction > 1)
                {
                    valueReached = true;
                }
            }
        }
	}

    public void ChangeWindDir()
    {
        timerToChange = timerToChangeMax + 1;
    }

    public void SetWindDir(float dir)
    {
        dir = Mathf.Clamp(dir, -windMultiplierMax, windMultiplierMax);

        timerToChange = 0;

        oldValue = currentWindMultiplier;

        if (currentMode == WindMode.RANDOM)
        {
            newValue = Random.Range(-windMultiplierMax, windMultiplierMax);
        }
        else
        {
            newValue = dir;
        }

        difference = Mathf.Abs(newValue - oldValue);
        startTime = Time.time;
        valueReached = false;
    }

    public void SetPause(bool pause)
    {
        paused = pause;
    }

    public bool IsPaused()
    {
        return paused;
    }
}
