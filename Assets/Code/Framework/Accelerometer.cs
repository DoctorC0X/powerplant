﻿using UnityEngine;
using System.Collections;
using System;

public class Accelerometer : MonoBehaviour 
{
    private Vector3 lastAcceleration;

    public float maxThreshold;

    public float updateTimerMax;
    private float updateTimer;

    public bool activated;

	// Use this for initialization
	void Start () 
    {
        lastAcceleration = Input.acceleration;
        updateTimer = updateTimerMax;

        activated = Application.isMobilePlatform;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (activated)
        {
            updateTimer -= Time.deltaTime;

            if (updateTimer <= 0)
            {
                updateTimer = updateTimerMax;

                if ((Input.acceleration - lastAcceleration).sqrMagnitude > (maxThreshold * maxThreshold))
                {
                    Trigger();
                }

                lastAcceleration = Input.acceleration;
            }
        }
	}

    public void Trigger()
    {
        Debug.Log("shake shake " + Time.time);
        PlantModel.MainPlant.skillManager.PerformSkill();
    }
}
