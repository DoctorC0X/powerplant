﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ClickManager : MonoBehaviour 
{
    private static ClickManager _instance;

    public static ClickManager Instance
    {
        get { return _instance; }
    }

    public SwipeInfo info;

    public float swipeLengthMin;
    public float swipeLengthMax;

    private Vector2 lastMousePos;

    private Vector2 start;
    private Vector2 end;

    private float distX;
    private float distY;

    private bool pointerDown;
    private bool isSwipe;
    private bool swipeHandled;

    public event Action OnPress;
    public event Action<bool> OnRelease;
    public event Action<SwipeInfo> OnSwipe;
    public event Action<SwipeInfo> OnSwipeDone;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            _instance = this;
        }
    }

    void Start()
    {

    }

    void Update()
    {
        if (pointerDown)
        {
            Swipe();
        }
    }

    public void MouseDown()
    {
        pointerDown = true;
        isSwipe = false;
        swipeHandled = false;

        lastMousePos = Input.mousePosition;

        info = new SwipeInfo();
        //info.startScreenPos = Input.mousePosition;

        distX = 0;
        distY = 0;

        if (OnPress != null)
            OnPress();
    }

    public void MouseUp()
    {
        pointerDown = false;

        if(OnRelease != null)
            OnRelease(isSwipe);

        if (isSwipe && !swipeHandled)
        {
            //info.endScreenPos = Input.mousePosition;
            CallSwipeListener();
        }
    }

    public void Swipe()
    {
        if (!swipeHandled)
        {
            Vector2 current = Input.mousePosition;

            distX += current.x - lastMousePos.x;
            distY += current.y - lastMousePos.y;

            info.swipeDir = new Vector2(distX / Screen.width, distY / Screen.height);

            info.fraction = (info.swipeDir.magnitude - swipeLengthMin) / (swipeLengthMax - swipeLengthMin);

            lastMousePos = current;

            if (info.swipeDir.sqrMagnitude >= (swipeLengthMin * swipeLengthMin))
            {
                if(OnSwipe != null)
                    OnSwipe(info);

                isSwipe = true;

                if (info.swipeDir.sqrMagnitude >= (swipeLengthMax * swipeLengthMax))
                {
                    //info.endScreenPos = Input.mousePosition;
                    CallSwipeListener();
                }
            }
        }
    }

    private void CallSwipeListener()
    {
        swipeHandled = true;

        if (OnSwipeDone != null)
            OnSwipeDone(info);
    }

    public struct SwipeInfo
    {
        public Vector2 swipeDir;
        public float fraction;

        //public Vector2 startScreenPos;
        //public Vector2 endScreenPos;

        public SwipeInfo(Vector2 dir, float f)
        {
            //startScreenPos = s;
            //endScreenPos = e;

            swipeDir = dir;
            fraction = f;
        }
    }
}
