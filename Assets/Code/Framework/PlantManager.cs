﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlantManager : MonoBehaviour 
{
    private static PlantManager _instance;
    public static PlantManager Instance
    {
        get { return _instance; }
    }

    public PrefabContainer plants;

    public int ChosenPlantIndex
    {
        get { return PlayerPrefs.GetInt("ChosenPlant"); }
        set
        {
            PlayerPrefs.SetInt("ChosenPlant", value);
        }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            DestroyImmediate(this.gameObject);
            return;
        }
        else
        {
            _instance = this;
        }

        if(plants == null)
            plants = this.GetComponent<PrefabContainer>();

        DontDestroyOnLoad(this.gameObject);
    }

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
