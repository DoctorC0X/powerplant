﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrefabContainer : MonoBehaviour, IEnumerable
{
    public string folderName;
    public bool loadAutomatically;
    public List<GameObject> prefabs;
    
    public GameObject this[int i]
    {
        get { return prefabs[i]; }
    }

    public int Count
    {
        get { return prefabs.Count; }
    }

    void Awake()
    {
        if (loadAutomatically)
        {
            prefabs = new List<GameObject>();

            prefabs.AddRange(Resources.LoadAll<GameObject>(folderName));
        }
    }

	// Use this for initialization
	void Start () 
    {
        
	}

    public IEnumerator GetEnumerator()
    {
        return new PrefabContainerEnum(prefabs.ToArray());
    }
}

public class PrefabContainerEnum : IEnumerator
{
    public GameObject[] prefabs;

    private int position = -1;

    public PrefabContainerEnum(GameObject[] list)
    {
        prefabs = list;
    }

    public object Current
    {
        get { return prefabs[position]; }
    }

    public bool MoveNext()
    {
        position++;
        return (position < prefabs.Length);
    }

    public void Reset()
    {
        position = -1;
    }
}

