﻿using EventSystem;
using System;
using System.Xml.Serialization;
using UnityEngine;

namespace Progress
{
    [Serializable]
    public class Value
    {
        public EventData.ID id;

        [XmlIgnore]
        public int currentRound;
        public int lastRound;
        public int record;
        public int total;

        private Value()
        {
        }

        public Value(EventData.ID id)
        {
            this.id = id;

            this.currentRound = 0;
            this.lastRound = 0;
            this.record = 0;
            this.total = 0;
        }
        
        public void Increase(int amount)
        {
            amount = Mathf.Clamp(amount, 0, int.MaxValue);

            currentRound += amount;
            total += amount;            
        }

        public void RoundEnd()
        {
            ApplyRecord();
            ApplyLastRound();
        }

        private void ApplyLastRound()
        {
            lastRound = currentRound;
            currentRound = 0;
        }

        private void ApplyRecord()
        {
            if (currentRound > record)
                record = currentRound;
        }
    }
}