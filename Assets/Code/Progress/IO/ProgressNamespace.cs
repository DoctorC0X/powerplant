﻿using UnityEngine;
using System.Collections;

namespace Progress.IO
{
    public class ProgressNamespace
    {
        public static string progressUri = "http://www.w3.org/2001/Progress";
        public static string progressPrefix = "prgs";
    }
}
