﻿using System.Xml;
using System.Collections.Generic;
using Uninspired.Core.IO.Xml;
using EventSystem;

namespace Progress.IO
{
    public class GoalFactory
    {
        public Goal Create(string fileName, string fileContent)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(fileContent);

            XmlNode root = xDoc.DocumentElement;

            return Create(fileName, root);
        }

        public Goal Create(string fileName, XmlNode node)
        {
            ProgressSelectorFactory selectorFactory = new ProgressSelectorFactory();
            XmlNodeSelector selector = selectorFactory.Create(node.OwnerDocument.NameTable);

            string title = selector.SelectSingleByXPath(node, "title").InnerXml;
            string description = selector.SelectSingleByXPath(node, "description").InnerXml;

            Progress.Goal.UnlockType type = selector.SelectSingleByXPath(node, "unlockType").AsEnum<Progress.Goal.UnlockType>();

            List<Subgoal> subgoals = new List<Subgoal>();
            SubgoalFactory subgoalFactory = new SubgoalFactory();

            XmlNodeWrapper[] subNodes = selector.SelectByXPath(node, "subgoals", "subgoal");
            for (int i = 0; i < subNodes.Length; i++)
            {
                Subgoal s = subgoalFactory.Create(subNodes[i].Node);

                subgoals.Add(s);
            }

            return new Goal(fileName, title, description, type, subgoals.ToArray());
        }
    }

    public class SubgoalFactory
    {
        public Subgoal Create(XmlNode node)
        {
            ProgressSelectorFactory selectorFactory = new ProgressSelectorFactory();
            XmlNodeSelector selector = selectorFactory.Create(node.OwnerDocument.NameTable);

            int startValue = selector.SelectSingleByXPath(node, "startValue").AsInt();
            bool saveProgress = selector.SelectSingleByXPath(node, "saveProgress").AsBool();

            EventHandlerFactory handlerFactory = new EventHandlerFactory();

            EventHandler increaseEvent = handlerFactory.Create(selector.SelectSingleByXPath(node, "increaseID").Node);
            EventHandler decreaseEvent = handlerFactory.Create(selector.SelectSingleByXPath(node, "decreaseID").Node);

            bool invertGoal = selector.SelectSingleByXPath(node, "invertGoal").AsBool();
            bool resetOnExit = selector.SelectSingleByXPath(node, "resetOnStateExit").AsBool();

            EventStateFactory stateFactory = new EventStateFactory();

            XmlNode stateNode = selector.SelectSingleByXPath(node, "states").Node;
            EventState desiredState = stateFactory.Create(stateNode, selector);

            return new Subgoal(startValue, saveProgress, invertGoal, resetOnExit, increaseEvent, decreaseEvent, desiredState);
        }
    }
}