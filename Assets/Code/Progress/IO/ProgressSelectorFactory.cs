﻿using System.Xml;
using Uninspired.Core.IO.Xml;

namespace Progress.IO
{
    public class ProgressSelectorFactory : AbstractXmlNodeSelectorFactory
    {
        public override XmlNodeSelector Create(XmlNameTable nameTable)
        {
            return new XmlNodeSelector(nameTable, ProgressNamespace.progressUri, ProgressNamespace.progressPrefix);
        }
    }
}