﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EventSystem;

public class MilestoneManager : MonoBehaviour
{
    private static MilestoneManager _instance;

    public static MilestoneManager Instance
    {
        get { return _instance; }
    }

    public Dictionary<EventData.ID, int> milestones;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
            return;
        }
        else
        {
            _instance = this;
        }

        milestones = new Dictionary<EventData.ID, int>();

        milestones.Add(EventData.ID.TIME, 300);
        milestones.Add(EventData.ID.HEIGHT, 500);
        milestones.Add(EventData.ID.BULLET_DODGE, 10);
        milestones.Add(EventData.ID.CRITTER_DELIVERED, 25);
        milestones.Add(EventData.ID.STARDUST_BONUS, 25);
        milestones.Add(EventData.ID.STARDUST_COLLECTED, 1000);
        milestones.Add(EventData.ID.STARPIECES_DELIVERED, 5);
        milestones.Add(EventData.ID.MISSION_CLAIMED, 10);
    }

    public int GetNextMilestone(EventData.ID id, int value)
    {
        int milestone = milestones[id];

        while (milestone <= value)
            milestone += milestone;

        return milestone;
    }

    public int GetPreviousMilestone(EventData.ID id, int value)
    {
        int milestone = GetNextMilestone(id, value);

        return (milestone - milestones[id]);
    }
}
