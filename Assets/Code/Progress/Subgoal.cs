﻿using EventSystem;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace Progress
{
    [System.Serializable]
    public class Subgoal
    {
        [System.NonSerialized]
        protected Goal master;

        public int currentValue;

        [XmlIgnore]
        public int startValue;

        public bool isReached;
        protected bool saveProgress;

        protected EventHandler increaseEvent;
        protected EventHandler decreaseEvent;

        protected EventState desiredState;

        protected bool invertGoal;
        protected bool resetOnExit;

        private Subgoal()
        {
        }

        public Subgoal(int startValue, bool saveProgress, bool invertGoal, bool resetOnExit,
            EventHandler increaseEventId, EventHandler decreaseEventId, EventState desiredState)
        {
            this.startValue = startValue;
            this.currentValue = startValue;
            this.saveProgress = saveProgress;
            this.invertGoal = invertGoal;
            this.resetOnExit = resetOnExit;

            this.increaseEvent = increaseEventId;
            this.decreaseEvent = decreaseEventId;

            this.desiredState = desiredState;

            this.isReached = false;
        }

        public virtual void Apply(Subgoal s)
        {
            this.isReached = s.isReached;
            this.currentValue = s.currentValue;
        }

        public virtual void Init(Goal master)
        {
            this.master = master;

            if (!saveProgress)
                currentValue = startValue;

            increaseEvent.SetOnEventAction(Increase);
            decreaseEvent.SetOnEventAction(Decrease);

            desiredState.Init();
            desiredState.OnStateActiveChange += (bool active) =>
            {
                if (!active && resetOnExit)
                    currentValue = startValue;
            };
        }

        [XmlIgnore]
        public int CurrentValue
        {
            get { return currentValue; }
            set
            {
                currentValue = value;

                CheckReached();
            }
        }

        public virtual void CheckReached()
        {
            isReached = (currentValue <= 0) ^ invertGoal;

            if (isReached)
                master.CheckUnlocked();
        }

        public virtual void Increase(EventData.ParamBase param)
        {
            if (desiredState.Active)
                CurrentValue += param.increase;
        }

        public virtual void Decrease(EventData.ParamBase param)
        {
            if (desiredState.Active)
                CurrentValue -= param.increase;
        }

        public virtual void RegisterOnEventManager()
        {
            increaseEvent.Register();
            decreaseEvent.Register();
            desiredState.Register();
        }

        public virtual void DeregisterOnEventManager()
        {
            increaseEvent.Deregister();
            decreaseEvent.Deregister();
            desiredState.Deregister();
        }

        public void ReadSaveFile(XmlNode parent)
        {
            try
            {
                if(saveProgress)
                    currentValue = int.Parse(parent.Attributes["value"].Value);

                isReached = bool.Parse(parent.Attributes["isReached"].Value);
            }
            catch (System.Exception e)
            {
                Debug.LogWarning("Subgoal: error when reading save file. falling back to default. msg: " + e.Message + "\n" + e.StackTrace);
                currentValue = startValue;
            }
        }

        public void WriteSaveFile(XmlDocument xDoc, XmlElement parent)
        {
            XmlElement subgoalElement = xDoc.CreateElement("subgoal");

            subgoalElement.SetAttribute("value", currentValue.ToString());
            subgoalElement.SetAttribute("isReached", isReached.ToString());

            parent.AppendChild(subgoalElement);
        }

        public int GetStartValue()
        {
            return startValue;
        }

        public bool IsReached()
        {
            return isReached;
        }
    }
}