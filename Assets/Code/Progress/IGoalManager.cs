﻿using UnityEngine;
using System.Collections;
using Progress.IO;

namespace Progress
{
    public interface IGoalManager
    {
        void Unlocked(Goal goal);
    }
}