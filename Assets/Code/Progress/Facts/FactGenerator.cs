﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Progress;
using System;
using System.Linq;
using UnityEngine.UI;
using EventSystem;

namespace Progress.Facts
{
    public class FactGenerator : MonoBehaviour
    {
        public enum Type { RECORD, MILESTONE, LASTROUND, TOTAL };

        public List<EventData.ID> ids;
        public List<int> idKudos;
        
        public List<IFactEvent> events;

        public float idKudosWeight;
        public float eventKudosWeight;
        public float valueKudosWeight;

        public float lowestKudosBorder;

        public bool test;
        public Text text;

        private Dictionary<EventData.ID, EventIDString> idStrings;

        public string eventStringPath;

        public string fallbackString;
        public Color activityColor;
        public Color differenceColor;
        public Color newColor;

        void Start()
        {
            idStrings = new Dictionary<EventData.ID, EventIDString>();

            foreach(EventIDString current in Resources.LoadAll<EventIDString>(eventStringPath))
                idStrings.Add(current.id, ScriptableObject.Instantiate<EventIDString>(current));

            if(test)
                EventManager.Instance.AddHandler(EventData.ID.GAMESTART, () => StartCoroutine(Testing()));
        }

        public string GenerateFact()
        {            
            IFactEvent factEvent = null;
            Value factValue = null;

            float highestKudos = 0;

            for(int idIndex = 0; idIndex < ids.Count; idIndex++)
            {
                Value value = ValueManager.Instance.Values[ids[idIndex]];

                if (ids.Contains(value.id))
                {
                    for (int eventIndex = 0; eventIndex < events.Count; eventIndex++)
                    {
                        float kudos = 0; 
                        kudos += idKudosWeight * idKudos[idIndex];
                        kudos += eventKudosWeight * events[eventIndex].GetEventKudos();
                        kudos += valueKudosWeight * events[eventIndex].CalcValueKudos(value);

                        bool diceRoll = false;

                        if (kudos == highestKudos)
                            diceRoll = (UnityEngine.Random.value >= 0.5f);
                        
                        if (kudos > highestKudos || diceRoll)
                        {
                            highestKudos = kudos;
                            factEvent = events[eventIndex];
                            factValue = value;
                        }
                    }
                }
            }

            EventIDString idString = null;

            idStrings.TryGetValue(factValue.id, out idString);

            if (highestKudos > lowestKudosBorder && idString != null)
                return factEvent.GenerateFact(idString.activity, factValue, activityColor, differenceColor, newColor);
            else
                return FallbackCreateTotalFact();
        }

        private string FallbackCreateTotalFact()
        {
            Value bestValue = null;
            float highestTotalRatio = 0;

            for (int i = 0; i < ids.Count; i++)
            {
                Value value = ValueManager.Instance.Values[ids[i]];

                if (value.total > 0)
                {
                    float totalRatio = (float)value.currentRound / (float)value.total;

                    bool diceRoll = false;

                    if (totalRatio == highestTotalRatio)
                        diceRoll = (UnityEngine.Random.value >= 0.5f);

                    if (totalRatio > highestTotalRatio || diceRoll)
                    {
                        highestTotalRatio = totalRatio;
                        bestValue = value;
                    }
                }
            }

            EventIDString idString = null;

            idStrings.TryGetValue(bestValue.id, out idString);

            if (highestTotalRatio > 0 && idString != null)
            {
                string noun = Utility.AddColorTags(idString.noun, activityColor);
                string total = Utility.AddColorTags(bestValue.total.ToString(), newColor);

                return string.Format(fallbackString, noun, total);
            }
            else
                return "Keep it up!";
        }

        public IEnumerator Testing()
        {
            while (true)
            {
                text.text = GenerateFact();
                yield return new WaitForSeconds(5);
            }
        }
    }
}