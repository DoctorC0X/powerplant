﻿using UnityEngine;
using System;

namespace Progress.Facts
{
    public class AlmostEvent : IFactEvent
    {
        public FactGenerator.Type eventType;
        public float eventKudos;

        public float ratioThree;
        public float ratioTwo;
        public float ratioOne;

        public string factString;

        public override float GetEventKudos()
        {
            return eventKudos;
        }

        public override float CalcValueKudos(Value v)
        {
            float kudos = -3;
            int threshold = GetThreshold(v);

            if (v.currentRound < threshold && v.currentRound > 0 && threshold > 0)
            {
                float ratio = (float)v.currentRound / (float)threshold;

                kudos = KudosIncreasePerRatio(ratio);
            }

            return kudos;
        }

        protected virtual float KudosIncreasePerRatio(float ratio)
        {
            if (ratio > ratioThree)
                return 3;
            else if (ratio > ratioTwo)
                return 2;
            else if (ratio > ratioOne)
                return 1;

            return 0;
        }

        protected virtual int GetThreshold(Value v)
        {
            switch (eventType)
            {
                case FactGenerator.Type.RECORD:
                    {
                        return v.record;
                    }
                case FactGenerator.Type.MILESTONE:
                    {
                        return MilestoneManager.Instance.GetNextMilestone(v.id, v.currentRound);
                    }
                case FactGenerator.Type.LASTROUND:
                    {
                        return v.lastRound;
                    }
                case FactGenerator.Type.TOTAL:
                    {
                        return v.total;
                    }
                default:
                    {
                        throw new ArgumentException("Unknown Type");
                    }
            }
        }

        public override string GenerateFact(string activity, Value v, Color activityC, Color differenceC, Color newC)
        {
            int difference = CalcDifference(GetThreshold(v), v.currentRound);

            string activityString = Utility.AddColorTags(activity, activityC);
            string differenceString = Utility.AddColorTags(difference.ToString(), differenceC);
            string newString = Utility.AddColorTags(v.currentRound.ToString(), newC);

            return string.Format(factString, activityString, differenceString, newString);
        }

        protected virtual int CalcDifference(int oldValue, int currentValue)
        {
            return oldValue - currentValue;
        }
    }
}