﻿using UnityEngine;
using System.Collections;
using System;


namespace Progress.Facts
{
    public class BrokeEvent : AlmostEvent
    {
        public override float CalcValueKudos(Value v)
        {
            float kudos = -3;
            int threshold = GetThreshold(v);

            if (v.currentRound > threshold && v.currentRound > 0 && threshold > 0)
            {
                float ratio = (float)v.currentRound / (float)threshold;

                kudos = KudosIncreasePerRatio(ratio);
            }

            return kudos;
        }

        protected override int GetThreshold(Value v)
        {
            switch (eventType)
            {
                case FactGenerator.Type.RECORD:
                    {
                        return v.record;
                    }
                case FactGenerator.Type.MILESTONE:
                    {
                        return MilestoneManager.Instance.GetPreviousMilestone(v.id, v.currentRound);
                    }
                case FactGenerator.Type.LASTROUND:
                    {
                        return v.lastRound;
                    }
                case FactGenerator.Type.TOTAL:
                    {
                        return v.total;
                    }
                default:
                    {
                        throw new ArgumentException("Unknown Type");
                    }
            }
        }

        protected override int CalcDifference(int oldValue, int currentValue)
        {
            return currentValue - oldValue;
        }
    }
}