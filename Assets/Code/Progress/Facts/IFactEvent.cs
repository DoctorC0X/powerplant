﻿using UnityEngine;
using System.Collections;

namespace Progress.Facts
{
    public abstract class IFactEvent : ScriptableObject
    {
        public abstract float GetEventKudos();

        public abstract float CalcValueKudos(Value v);

        public abstract string GenerateFact(string activity, Value v, Color activityC, Color differenceC, Color newC);
    }
}