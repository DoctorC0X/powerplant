﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml.Serialization;
using System;
using System.Xml;
using Progress.IO;

namespace Progress
{
    [Serializable]
    public class Goal
    {
        [NonSerialized]
        protected IGoalManager oManager;

        public string fileName;
        protected string title;
        protected string description;

        protected Sprite sprite;

        public enum UnlockType { OR, AND };
        protected UnlockType type;

        public bool unlocked;
        protected bool activated;

        public Subgoal[] subgoals;

        protected Goal()
        {
        }

        public Goal(string fileName, string title, string description, UnlockType type, Subgoal[] subgoals)
        {
            this.fileName = fileName;
            this.title = title;
            this.description = description;
            this.type = type;
            this.subgoals = subgoals;
            unlocked = false;
        }

        public Goal(Goal g) 
            : this(g.GetFileName(), g.GetTitle(), g.GetDescription(), g.GetUnlockType(), g.GetSubgoals())
        {
        }

        public virtual void ApplySaveData(Goal g)
        {
            this.unlocked = g.unlocked;

            for (int i = 0; i < subgoals.Length; i++)
            {
                subgoals[i].Apply(g.subgoals[i]);
            }
        }

        public virtual void Init(IGoalManager m)
        {
            oManager = m;

            UpdateDescription();

            for (int i = 0; i < subgoals.Length; i++)
            {
                try
                {
                    subgoals[i].Init(this);
                }
                catch (NullReferenceException ne)
                {
                    Debug.LogError("VOIObserver " + title + " ran into a null target. Index: " + i);
                }
            }

            activated = false;
        }

        private void UpdateDescription()
        {
            for (int i = 0; i < subgoals.Length; i++)
            {
                description = description.Replace("%" + i, subgoals[i].GetStartValue().ToString());
            }
        }

        public virtual void Activate()
        {
            if (!unlocked)
            {
                RegisterAllSubgoals();

                activated = true;
            }
        }

        // check if needed goals are reached
        public virtual void CheckUnlocked()
        {
            if (!unlocked)
            {
                bool result = false;

                switch (type)
                {
                    case UnlockType.AND:
                        {
                            // AND = all goals need to be reached

                            result = true;

                            for (int i = 0; i < subgoals.Length; i++)
                            {
                                if (!subgoals[i].IsReached())
                                {
                                    result = false;
                                    break;
                                }
                            }
                            break;
                        }
                    case UnlockType.OR:
                        {
                            // OR = a single goal needs to be reached

                            for (int i = 0; i < subgoals.Length; i++)
                            {
                                if (subgoals[i].IsReached())
                                {
                                    result = true;
                                    break;
                                }
                            }
                            break;
                        }
                }

                if (result)
                {
                    // every other goal was reached so I'm unlocked
                    unlocked = true;

                    oManager.Unlocked(this);

                    // remove all listener
                    DeregisterAllSubgoals();
                }
            }
        }

        protected virtual void RegisterAllSubgoals()
        {
            for (int i = 0; i < subgoals.Length; i++)
            {
                subgoals[i].RegisterOnEventManager();
            }
        }

        protected virtual void DeregisterAllSubgoals()
        {
            for (int i = 0; i < subgoals.Length; i++)
            {
                subgoals[i].DeregisterOnEventManager();
            }
        }

        public string GetFileName()
        {
            return fileName;
        }

        public string GetTitle()
        {
            return title;
        }

        public string GetDescription()
        {
            return description;
        }

        public Sprite GetSprite()
        {
            return sprite;
        }

        public virtual bool IsUnlocked()
        {
            return unlocked;
        }

        public virtual bool IsActivated()
        {
            return activated;
        }

        public UnlockType GetUnlockType()
        {
            return type;
        }

        public Subgoal[] GetSubgoals()
        {
            return subgoals;
        }
    }
}