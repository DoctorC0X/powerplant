﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Progress
{
    [CustomEditor(typeof(GoalManager))]
    public class GoalManagerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            GoalManager myTarget = (GoalManager)target;

            if (Application.isPlaying)
            {
                foreach (KeyValuePair<string, Goal> current in myTarget.Goals)
                {
                    EditorGUILayout.Space();

                    EditorGUILayout.TextField("title ", current.Value.GetTitle());
                    EditorGUILayout.TextField("file ", current.Value.GetFileName());
                    EditorGUILayout.Toggle("unlocked", current.Value.IsUnlocked());

                    int indent = EditorGUI.indentLevel;
                    EditorGUI.indentLevel++;

                    for (int j = 0; j < current.Value.GetSubgoals().Length; j++)
                    {
                        EditorGUILayout.IntField("subgoal " + j, current.Value.GetSubgoals()[j].currentValue);
                    }

                    EditorGUI.indentLevel = indent;
                }
            }
        }
    }
}