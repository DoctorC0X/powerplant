﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using EventSystem;

namespace Progress
{
    [CustomEditor(typeof(ValueManager))]
    public class ValueManagerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            ValueManager myTarget = (ValueManager)target;

            if (Application.isPlaying)
            {
                foreach (KeyValuePair<EventData.ID, Value> current in myTarget.Values)
                {
                    EditorGUILayout.LabelField(current.Key.ToString());

                    EditorGUILayout.IntField("current", current.Value.currentRound);
                    EditorGUILayout.IntField("total", current.Value.total);

                    EditorGUILayout.Space();
                }
            }
        }
    }
}