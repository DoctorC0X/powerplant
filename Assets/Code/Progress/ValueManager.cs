﻿using System.Collections.Generic;
using Uninspired.Core.IO;
using UnityEngine;
using Progress.IO;
using System.Linq;
using EventSystem;
using System;

namespace Progress
{
    public class ValueManager : MonoBehaviour
    {
        private static ValueManager _instance;
        public static ValueManager Instance
        {
            get { return _instance; }
        }

        public Dictionary<EventData.ID, Value> Values
        {
            get { return ((ValueData)PersistenceManager.Instance.GetData(gameObject.name)).values; }
        }

        void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this);
                return;
            }
            else
            {
                _instance = this;
            }

            if (PersistenceManager.Instance != null && !PersistenceManager.Instance.IsRegistered(gameObject.name))
                PersistenceManager.Instance.SetData(gameObject.name, new ValueData());
        }

        void Start()
        {
            foreach (EventData.ID current in Enum.GetValues(typeof(EventData.ID)))
            {
                EventManager.Instance.AddHandler(current, IncreaseValue);
            }
        }

        public void IncreaseValue(EventData.ID id, EventData.ParamBase param)
        {
            if (!Values.ContainsKey(id))
                throw new ArgumentException("MedalValue with tag " + id + " not found!");

            Values[id].Increase(param.increase);
        }

        public void IncreaseValue(EventData.ParamBase param)
        {
            IncreaseValue(param.ID, param);
        }

        public void AddValue(EventData.ID id, Value value)
        {
            Values.Add(id, value);
        }

        public void TriggerRoundEnd()
        {
            foreach (KeyValuePair<EventData.ID, Value> current in Values)
            {
                current.Value.RoundEnd();
            }
        }
    }

    [Serializable]
    public class ValueData : PersistentData
    {
        public SerializableDictionary<EventData.ID, Value> values;

        public ValueData()
        {
            values = new SerializableDictionary<EventData.ID, Value>();

            foreach (EventData.ID current in Enum.GetValues(typeof(EventData.ID)))
            {
                values.Add(current, new Value(current));
            }
        }
    }
}

