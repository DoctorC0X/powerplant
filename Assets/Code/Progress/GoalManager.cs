﻿using System;
using Progress.IO;
using UnityEngine;
using EventSystem;
using System.Collections.Generic;
using Uninspired.Core.IO;

namespace Progress
{
    public class GoalManager : MonoBehaviour, IGoalManager
    {
        public static string gameFilePath = "Progress/Medals";
        public static bool firstAwake = true;

        public event Action<Goal> OnUnlocked;

        public Dictionary<string, Goal> Goals
        {
            get { return ((GoalData)PersistenceManager.Instance.GetData(gameObject.name)).goals; }
        }

        void Awake()
        {
            if (PersistenceManager.Instance != null)
            {
                if(firstAwake)
                {
                    GoalData data = new GoalData();

                    if (PersistenceManager.Instance.IsRegistered(gameObject.name))
                    {
                        GoalData saveData = (GoalData)PersistenceManager.Instance.GetData(gameObject.name);
                        data.Apply(saveData);
                    }

                    PersistenceManager.Instance.SetData(gameObject.name, data);

                    firstAwake = false;
                }

                InitGoals();
            }
        }

        protected virtual void InitGoals()
        {
            foreach(KeyValuePair<string, Goal> current in Goals)
            {
                current.Value.Init(this);
                current.Value.Activate();
            }
        }

        public void Unlocked(Goal goal)
        {
            EventManager.Instance.CallEvent(EventData.ID.MEDAL_UNLOCKED);

            if (OnUnlocked != null)
                OnUnlocked(goal);
        }
    }

    [Serializable]
    public class GoalData : PersistentData
    {
        public SerializableDictionary<string, Goal> goals;

        public GoalData()
        {
            TextAsset[] assets = Resources.LoadAll<TextAsset>(GoalManager.gameFilePath);
            GoalFactory factory = new GoalFactory();

            goals = new SerializableDictionary<string, Goal>();

            for (int i = 0; i < assets.Length; i++)
            {
                goals.Add(assets[i].name, factory.Create(assets[i].name, assets[i].text));
            }
        }

        public void Apply(GoalData save)
        {
            foreach (KeyValuePair<string, Goal> current in save.goals)
            {
                if (goals.ContainsKey(current.Key))
                    goals[current.Key].ApplySaveData(current.Value);
            }
        }
    }
}

