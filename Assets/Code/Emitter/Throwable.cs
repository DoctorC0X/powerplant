﻿using UnityEngine;
using System.Collections;

public class Throwable : MonoBehaviour 
{
    public Thrower thrower;

    public float timePassed;
    public int dir;

    public Vector2 start;

    public int pointIndex;
    public Vector2 previousPoint;
    public Vector2 nextPoint;

    public bool useWorldSpace;

    public int prefabIndex;

    public virtual void Init(Thrower t, int index, int d, bool world)
    {
        thrower = t;
        timePassed = 0;
        dir = d;

        prefabIndex = index;

        start = thrower.transform.position;

        useWorldSpace = world;

        pointIndex = 1;
        ProceedPoints();
    }

    public virtual void UpdatePos()
    {
        timePassed += Time.deltaTime;

        float fraction = timePassed / (thrower.duration / (float)thrower.curveSegments);

        if (useWorldSpace)
        {
            transform.position = start + Vector2.Lerp(previousPoint, nextPoint, fraction);
        }
        else
        {
            transform.position = thrower.transform.TransformPoint(Vector2.Lerp(previousPoint, nextPoint, fraction));
        }

        if (fraction >= 1)
        {
            pointIndex++;

            if (pointIndex + 1 < thrower.curvePoints.Length)
            {
                ProceedPoints();
            }
            else
            {
                thrower.RemoveThrowable(prefabIndex);
            }
        }
    }

    private void ProceedPoints()
    {
        timePassed = 0;

        previousPoint = thrower.curvePoints[pointIndex];
        nextPoint = thrower.curvePoints[pointIndex + 1];

        previousPoint = new Vector2(previousPoint.x * dir, previousPoint.y);
        nextPoint = new Vector2(nextPoint.x * dir, nextPoint.y);
    }

    public virtual void HandleParameter(object param)
    {

    }
}
