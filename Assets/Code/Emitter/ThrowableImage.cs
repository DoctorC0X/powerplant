﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ThrowableImage : Throwable 
{
    public Image image;

    public override void Init(Thrower t, int index, int d, bool world)
    {
        base.Init(t, index, d, world);

        transform.SetParent(FollowCanvas.Instance.backgroundCanvas.transform, true);
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0);
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;

        image = GetComponent<Image>();
    }

    public override void UpdatePos()
    {
        base.UpdatePos();

        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0);
    }

    public override void HandleParameter(object param)
    {
        Params p = (Params)param;

        image.sprite = p.sprite;
        image.color = p.color;
        //GetComponent<RectTransform>().sizeDelta = new Vector2(p.size, p.size);
        transform.localScale = transform.localScale * p.size;
    }

    public struct Params
    {
        public Sprite sprite;
        public Color color;
        public float size;

        public Params(Sprite s, Color c, float pSize)
        {
            sprite = s;
            color = c;
            size = pSize;
        }
    }
}
