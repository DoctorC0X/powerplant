﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ThrowableText : Throwable 
{
    public override void Init(Thrower t, int index, int d, bool world)
    {
        base.Init(t, index, d, world);

        transform.SetParent(FollowCanvas.Instance.backgroundCanvas.transform, true);
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0);
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;

        GetComponent<Text>().enabled = true;
    }

    public override void UpdatePos()
    {
        base.UpdatePos();

        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0);
    }

    /// <summary>
    /// Set Parameters for ThrowableText
    /// </summary>
    /// <param name="parameter"> 0 = message, 1 = color, 2 = fontSize </param>
    public override void HandleParameter(object parameter)
    {
        Params p = (Params)parameter;

        GetComponent<Text>().text = p.text;
        GetComponent<Text>().fontSize = p.fontSize;

        GetComponent<Text>().color = p.textColor;
        GetComponent<Outline>().effectColor = p.outlineColor;
    }

    public struct Params
    {
        public string text;
        public int fontSize;
        public Color textColor;
        public Color outlineColor;

        public Params(string txt, int size, Color color, Color outline)
        {
            text = txt;
            fontSize = size;
            textColor = color;
            outlineColor = outline;
        }
    }
}


