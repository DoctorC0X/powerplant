﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Thrower : MonoBehaviour 
{
    public GameObject[] prefabsToThrow;
    public int[] poolSize;

    public GameObject throwPoolParent;

    public Vector3 offset;

    public Transform cStart;
    public Transform cHandle;
    public Transform cEnd;

    public float duration;

    public int nextDir;
    public bool alternateSides;

    public int curveSegments;
    public Vector2[] curvePoints;

    // queue thrown items (since they all have the same live time)
    public Queue<Throwable> thrown;

    public ThrowerPool[] throwerPools;
    
    void Awake()
    {
        // just a parent for pooled throwables, so they dont spam the hierachy
        throwPoolParent = new GameObject("throw_pool");
        throwPoolParent.transform.parent = transform;
        throwPoolParent.transform.localPosition = Vector3.zero;

        throwerPools = new ThrowerPool[prefabsToThrow.Length];

        // create a bunch of throwables and pool them (cheaper then instantiating them over and over ingame)
        for (int i = 0; i < prefabsToThrow.Length; i++)
        {
            throwerPools[i] = new ThrowerPool();
            for (int j = 0; j < poolSize[i]; j++)
            {
                GameObject go = Instantiate(prefabsToThrow[i]) as GameObject;
                go.transform.SetParent(throwPoolParent.transform);
                go.transform.position = Vector3.zero;
                go.SetActive(false);

                throwerPools[i].pool.Enqueue(go);
            }
        }

        curvePoints = new Vector2[curveSegments + 1];
        for (int i = 0; i <= curveSegments; i++)
        {
            curvePoints[i] = GetPointOnCurve(cStart.localPosition, cHandle.localPosition, cEnd.localPosition, (float)i / (float)curveSegments);
        }

        thrown = new Queue<Throwable>();
        nextDir = 1;
    }

    public void Init()
    {
        transform.localPosition = offset;
        transform.localRotation = Quaternion.identity;
    }

    // Update is called once per frame
    void Update() 
    {
        if (!GameManager.Instance.paused)
        {
            // thrower rotation shall always be 0
            this.transform.rotation = Quaternion.identity;

            // update the position of each throwable
            if (thrown.Count > 0)
            {
                Throwable[] thrownArray = thrown.ToArray();

                for (int i = 0; i < thrownArray.Length; i++)
                {
                    thrownArray[i].UpdatePos();
                }
            }
        }
	}

    // get point on cubic curve calculated with custom points
    public Vector2 GetPointOnCurve(Vector2 start, Vector2 handle, Vector2 end, float t)
    {
        t = Mathf.Clamp(t, 0, 1);

        float first = Sqr(1 - t);
        float second = 2 * t * (1 - t);
        float third = Sqr(t);

        return first * start + second * handle + third * end;
    }

    // get point on standard curve
    public Vector2 GetPointOnCurve(float t)
    {
        return GetPointOnCurve(cStart.position, cHandle.position, cEnd.position, t);
    }

    public Vector2 GetPointOnCurve(int index)
    {
        return curvePoints[index];
    }

    public void Throw(object parameter, int prefabIndex, bool world = true)
    {
        //GameObject go = Instantiate(prefabToThrow) as GameObject;

        if (throwerPools[prefabIndex].pool.Count != 0)
        {
            GameObject go = throwerPools[prefabIndex].pool.Dequeue();

            Throwable newThrowable = go.GetComponent<Throwable>();

            newThrowable.Init(this, prefabIndex, nextDir, world);
            newThrowable.HandleParameter(parameter);

            go.SetActive(true);

            //Throwable newThrowable = new Throwable(go, cStart.position, cHandle.position, cEnd.position, nextDir);

            thrown.Enqueue(newThrowable);

            if(alternateSides)
                nextDir *= -1;
        }
        else
        {
            Debug.LogError("Thrower couldn't throw prefab " + prefabIndex + ", because pool was empty");
        }
    }

    // since thrown items are queued, we just destroy the first one (hence same life time)
    public void RemoveThrowable(int prefabIndex)
    {
        GameObject go = thrown.Dequeue().gameObject;

        // throw it back to pool :D
        go.SetActive(false);
        go.transform.SetParent(throwPoolParent.transform);
        go.transform.position = Vector3.zero;

        throwerPools[prefabIndex].pool.Enqueue(go);
    }

    // f * f is cheaper than Mathf.Sqr(f, 2)
    private float Sqr(float f)
    {
        return f * f;
    }
}

public class ThrowerPool
{
    public Queue<GameObject> pool;

    public ThrowerPool()
    {
        pool = new Queue<GameObject>();
    }
}
