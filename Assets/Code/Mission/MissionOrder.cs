﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Progress.Missions
{
    [Serializable]
    public class MissionOrder 
    {
        private Queue<int> indexQueue;
        
        public int[] currentOrder
        {
            get { return indexQueue.ToArray(); }
            set { indexQueue = new Queue<int>(value); }
        }

        private MissionOrder()
        {
        }

        public MissionOrder(int missionCount)
        {
            int[] shuffledIndices = new int[missionCount];

            for (int i = 0; i < shuffledIndices.Length; i++)
                shuffledIndices[i] = i;

            indexQueue = new Queue<int>(Utility.ShuffleList<int>(shuffledIndices));
        }

        public void ApplySaveData(MissionOrder save)
        {
            int[] oldOrder = save.currentOrder;

            indexQueue = new Queue<int>(oldOrder);

            if (indexQueue.Count == oldOrder.Length)
                return;

            if (indexQueue.Count > oldOrder.Length)
            {
                indexQueue = new Queue<int>(indexQueue.Where(x => x < oldOrder.Length));
            }
            else
            {
                while (indexQueue.Count < oldOrder.Length)
                {
                    indexQueue.Enqueue(indexQueue.Count);
                }
            }
        }

        public int GetNextMissionIndex()
        {
            int index = indexQueue.Dequeue();
            indexQueue.Enqueue(index);

            return index;
        }

        public int[] GetOrder()
        {
            return indexQueue.ToArray();
        }
    }
}