﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml.Serialization;
using Progress;
using System.Xml;
using System.Collections.Generic;
using Progress.IO;

namespace Progress.Missions
{
    [Serializable]
    public class Mission : Goal
    {
        private int reward;
        public int queueNr;

        private Mission() 
            : base()
        {
        }

        public Mission(Goal g, int queueNr, int reward)
            : base(g)
        {
            this.queueNr = queueNr;
            this.reward = reward;
        }

        public int GetReward()
        {
            return reward;
        }

        public int GetQueueNr()
        {
            return queueNr;
        }
    }
}