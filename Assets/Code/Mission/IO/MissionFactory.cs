﻿using UnityEngine;
using System.Xml;
using Progress.IO;
using Uninspired.Core.IO.Xml;

namespace Progress.Missions.IO
{
    public class MissionFactory
    {
        public Mission Create(string fileName, int queueLevel)
        {
            TextAsset asset = Resources.Load<TextAsset>(string.Format("{0}/{1}", MissionManager.gameFilePath, fileName));

            return Create(fileName, queueLevel, asset.text);
        }

        public Mission Create(string fileName, int queueLevel, string fileContent)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(fileContent);

            XmlNode root = xDoc.DocumentElement;

            return Create(fileName, queueLevel, root);
        }

        public Mission Create(string fileName, int queueLevel, XmlNode node)
        {
            MissionSelectorFactory selectorFactory = new MissionSelectorFactory();
            XmlNodeSelector selector = selectorFactory.Create(node.OwnerDocument.NameTable);

            XmlNode missionTemplate = selector.SelectSingleByXPath(node, "missionTemplate").Node;

            GoalFactory gFactory = new GoalFactory();
            Goal template = gFactory.Create(fileName, missionTemplate);

            string missionString = string.Format("{0}[@{1}='{2}']", "missionStep", "nr", queueLevel);
            XmlNode missionStep = selector.SelectSingleByXPath(node, "missionSteps", missionString).Node;

            XmlNodeWrapper[] valueNodes = selector.SelectByXPath(missionStep, "value");
            for (int i = 0; i < valueNodes.Length; i++)
            {
                template.subgoals[i].startValue = valueNodes[i].AsInt();
            }

            int reward = selector.SelectSingleByXPath(missionStep, "reward").AsInt();

            return new Mission(template, queueLevel, reward);
        }
    }
}