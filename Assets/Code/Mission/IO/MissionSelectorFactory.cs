﻿using UnityEngine;
using System.Collections;
using System.Xml;
using Progress.IO;
using Uninspired.Core.IO.Xml;

namespace Progress.Missions.IO
{
    public class MissionSelectorFactory : AbstractXmlNodeSelectorFactory
    {
        public override XmlNodeSelector Create(XmlNameTable nameTable)
        {
            return new XmlNodeSelector(nameTable, MissionNamespace.missionUri, MissionNamespace.missionPrefix);
        }
    }
}
