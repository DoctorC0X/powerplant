﻿using UnityEngine;
using System.Collections;

namespace Progress.Missions.IO
{
    public class MissionNamespace : Progress.IO.ProgressNamespace
    {
        public static string missionUri = "http://www.w3.org/2001/Mission";
        public static string missionPrefix = "mis";
    }
}