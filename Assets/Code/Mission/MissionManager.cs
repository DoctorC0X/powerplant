﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Progress;
using Progress.Missions.IO;
using Progress.IO;
using System.IO;
using System.Linq;
using EventSystem;
using System;

namespace Progress.Missions
{
    public class MissionManager : MonoBehaviour, IGoalManager
    {
        public static int queueLevelMax = 10;
        public static int maxActiveMissions = 3;
        public static int maxPotentialMissions = 3;

        public static string gameFilePath = "Progress/Missions";
        public static bool firstAwake = true;

        private MissionData Data
        {
            get { return (MissionData)PersistenceManager.Instance.GetData(gameObject.name); }
            set { PersistenceManager.Instance.SetData(gameObject.name, value); }
        }

        private Mission[] ActiveMissions
        {
            get { return Data.activeMissions; }
            set { Data.activeMissions = value; }
        }

        private Mission[] PotentialMissions
        {
            get { return Data.potentialMissions; }
            set { Data.potentialMissions = value; }
        }

        public GameObject missionPicker;

        void Awake()
        {
            if (PersistenceManager.Instance != null)
            {
                if (firstAwake)
                {
                    MissionData data = new MissionData();

                    if (PersistenceManager.Instance.IsRegistered(gameObject.name))
                    {
                        MissionData saveData = (MissionData)PersistenceManager.Instance.GetData(gameObject.name);
                        data.Apply(saveData);
                    }
                    else
                    {
                        for (int i = 0; i < data.activeMissions.Length; i++)
                        {
                            if (data.activeMissions[i] == null)
                                data.CreateActiveMission(i);
                        }
                    }

                    PersistenceManager.Instance.SetData(gameObject.name, data);

                    firstAwake = false;
                }

                InitActiveMissions();
                InitPotentialMissions();
            }
        }

        void Start()
        {
            missionPicker = Instantiate<GameObject>(missionPicker);
            missionPicker.transform.parent = FollowCanvas.Instance.pauseScreen.transform;
            missionPicker.transform.localPosition = Vector3.zero;
            missionPicker.transform.localRotation = Quaternion.identity;
            missionPicker.transform.localScale = Vector3.one;

            missionPicker.GetComponent<MissionPicker>().Init(this);
        }

        private void InitActiveMissions()
        {
            for (int i = 0; i < maxActiveMissions; i++)
            {
                if (ActiveMissions[i] != null)
                {
                    ActiveMissions[i].Init(this);
                    ActiveMissions[i].Activate();
                }
            }
        }

        private void InitPotentialMissions()
        {
            for (int i = 0; i < maxPotentialMissions; i++)
            {
                if (PotentialMissions[i] == null)
                    PotentialMissions[i] = Data.CreatePotentialMission(i);

                PotentialMissions[i].Init(this);
            }
        }

        public void Unlocked(Goal goal)
        {
            Mission unlockedMission = (Mission)goal;

            //container.completionTable[unlockedMission.fileName] += 1;

            PlantModel.MainPlant.plantFace.Celebrate();

            EventManager.Instance.CallEvent(EventData.ID.MISSION_COMPLETED, new EventData.ParamMission() { increase = 1, mission = unlockedMission });
        }

        public bool ClaimMission(int index)
        {
            if (index < 0 || index >= maxActiveMissions)
                return false;

            if (ActiveMissions[index] != null && ActiveMissions[index].IsUnlocked())
            {
                EventManager.Instance.CallEvent(EventData.ID.MISSION_CLAIMED, new EventData.ParamMission() { increase = 1, mission = ActiveMissions[index] });

                ActiveMissions[index] = null;

                return true;
            }

            return false;
        }

        public void ActivateMission(int potentialIndex, int activeIndex)
        {
            if (potentialIndex < 0 || potentialIndex >= maxPotentialMissions)
                return;

            if (activeIndex < 0 || activeIndex >= maxActiveMissions)
                return;

            ActiveMissions[activeIndex] = PotentialMissions[potentialIndex];
            ActiveMissions[activeIndex].Activate();

            Data.CreatePotentialMission(potentialIndex);

            if (PotentialMissions[potentialIndex] != null)
                PotentialMissions[potentialIndex].Init(this);
        }

        public bool ActiveSlotFree(int index)
        {
            return ActiveMissions[index] == null;
        }

        public bool ActiveUnlocked(int index)
        {
            return ActiveMissions[index].IsUnlocked();
        }

        public bool PotentialSlotFree(int index)
        {
            return PotentialMissions[index] == null || PotentialMissions[index].IsUnlocked();
        }

        public Goal[] GetGoals()
        {
            return ActiveMissions;
        }

        public Goal[] GetPotentialMissions()
        {
            return PotentialMissions;
        }

        public Goal GetPotentialMission(int index)
        {
            return PotentialMissions[index];
        }

        public int GetGoalCount()
        {
            return maxActiveMissions;
        }

        public Goal GetGoal(int index)
        {
            return ActiveMissions[index];
        }
    }

    [Serializable]
    public class MissionData : PersistentData
    {
        public CompletionTable completionTable;
        public MissionOrder missionOrder;
         
        public Mission[] activeMissions;
        public Mission[] potentialMissions;

        public MissionData()
        {
            TextAsset[] assets = Resources.LoadAll<TextAsset>(MissionManager.gameFilePath);
            string[] fileNames = assets.Select(t => t.name).ToArray();

            completionTable = new CompletionTable(fileNames);
            missionOrder = new MissionOrder(fileNames.Length);

            activeMissions = new Mission[MissionManager.maxActiveMissions];
            potentialMissions = new Mission[MissionManager.maxPotentialMissions];
        }

        public void Apply(MissionData save)
        {
            completionTable.ApplySaveData(save.completionTable);
            missionOrder.ApplySaveData(save.missionOrder);

            MissionFactory factory = new MissionFactory();

            for (int i = 0; i < save.activeMissions.Length; i++)
            {
                if (save.activeMissions[i] != null && completionTable.fileNames.Contains(save.activeMissions[i].GetFileName()))
                {
                    activeMissions[i] = CreateMission(save.activeMissions[i].GetFileName(), save.activeMissions[i].GetQueueNr());
                    activeMissions[i].ApplySaveData(save.activeMissions[i]);
                }
            }

            for (int i = 0; i < save.potentialMissions.Length; i++)
            {
                if (save.potentialMissions[i] != null && completionTable.fileNames.Contains(save.potentialMissions[i].GetFileName()))
                {
                    potentialMissions[i] = CreateMission(save.potentialMissions[i].GetFileName(), save.potentialMissions[i].GetQueueNr());
                }
            }
        }

        public Mission CreateActiveMission(int index)
        {
            activeMissions[index] = CreateMission();
            return activeMissions[index];
        }

        public Mission CreatePotentialMission(int index)
        {
            potentialMissions[index] = CreateMission();
            return potentialMissions[index];
        }

        private Mission CreateMission(string fileName, int queueLevel)
        {
            MissionFactory mFactory = new MissionFactory();

            return mFactory.Create(fileName, queueLevel);
        }

        private Mission CreateMission()
        {
            string fileName = completionTable.fileNames[missionOrder.GetNextMissionIndex()];
            int queueLevel = completionTable.GetNextQueueLevel(fileName);

            return CreateMission(fileName, queueLevel);
        }
    }
}