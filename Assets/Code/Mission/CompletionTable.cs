﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Uninspired.Core.IO;

namespace Progress.Missions
{
    [Serializable]
    public class CompletionTable
    {
        public SerializableDictionary<string, int> cTable;

        [XmlIgnore]
        public string[] fileNames
        {
            get { return cTable.Keys.ToArray(); }
        }

        [XmlIgnore]
        public int[] completion
        {
            get { return cTable.Values.ToArray(); }
        }

        private CompletionTable()
        {
        }

        public CompletionTable(string[] fileNames)
        {
            cTable = new SerializableDictionary<string, int>();
            for (int i = 0; i < fileNames.Length; i++)
            {
                cTable.Add(fileNames[i], 1);
            }
        }

        public void ApplySaveData(CompletionTable save)
        {
            foreach (KeyValuePair<string, int> current in save.cTable)
            {
                if (cTable.ContainsKey(current.Key))
                    cTable[current.Key] = current.Value;
            }
        }

        public int GetNextQueueLevel(string fileName)
        {
            int queueLevel = cTable[fileName];

            cTable[fileName]++;

            if (cTable[fileName] > MissionManager.queueLevelMax)
            {
                cTable[fileName] = 1;
            }

            return queueLevel;
        }

        public int[] GetValues()
        {
            return cTable.Values.ToArray();
        }
    }
}