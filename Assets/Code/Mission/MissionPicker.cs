﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.EventSystems;
using Progress;
using EventSystem;

namespace Progress.Missions
{
    public class MissionPicker : MonoBehaviour
    {
        private MissionManager mManager;

        public GameObject notification;

        private Image bubble;
        private Text bubbleText;

        public GameObject puffEffect;

        public int starDustDummySpawnAmount;
        public Vector2 starDustDummySpawnOffset;
        public Vector2 starDustDummyDurationRange;

        public GameObject pixelHand;
        public bool pixelHandActive;

        public bool FirstPlay
        {
            get { return !PlayerPrefs.HasKey(firstPlayKey); }
            set
            {
                if (value)
                    PlayerPrefs.SetInt(firstPlayKey, 1);
            }
        }
        public string firstPlayKey;

        private int _unlocked;

        public MoveTo moveToCurrent;
        public MoveTo moveToPotential;

        public Transform[] activeSlots;
        public Transform[] potentialSlots;
        public Image[] stars;

        public MedalTile[] activeTiles;
        public MedalTile[] potentialTiles;

        public GameObject smallMetalTilePrefab;

        public enum State { SHOW, PICK };
        public State currentState;

        public bool showing;

        public Vector2 dragOffset;
        public int draggedTileIndex;

        public float dragSpeed;
        public bool dropped;

        public int Unlocked
        {
            get { return _unlocked; }
            set
            {
                _unlocked = value;

                if (bubble != null)
                {
                    if (_unlocked <= 0)
                    {
                        bubble.gameObject.SetActive(false);
                    }
                    else
                    {
                        bubble.gameObject.SetActive(true);
                        bubbleText.text = _unlocked.ToString();
                    }
                }
            }
        }

        public void Init(MissionManager mManager)
        {
            this.mManager = mManager;

            FollowCanvas.Instance.pauseScreen.missionPicker = this;

            notification = Instantiate<GameObject>(notification);
            notification.transform.parent = FollowCanvas.Instance.pauseButton.transform;
            notification.transform.localPosition = Vector3.zero;
            notification.transform.localRotation = Quaternion.identity;
            notification.transform.localScale = Vector3.one;

            bubble = notification.GetComponentInChildren<Image>();
            bubbleText = notification.GetComponentInChildren<Text>();

            Unlocked = 0;

            FillActiveSlots();
            FillPotentialSlots();

            for (int i = 0; i < MissionManager.maxActiveMissions; i++)
            {
                SetSlotEnabled(i, false);

                if (mManager.ActiveSlotFree(i) || mManager.ActiveUnlocked(i))
                {
                    Unlocked++;

                    if (!mManager.ActiveSlotFree(i) && mManager.ActiveUnlocked(i))
                        SetSlotEnabled(i, true);
                }
            }

            EventManager.Instance.AddHandler(EventData.ID.MISSION_COMPLETED, () => Unlocked++);

            SetShow(false);
        }

        public void SwitchState()
        {
            if (currentState == State.SHOW)
            {
                currentState = State.PICK;

                moveToCurrent.Begin();
                moveToPotential.Begin();
            }
            else
            {
                currentState = State.SHOW;

                moveToCurrent.Back();
                moveToPotential.Back();
            }
        }

        public void FillActiveSlots()
        {
            activeTiles = new MedalTile[activeSlots.Length];

            FillSlots(mManager.GetGoals(), activeSlots, activeTiles);
        }

        public void FillPotentialSlots()
        {
            potentialTiles = new MedalTile[potentialSlots.Length];

            FillSlots(mManager.GetPotentialMissions(), potentialSlots, potentialTiles);
        }

        private void FillSlots(Goal[] goal, Transform[] slots, MedalTile[] tiles)
        {
            if (goal.Length != slots.Length)
                throw new ArgumentException("Amount of Observer and Slots is not equal!");

            for (int i = 0; i < goal.Length; i++)
            {
                if (goal[i] != null)
                {
                    tiles[i] = CreateNewTile(i, slots[i], goal[i]);
                }
            }
        }

        private MedalTile CreateNewTile(int slotNr, Transform slot, Goal goal)
        {
            GameObject tileGo = Instantiate<GameObject>(smallMetalTilePrefab);

            MedalTile mTile = tileGo.GetComponent<MedalTile>();
            mTile.Init(goal);

            SetTileToSlot(mTile, slot);

            return mTile;
        }

        private void SetTileToSlot(MedalTile tile, Transform slot)
        {
            tile.transform.SetParent(slot, false);
            tile.transform.localPosition = Vector3.zero;
        }

        public void SetShow(bool show)
        {
            showing = show;

            moveToCurrent.gameObject.SetActive(show);
            moveToPotential.gameObject.SetActive(show);

            if (!showing && currentState != State.SHOW)
            {
                SwitchState();
                moveToCurrent.Skip();
                moveToPotential.Skip();
            }

            if (showing)
            {
                bool showPotentials = false;

                for (int i = 0; i < MissionManager.maxActiveMissions; i++)
                {
                    // if a single active mission is null or unlocked, switch to pick state
                    if (!showPotentials)
                        showPotentials = mManager.ActiveSlotFree(i);

                    // reset tile to show the unlocked image
                    if (!mManager.ActiveSlotFree(i) && mManager.ActiveUnlocked(i))
                    {
                        Destroy(activeTiles[i].gameObject);
                        activeTiles[i] = CreateNewTile(i, activeSlots[i], mManager.GetGoal(i));

                        SetSlotEnabled(i, true);

                        if (FirstPlay)
                        {
                            pixelHand = Instantiate(pixelHand) as GameObject;
                            pixelHand.transform.SetParent(activeSlots[i].transform, false);
                            pixelHand.transform.localPosition = new Vector3(0, 0, -50);
                            FirstPlay = true;
                            pixelHandActive = true;
                        }
                        else
                        {
                            pixelHand = null;
                        }
                    }
                }

                if (showPotentials)
                    SwitchState();
            }
        }

        public void OnSlotClick(int slotNr)
        {
            if (mManager.ClaimMission(slotNr))
            {
                puffEffect.transform.position = activeSlots[slotNr].transform.position;

                foreach (Animator current in puffEffect.GetComponentsInChildren<Animator>())
                    current.SetTrigger("Trigger");

                AudioSources.Instance.missionClaim.Play();

                StarDustManager.Instance.SpawnStarDustDummys(starDustDummySpawnAmount, activeSlots[slotNr].transform.position, FollowCanvas.Instance.display.starDustCounter.transform.position, starDustDummySpawnOffset, starDustDummyDurationRange);

                Destroy(activeTiles[slotNr].gameObject);

                SetSlotEnabled(slotNr, false);

                if (currentState == State.SHOW)
                    SwitchState();

                if (pixelHandActive)
                {
                    Destroy(pixelHand);
                    pixelHandActive = false;
                }
            }
        }

        public void OnDragBegin(int slotNr)
        {
            dropped = false;
            draggedTileIndex = slotNr;
            dragOffset = potentialTiles[draggedTileIndex].transform.InverseTransformPoint(Camera.main.ScreenToWorldPoint(Input.mousePosition));

            potentialTiles[draggedTileIndex].transform.SetParent(moveToPotential.transform, true);
        }

        public void OnDrag(BaseEventData data)
        {
            Vector2 pos = potentialTiles[draggedTileIndex].transform.parent.InverseTransformPoint(Camera.main.ScreenToWorldPoint(Input.mousePosition));

            potentialTiles[draggedTileIndex].GetComponent<RectTransform>().anchoredPosition3D = new Vector3(pos.x - dragOffset.x, pos.y - dragOffset.y, 0);
        }

        public void OnDragEnd()
        {
            if (!dropped)
                SetTileToSlot(potentialTiles[draggedTileIndex], potentialSlots[draggedTileIndex]);
        }

        public void OnDrop(int slotIndex)
        {
            if (activeTiles[slotIndex] == null)
            {
                dropped = true;

                Unlocked--;

                SetTileToSlot(potentialTiles[draggedTileIndex], activeSlots[slotIndex]);
                activeTiles[slotIndex] = potentialTiles[draggedTileIndex];

                mManager.ActivateMission(draggedTileIndex, slotIndex);

                if (mManager.GetPotentialMission(draggedTileIndex) != null)
                {
                    potentialTiles[draggedTileIndex] = CreateNewTile(draggedTileIndex, potentialSlots[draggedTileIndex], mManager.GetPotentialMission(draggedTileIndex));

                    // spawn new tile out of screen
                    Vector3 oldPos = potentialSlots[draggedTileIndex].transform.position;
                    potentialTiles[draggedTileIndex].GetComponent<RectTransform>().anchoredPosition3D = new Vector3(moveToPotential.InitialPos.x, oldPos.y, oldPos.z);

                    // and let it swoop in like batman!
                    MoveTo potMove = potentialTiles[draggedTileIndex].gameObject.AddComponent<MoveTo>();
                    potMove.Init(MoveTo.PosType.LOCAL, Vector3.zero);
                    potMove.duration = moveToPotential.duration;
                    potMove.animCurve = moveToPotential.animCurve;
                    potMove.OnReachedEnd += () => Destroy(potMove);
                    potMove.Begin();
                }

                if (Unlocked == 0)
                    SwitchState();
            }
        }

        public void SetSlotEnabled(int index, bool enabled)
        {
            stars[index].enabled = enabled;
        }
    }
}