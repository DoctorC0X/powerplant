﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Progress.Missions
{
    [CustomEditor(typeof(MissionManager))]
    public class MissionManagerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            MissionManager myTarget = (MissionManager)target;

            if (Application.isPlaying)
            {
                EditorGUILayout.PrefixLabel("actives");

                int indent = EditorGUI.indentLevel;
                EditorGUI.indentLevel++;

                for (int i = 0; i < MissionManager.maxActiveMissions; i++)
                {
                    Mission m = (Mission)myTarget.GetGoal(i);

                    ShowMission(m);
                }

                EditorGUI.indentLevel = indent;

                EditorGUILayout.PrefixLabel("potentials");

                indent = EditorGUI.indentLevel;
                EditorGUI.indentLevel++;

                for (int i = 0; i < MissionManager.maxPotentialMissions; i++)
                {
                    Mission m = (Mission)myTarget.GetPotentialMission(i);

                    ShowMission(m);
                }

                EditorGUI.indentLevel = indent;
            }
        }

        private void ShowMission(Mission m)
        {
            if (m != null)
            {
                EditorGUILayout.TextField("title ", m.GetTitle());
                EditorGUILayout.TextField("file ", m.GetFileName());
                EditorGUILayout.Toggle("unlocked", m.IsUnlocked());
                EditorGUILayout.Toggle("activated", m.IsActivated());

                int indent = EditorGUI.indentLevel;
                EditorGUI.indentLevel++;

                for (int j = 0; j < m.subgoals.Length; j++)
                {
                    EditorGUILayout.IntField("subgoal " + j, m.subgoals[j].currentValue);
                }

                EditorGUI.indentLevel = indent;

                EditorGUILayout.Space();
            }
        }
    }
}