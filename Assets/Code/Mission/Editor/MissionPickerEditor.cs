﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Progress.Missions
{
    [CustomEditor(typeof(MissionPicker))]
    public class MissionPickerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (Application.isPlaying && GUILayout.Button("Switch State"))
            {
                ((MissionPicker)target).SwitchState();
            }
        }
    }
}