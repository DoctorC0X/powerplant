﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using EventSystem;

public class MenuCamera : MonoBehaviour
{
    private static MenuCamera _instance;

    public static MenuCamera Instance
    {
        get { return _instance; }
    }

    public enum CamPosition { MID, UP, DOWN, LEFT, RIGHT };
    public CamPosition currentPos;
    public CamPosition lastPos;

    public Dictionary<CamPosition, Vector3> camPositionsDict;
    public List<CanvasBehaviour> canvases;
    public Dictionary<CamPosition, GameObject> canvasesDict;

    private Camera cam;

    public Vector2 viewPortSize;

    public float verticalSwitchSpeed;
    public float horizontalSwitchSpeed;

    public float currentSwitchSpeed;

    public float maxFractionTillSwitch;

    [Range(0.00001f, 1)]
    public float shortSwitchSpeedReduction;

    public bool switching;

    public AnimationCurve switchCurve;

    private Vector3 startPos;
    private Vector3 targetPos;
    private float distance;
    private float startTime;

    private float currentFraction;

    public event Action OnStartSwitch;
    public event Action<CamPosition> OnEndSwitch;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else
        {
            _instance = this;
        }

        cam = this.GetComponent<Camera>();

        // -------- saving canvases in a dictionary for easier access
        canvasesDict = new Dictionary<CamPosition, GameObject>();

        foreach (CanvasBehaviour current in canvases)
        {
            canvasesDict.Add(current.myPos, current.gameObject);
        }
        // -----------------------------------

        // -------- saving camera positions in a dictionary for easier access
        camPositionsDict = new Dictionary<CamPosition, Vector3>();
        
        
        camPositionsDict.Add(CamPosition.MID, this.transform.position);
        currentPos = CamPosition.MID;

        // left & right
        viewPortSize.x = (cam.ViewportToWorldPoint(new Vector2(1, 0)).x - this.transform.position.x) * 2;

        Vector3 leftPos  = this.transform.position - new Vector3(viewPortSize.x, 0);
        Vector3 rightPos = this.transform.position + new Vector3(viewPortSize.x, 0);

        if(canvasesDict.ContainsKey(CamPosition.LEFT))
            camPositionsDict.Add(CamPosition.LEFT , leftPos);

        if(canvasesDict.ContainsKey(CamPosition.RIGHT))
            camPositionsDict.Add(CamPosition.RIGHT, rightPos);

        // up & down
        viewPortSize.y = (cam.ViewportToWorldPoint(new Vector2(0, 1)).y - this.transform.position.y) * 2;

        Vector3 downPos = this.transform.position - new Vector3(0, viewPortSize.y);
        Vector3 upPos   = this.transform.position + new Vector3(0, viewPortSize.y);

        if(canvasesDict.ContainsKey(CamPosition.DOWN))
            camPositionsDict.Add(CamPosition.DOWN, downPos);

        if(canvasesDict.ContainsKey(CamPosition.UP))
            camPositionsDict.Add(CamPosition.UP  , upPos);
        // -----------------------------------
    }

	// Use this for initialization
	void Start () 
    {
        //MainMenuSwiper.Instance.OnSwipe += OnSwipe;
        //MainMenuSwiper.Instance.OnRelease += OnRelease;

        ClickManager.Instance.OnSwipe += HandleSwipe;
        ClickManager.Instance.OnRelease += HandleRelease;

        EventManager.Instance.AddHandler(EventData.ID.GAMESTART, (EventData.ParamBase b) => Destroy(this));
	}

	// Update is called once per frame
	void Update () 
    {
        if (switching)
        {
            float traveled = (Time.time - startTime) * currentSwitchSpeed;

            currentFraction = traveled / distance;

            this.transform.position = Vector3.Lerp(startPos, targetPos, switchCurve.Evaluate(currentFraction));

            if (currentFraction >= 1)
            {
                switching = false;

                if (OnEndSwitch != null)
                    OnEndSwitch(currentPos);
            }
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            SwitchCamPos(CamPosition.UP);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            SwitchCamPos(CamPosition.DOWN);
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            SwitchCamPos(CamPosition.LEFT);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            SwitchCamPos(CamPosition.RIGHT);
        }
	}

    // handle swipe input (peak & switch)
    public void HandleSwipe(ClickManager.SwipeInfo info)
    {
        if (!switching)
        {
            CamPosition direction = CalcDir(info.swipeDir);

            // just peaking
            //CamPeak(direction, info.fraction);

            if (info.fraction > maxFractionTillSwitch)
            {
                // max fraction reached, now switch
                SwitchCamPos(direction);
            }
        }
    }

    public void HandleRelease(bool wasSwiping)
    {
        if(!switching)
            SwitchCamPos(currentPos);
    }

    public void SwitchCamPos(string dirString)
    {
        CamPosition dir = (CamPosition) Enum.Parse(typeof(CamPosition), dirString);

        SwitchCamPos(dir);
    }

    public void SwitchCamPos(CamPosition dir, float startFraction = 0)
    {
        // decide here in which direction to go (based on the current pos)
        if (!switching)
        {
            CamPosition target = CalcTarget(dir, out currentSwitchSpeed);

            if (camPositionsDict.ContainsKey(target))
                MoveCamTo(target, startFraction);
        }
        else
        {
            CamPosition newTarget = CalcTarget(dir, out currentSwitchSpeed);

            // in mid-switch only going back is possible
            if (newTarget == lastPos && currentPos != lastPos && camPositionsDict.ContainsKey(newTarget))
            {
                MoveCamTo(newTarget, 1 - currentFraction);
            }
        }
    }

    // peak, but do not switch position yet
    public void CamPeak(CamPosition dir, float fraction)
    {
        if (!switching)
        {
            CamPosition target = CalcTarget(dir);

            if(camPositionsDict.ContainsKey(target))
                this.transform.position = Vector3.Lerp(camPositionsDict[currentPos], camPositionsDict[target], switchCurve.Evaluate(fraction));
        }
    }

    // initialize camera position interpolation
    private void MoveCamTo(CamPosition pos, float startFraction)
    {
        startFraction = Mathf.Clamp(startFraction, 0, 1);

        // because of peaking, the startFraction is not always 0
        if (startFraction == 0)
            startPos = this.transform.position;
        else
            startPos = camPositionsDict[currentPos];

        targetPos = camPositionsDict[pos];

        if (startPos != targetPos)
        {
            switching = true;

            distance = (startPos - targetPos).magnitude;

            // switch speed reduced when returning to currrent fix point
            if (currentPos == pos)
                currentSwitchSpeed *= shortSwitchSpeedReduction;

            // if not starting at 0, startTime needs to be manipulated (put into past)
            if (startFraction == 0)
                startTime = Time.time;
            else
                startTime = Time.time - ((distance * startFraction) / currentSwitchSpeed);

            lastPos = currentPos;
            currentPos = pos;

            AudioSources.Instance.menuTransition.PlayDelayed(0.55f);

            if (OnStartSwitch != null)
                OnStartSwitch();
        }
    }

    private CamPosition CalcTarget(CamPosition dir)
    {
        float tmp = 0;

        return CalcTarget(dir, out tmp);
    }

    // from the current position, get the target based on swipe dir (also adjust the switchSpeed to horizontal & vertical)
    private CamPosition CalcTarget(CamPosition dir, out float switchSpeed)
    {
        switch (currentPos)
        {
            case CamPosition.MID:
                {
                    if (dir == CamPosition.UP || dir == CamPosition.DOWN)
                    {
                        switchSpeed = verticalSwitchSpeed;
                    }
                    else
                    {
                        switchSpeed = horizontalSwitchSpeed;
                    }

                    return dir;
                }
            case CamPosition.UP:
                {
                    if (dir == CamPosition.DOWN)
                    {
                        switchSpeed = verticalSwitchSpeed;

                        return CamPosition.MID;
                    }

                    break;
                }
            case CamPosition.DOWN:
                {
                    if (dir == CamPosition.UP)
                    {
                        switchSpeed = verticalSwitchSpeed;

                        return CamPosition.MID;
                    }

                    break;
                }
            case CamPosition.LEFT:
                {
                    if (dir == CamPosition.RIGHT)
                    {
                        switchSpeed = horizontalSwitchSpeed;

                        return CamPosition.MID;
                    }

                    break;
                }
            case CamPosition.RIGHT:
                {
                    if (dir == CamPosition.LEFT)
                    {
                        switchSpeed = horizontalSwitchSpeed;

                        return CamPosition.MID;
                    }

                    break;
                }
            default:
                {
                    throw new ArgumentException("Invalid CameraPosition!");
                }
        }

        switchSpeed = horizontalSwitchSpeed;

        return currentPos;
    }

    private CamPosition CalcDir(Vector2 swipeDir)
    {
        CamPosition dir = CamPosition.MID;

        if (Mathf.Abs(swipeDir.x) > Mathf.Abs(swipeDir.y))
        {
            dir = swipeDir.x <= 0 ? CamPosition.RIGHT : CamPosition.LEFT;
        }
        else
        {
            dir = swipeDir.y <= 0 ? CamPosition.UP : CamPosition.DOWN;
        }

        return dir;
    }

    void OnDestroy()
    {
        ClickManager.Instance.OnSwipe -= HandleSwipe;
        ClickManager.Instance.OnRelease -= HandleRelease;
    }
}