﻿using UnityEngine;
using System.Collections;
using EventSystem;

public class IngameCamera : MonoBehaviour 
{
    private Transform target;

    public bool follow;
    public bool shake;

    public float speed;
    public float maxDistance;

    public float lowestHeight;

    public float screenHeight;

	// Use this for initialization
	void Start () 
    {
        target = PlantModel.MainPlant.plantFace.transform;

        EventManager.Instance.AddHandler(EventData.ID.GAMESTART, (EventData.ParamBase b) =>
            {
                enabled = true;
                follow = true;
            });

        enabled = false;

        lowestHeight = float.MinValue;

        screenHeight = FollowCanvas.Instance.topLeft.position.y - FollowCanvas.Instance.botRight.position.y;
	}

    void Update()
    {
        if (follow)
        {
            float distance = Mathf.Clamp(target.position.y, lowestHeight, float.MaxValue) - transform.position.y;

            if (Mathf.Abs(distance) >= speed * Time.deltaTime)
            {
                transform.Translate(0, Mathf.Sign(distance) * Mathf.Clamp(Mathf.Abs(distance) / maxDistance, 0, 1) * speed * Time.deltaTime, 0);
            }
        }
    }

    public void SetLowestBorder(float height)
    {
        lowestHeight = height + (screenHeight / 2);
    }
}
