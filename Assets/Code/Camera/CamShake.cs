﻿using UnityEngine;
using System.Collections;

public class CamShake : MonoBehaviour 
{
    public bool shaking;

    public int shakeAmount;

    public float speedMin;
    public float speedMax;

    public Vector2 offsetMax;

    public int shakeCountdown;

    private Vector3 initialPos;

    private Vector3 startPos;
    private Vector3 endPos;

    public InterpolatorBase interpolator;

    void Start()
    {
        interpolator = InterpolationManager.Instance.CreateInterpolator(InterpolationManager.Type.SPEED);

        interpolator.AddUpdateListener(UpdatePos);
        interpolator.AddDoneListener(TargetReached);
    }

    public void StartShake()
    {
        shaking = true;
        shakeCountdown = shakeAmount;

        initialPos = transform.position;

        SetValues();

        interpolator.StartInterpolation(new SpeedParameter(1, Random.Range(speedMin, speedMax), (endPos - startPos).magnitude));
    }

    public void UpdatePos(float fraction)
    {
        transform.position = Vector3.Lerp(startPos, endPos, fraction);
    }

    public void TargetReached()
    {
        shakeCountdown--;

        if (shakeCountdown > 0)
        {
            SetValues();
            interpolator.StartInterpolation(new SpeedParameter(1, Random.Range(speedMin, speedMax), (endPos - startPos).magnitude));
        }
        else if (shakeCountdown == 0)
        {
            SetValues(initialPos);
            interpolator.StartInterpolation(new SpeedParameter(1, Random.Range(speedMin, speedMax), (endPos - startPos).magnitude));
        }
        else if (shakeCountdown < 0)
        {
            shaking = false;
        }
    }

    private void SetValues(Vector3 start, Vector3 end)
    {
        startPos = start;
        endPos = end;
    }

    private void SetValues(Vector3 end)
    {
        SetValues(transform.position, end);
    }

    private void SetValues()
    {
        SetValues(initialPos + new Vector3(Random.Range(-offsetMax.x, offsetMax.x), Random.Range(-offsetMax.y, offsetMax.y), 0));
    }
}
