﻿using UnityEngine;
using System.Collections;

public class PlantNeck : MonoBehaviour
{
    public PlantModel plantModel;

    public void Init(PlantModel pm)
    {
        plantModel = pm;
    }

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (plantModel.highestPiece != null)
        {
            /*
            GameObject top = plantModel.highestPiece;
            transform.position = top.transform.position + (plantModel.pieceHeight / 2) * top.transform.up;
            transform.rotation = top.transform.rotation;
            */

            transform.position = plantModel.plantHead.transform.position;
            transform.rotation = plantModel.plantHead.transform.rotation;
        }
	}
}
