﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlantpieceModel : MonoBehaviour, GameOverListener, PauseListener
{
    public PlantModel plantModel;

    public Rigidbody2D rBody;
    public HingeJoint2D joint;

    public PlantpieceModel next;
    public PlantpieceModel prev;

    public SpriteRenderer pieceSpriteRenderer;

    public static int normalLayer = 14;
    public static int ignoreLayer = 15;

    public static PlantpieceModel brokePiece;

    private bool paused;
    private Vector2 linearVelocity;
    private float angularVelocity;

    private Vector3 landingPos;

    void Awake()
    {
        rBody = GetComponent<Rigidbody2D>();
        joint = GetComponent<HingeJoint2D>();
    }

    public void Init(PlantModel pm)
    {
        plantModel = pm;

        next = null;
        prev = null;

        SetPause(GameManager.Instance.paused);

        rBody.mass = plantModel.pData.pieceMass;

        JointAngleLimits2D limits = new JointAngleLimits2D();
        limits.max = plantModel.pData.pieceJointAngleLimit;
        limits.min = limits.max * -1;
        joint.limits = limits;
    }

    public void AddToPlant(Transform parent, PlantpieceModel predecessor)
    {
        // is this not the first piece?
        if (predecessor != null)
        {
            // set next and previous piece (for recursive methods)
            prev = predecessor;
            prev.next = this;

            // set pos + rot
            transform.position = prev.transform.position + prev.transform.TransformDirection(0, plantModel.pieceHeight, 0);
            transform.rotation = prev.transform.rotation;

            // set joint
            joint.connectedBody = prev.rBody;
        }
        else
        {
            // set position
            transform.position = plantModel.transform.position + new Vector3(0, plantModel.pieceHeight/2, 0);

            // set joint
            joint.connectedBody = null;
            joint.connectedAnchor = plantModel.transform.position;
        }

        transform.SetParent(parent);

        //transform.rotation = plantModel.plantHead.transform.rotation;

        // when done, enable collider + joint
        GetComponent<Collider2D>().enabled = true;
        joint.enabled = true;
    }

    public void Break()
    {
        //Destroy(GetComponent<HingeJoint2D>());
        joint.enabled = false;
        brokePiece = this;

        //plantModel.plantHead.tailEnd = this;

        if (prev != null)
            prev.GetComponent<Collider2D>().enabled = false;

        //plantModel.Broken = true;

        CallBrokenRecursiveUp(ignoreLayer);
    }

    public void CallBrokenRecursiveUp(int layer)
    {
        // change layer when broke to ignore underlying magnets 
        gameObject.layer = layer;

        if (next != null)
        {
            next.CallBrokenRecursiveUp(layer);
        }
    }

    public void SetKinematic(bool kinematic)
    {
        rBody.isKinematic = kinematic;
    }

    public void GameOver()
    {
        rBody.isKinematic = false;
        rBody.constraints = RigidbodyConstraints2D.None;

        Destroy(joint);
        Destroy(this);
    }

    public void FixPiece()
    {
        /*
        // set kinematic
        GetComponent<Rigidbody2D>().isKinematic = true;

        // change sprite
        GetComponentInChildren<SpriteRenderer>().sprite = plantModel.pData.pieceFixSprite;

        // destroy unneeded components
        Destroy(GetComponent<BoxCollider2D>());
        Destroy(GetComponent<HingeJoint2D>());

        // next piece´s joint needs to be attached to world space
        next.SetHingeJointToWorld();
        next.prev = null;

        // no need for rigidbody no more, no no
        Destroy(GetComponent<Rigidbody2D>());

        // remove from plantModels array
        plantModel.plantPieces.Remove(this.gameObject);

        // move to background
        this.transform.position += new Vector3(0, 0, 1);

        // seppuku
        Destroy(this);
        */

        // next piece´s joint needs to be attached to world space
        next.SetHingeJointToWorld();
        next.prev = null;

        // disable unneeded components
        GetComponent<BoxCollider2D>().enabled = false;
        joint.enabled = false;
        joint.connectedAnchor = new Vector2(0, 0.5f);

        // remove from plantModels array
        plantModel.plantPiecesActive.Remove(this);

        // get back into the pool 
        plantModel.ReleasePieceToPool(gameObject);
    }

    public void SetHingeJointToWorld()
    {
        Vector2 point = this.transform.position + this.transform.up * (-plantModel.pieceHeight / 2);

        joint.connectedBody = null;
        joint.connectedAnchor = point;
    }

    public void SetPause(bool pause)
    {
        paused = pause;

        if (pause)
        {
            linearVelocity = rBody.velocity;
            angularVelocity = rBody.angularVelocity;

            rBody.isKinematic = true;
        }
        else
        {
            rBody.isKinematic = false;

            rBody.velocity = linearVelocity;
            rBody.angularVelocity = angularVelocity;
        }
    }

    public bool IsPaused()
    {
        return paused;
    }

    // when broken
    public void GiveRelativeAccImpulse(Vector2 accImpulseVector)
    {
        rBody.AddRelativeForce(accImpulseVector * rBody.mass, ForceMode2D.Impulse);
        Debug.DrawRay(this.transform.position, this.transform.TransformDirection((accImpulseVector) / 10), Color.magenta, 3);
    }
}
