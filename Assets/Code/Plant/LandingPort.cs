﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using EventSystem;

public class LandingPort : MonoBehaviour
{
    private PlantModel plantModel;

    public enum Side { LEFT = 0, RIGHT = 1 }

    private bool initialized;

    public int fixedOnIndexFromHead;

    public Transform currentTarget;
    private Transform oldTarget;

    private bool reached;
    private float startTime;

    public GameObject leafLeft;
    public GameObject leafRight;

    public LandingSpot spotLeft;
    public LandingSpot spotRight;

    public void Init(PlantModel pm)
    {
        plantModel = pm;

        EventManager.Instance.AddHandler(EventData.ID.PIECES, UpdateTargetTransform);

        reached = true;

        spotLeft.Init(this);
        spotRight.Init(this);

        currentTarget = plantModel.plantPiecesActive[plantModel.plantPiecesActive.Count - fixedOnIndexFromHead].transform;
        initialized = true;
    }

	// Update is called once per frame
	void Update () 
    {
        if (initialized)
        {
            if (!reached)
            {
                float fraction = (Time.time - startTime) / plantModel.pData.growthDuration;

                transform.position = Vector3.Lerp(oldTarget.position, currentTarget.position, fraction);
                transform.rotation = Quaternion.Lerp(oldTarget.rotation, currentTarget.rotation, fraction);

                if (fraction >= 1)
                {
                    reached = true;
                }
            }

            if (reached)
            {
                transform.position = currentTarget.position;
                transform.rotation = currentTarget.rotation;
            }
        }
	}

    public void UpdateTargetTransform()
    {
        if (plantModel.plantPiecesActive.Count - fixedOnIndexFromHead >= 0)
        {
            oldTarget = currentTarget;
            currentTarget = plantModel.plantPiecesActive[plantModel.plantPiecesActive.Count - fixedOnIndexFromHead].transform;

            reached = false;
            startTime = Time.time;
        }
    }

    public LandingSpot GetFreeLandingSpot()
    {
        if (!spotLeft.IsOccupied() && !spotLeft.IsReserved())
            return spotLeft;

        if (!spotRight.IsOccupied() && !spotRight.IsReserved())
            return spotRight;

        return null;
    }

    public int GetFreeLandingSpotCount()
    {
        int result = 0;

        if (!spotLeft.IsOccupied())
            result++;

        if (!spotRight.IsOccupied())
            result++;

        return result;
    }
}
