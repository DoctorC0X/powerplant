﻿using UnityEngine;
using System.Collections;
using EventSystem;

public class LandingSpot : CritterLandingSpot 
{
    public LandingPort port;

    public EventData.ID landingEventId;
    public EventData.ID startingEventId;

    public void Init(LandingPort port)
    {
        this.port = port;
    }

    public override void Attach(Critter c)
    {
        base.Attach(c);

        EventManager.Instance.CallEvent(landingEventId, new EventData.ParamCritter() { increase = 1, critter = attached });
    }

    public override void Detach()
    {
        EventManager.Instance.CallEvent(startingEventId, new EventData.ParamCritter() { increase = 1, critter = attached });

        base.Detach();
    }
}