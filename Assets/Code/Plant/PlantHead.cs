﻿using UnityEngine;
using System.Collections;
using System;
using EventSystem;

public class PlantHead : MonoBehaviour, PauseListener
{
    public PlantModel plantModel;

    public PlantHeadPivot pivot;

    public Rigidbody2D rBody;
    public HingeJoint2D joint;

    public bool paused;
    private Vector2 linearVelocity;
    private float angularVelocity;

    private bool slowMo;

    public float maxVelocity;
    public float maxDistance;

    public float targetGrowthOffset;

    public float distanceCheckDelay;
    private float distanceCheckTimer;
    private Vector3 lastPos;

    public Vector3 Center
    {
        get { return transform.position + new Vector3(0, 1, 0); }
    }

    public void Init(PlantModel pm)
    {
        plantModel = pm;

        rBody = GetComponent<Rigidbody2D>();
        rBody.mass = plantModel.pData.faceMass;

        //GamestartEvent.Instance.OnGamestart += () => GetComponent<WindVictim>().maxAcceleration = plantModel.pData.faceMaxWindAcc;

        joint = GetComponent<HingeJoint2D>();

        JointAngleLimits2D limits = new JointAngleLimits2D();
        limits.max = plantModel.pData.faceJointAngleLimit;
        limits.min = limits.max * -1;
        joint.limits = limits;

        pivot.Init(plantModel);

        EventManager.Instance.AddHandler(EventData.ID.GAMESTART, () => lastPos = transform.position);

        distanceCheckTimer = distanceCheckDelay;
    }

    void FixedUpdate()
    {
        /*
        // handle critter weight on plantFace
        if (plantModel.landingPort.accOnPlayer.sqrMagnitude > 0)
        {
            GiveRelativeAcc(plantModel.landingPort.accOnPlayer);
            Debug.DrawRay(transform.position, plantModel.landingPort.accOnPlayer, Color.blue);
        }
        */

        if (!paused && !GameManager.Instance.gameOver && !plantModel.Broken)
        {
            Debug.DrawRay(transform.position, rBody.velocity, Color.black);

            Vector2 distance = pivot.target.transform.position - transform.position;

            rBody.velocity = distance.normalized * (maxVelocity * Mathf.Clamp(distance.magnitude / maxDistance, 0, 1));
        }
    }

    void Update()
    {
        if (!paused && GameManager.Instance.gameActive && !plantModel.Broken)
        {
            distanceCheckTimer -= Time.deltaTime;

            if (distanceCheckTimer <= 0)
            {
                distanceCheckTimer = distanceCheckDelay;

                int distance = Mathf.RoundToInt((transform.position - lastPos).magnitude);
                
                if(distance > 0)
                    EventManager.Instance.CallEvent(EventData.ID.TRAVELLED, new EventData.ParamBase() { increase = distance });

                lastPos = transform.position;
            }
        }
    }

    public void GiveAccImpulse(Vector2 accImpulseVector)
    {
        rBody.AddForce(accImpulseVector * rBody.mass, ForceMode2D.Impulse);
        Debug.DrawRay(transform.position, accImpulseVector / 10, Color.magenta, 3);
    }

    public void GiveAcc(Vector2 accVector)
    {
        rBody.AddForce(accVector * rBody.mass);
        //Debug.DrawRay(transform.position, accVector, Color.cyan);
    }

    // when broken
    public void GiveRelativeAccImpulse(Vector2 accImpulseVector)
    {
        rBody.AddRelativeForce(accImpulseVector * rBody.mass, ForceMode2D.Impulse);
        Debug.DrawRay(this.transform.position, this.transform.TransformDirection((accImpulseVector) / 10), Color.magenta, 3);
    }

    // used for the critter weights
    public void GiveRelativeAcc(Vector2 acc)
    {
        rBody.AddRelativeForce(acc * rBody.mass, ForceMode2D.Force);
        Debug.DrawRay(transform.position, transform.TransformDirection(acc / 10), Color.blue);
    }

    public void OnGrowth()
    {
        PlantpieceModel top = plantModel.highestPiece;

        transform.position = top.transform.position + top.transform.TransformDirection(0, plantModel.pieceHeight * 0.5f, 0);
        transform.rotation = top.transform.rotation;

        // connect joints
        joint.connectedBody = top.rBody;
    }

    public void SetPause(bool pause)
    {
        paused = pause;

        if (pause)
        {
            Rigidbody2D rb = this.rBody;

            linearVelocity = rb.velocity;
            angularVelocity = rb.angularVelocity;

            rb.isKinematic = true;
        }
        else
        {
            Rigidbody2D rb = this.rBody;

            rb.isKinematic = false;

            rb.velocity = linearVelocity;
            rb.angularVelocity = angularVelocity;
        }
    }

    public bool IsPaused()
    {
        return paused;
    }

    public void OnCollisionEnter2D(Collision2D c)
    {
        if (c.collider.tag == Tags.ground)
        {
            EventManager.Instance.CallEvent(EventData.ID.GROUNDTOUCH_ENTER);
        }
    }

    public void OnCollisionExit2D(Collision2D c)
    {
        if (c.collider.tag == Tags.ground)
        {
            EventManager.Instance.CallEvent(EventData.ID.GROUNDTOUCH_EXIT);
        }
    }
}

/*
if (!paused && !GameManager.Instance.gameOver && !plantModel.Broken)
{
    Debug.DrawRay(transform.position, rBody.velocity, Color.black);

    Vector2 desiredVelDir = target.position - transform.position;
    float distanceFraction = Mathf.Clamp((desiredVelDir.magnitude / maxDistance), 0, 1);
    float desiredVel = maxVelocity * distanceFraction;
    desiredVelDir.Normalize();

    Debug.DrawRay(transform.position, (desiredVelDir * desiredVel), Color.white);

    Vector2 accDir = ((desiredVelDir * desiredVel) - rBody.velocity).normalized;

    Debug.DrawRay(transform.position, accDir * maxAcceleration, Color.red);

    rBody.velocity += accDir * (maxAcceleration * distanceFraction);
}
*/

// damper
/*
if (!paused && !GameManager.Instance.gameOver && !plantModel.Broken)
{
    Vector2 actualVel = rBody.velocity;

    float angleFraction = CalcAngleFraction();

    // desired velocity (dir = to mid. value = distance * max)
    //Vector2 desiredVelDir = GetArcDir(Mathf.Sign(transform.position.x) * -1);
    Vector2 desiredVelDir = GetArcDir(GetHeadSide() * -1);

    float desiredVel = plantModel.pData.damperVelMax * angleFraction;
    //

    Debug.DrawRay(transform.position, (desiredVelDir * desiredVel), Color.white);

    // acceleration to reach desired velocity (dir = desiredV - currentV)
    Vector2 accDir = ((desiredVelDir * desiredVel) - actualVel).normalized;
    float accValue = plantModel.pData.damperAcc * (1 - angleFraction) * Mathf.Clamp(CalcDistanceFraction() * 10, 0, 1);
    //

    Debug.DrawRay(transform.position, accDir * accValue * 10, Color.red);

    rBody.velocity += accDir * accValue;
}
*/

/*
public bool HeadBelow
{
    get { return _headBelow; }
    set
    {
        if (_headBelow != value)
        {
            _headBelow = value;

            if (!_headBelow)
            {
                breakTimer = plantModel.pData.breakTimer;
                plantModel.plantPiecesActive[0].GetComponent<PlantpieceModel>().SetColor(0);
            }

            if (OnHeadBelow != null)
                OnHeadBelow(_headBelow, (int)Mathf.Sign(transform.localPosition.x));
        }
    }
}
*/

/*
void Update()
{
        
    if (!plantModel.Broken && !paused && !GameManager.Instance.gameOver && GameManager.Instance.gameActive)
    {
        HeadTilted = Vector3.Angle(this.transform.up, Vector3.up) > 90;

        // trigger slowMo when to close to the ground and face is tilted 90°
        if (!slowMo && HeadTilted && transform.position.y < highestY / 4 && rBody.velocity.y < 0)
        {
            slowMo = true;
            plantModel.SlowMo(true);
        }

        if (slowMo && rBody.velocity.y > 0)
        {
            slowMo = false;
            plantModel.SlowMo(false);
        }

        HeadBelow = this.transform.position.y < plantModel.plantPiecesActive[0].transform.position.y;

        // if face is beneath the lowest unfixed piece, start countdown for breaking
        if (HeadBelow && checkBreak)
        {
            breakTimer -= Time.deltaTime;

            float fraction = 1 - (breakTimer / plantModel.pData.breakTimer);

            plantModel.plantPiecesActive[0].GetComponent<PlantpieceModel>().SetColor(fraction);

            if (breakTimer <= 0)
            {
                if (plantModel.allowBreak && !plantModel.enableHelpBelow)
                {
                    plantModel.Break(0);
                    plantModel.plantPiecesActive[0].GetComponent<PlantpieceModel>().SetColor(0);
                }
                    
                if(!plantModel.allowBreak && plantModel.enableHelpBelow)
                {
                    GivePerfectImpulse();

                    checkBreak = false;

                    gameObject.AddComponent<Coworker>().StartDelayedAction(2.5f, null, () => checkBreak = true);
                }
            }
        }
    }
}
*/