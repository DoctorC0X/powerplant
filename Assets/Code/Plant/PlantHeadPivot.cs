﻿using UnityEngine;
using System.Collections;
using EventSystem;

public class PlantHeadPivot : MonoBehaviour
{
    private PlantModel plantModel;

    public GameObject target;

    public GameObject groundCollider;

    public float rotationSpeed;

    public void Init(PlantModel pm)
    {
        plantModel = pm;

        EventManager.Instance.AddHandler(EventData.ID.PIECES, OnGrowth);
    }

    public void Rotate(float fraction)
    {
        float rotAngle = rotationSpeed * fraction * Time.deltaTime;

        if ((groundCollider.transform.position - transform.position).sqrMagnitude <= (target.transform.localPosition.y * target.transform.localPosition.y))
        {
            Quaternion rot = Quaternion.AngleAxis(rotAngle, transform.forward);

            Vector3 dir = rot * (transform.up * target.transform.localPosition.y);

            dir = transform.position + dir;

            Debug.DrawLine(transform.position, dir, Color.red);

            if (dir.y > groundCollider.transform.position.y)
                transform.Rotate(0, 0, rotAngle);
        }
        else
        {
            transform.Rotate(0, 0, rotAngle);
        }
    }

    public void SetOffset(float fraction)
    {
        target.transform.position = plantModel.plantHead.transform.TransformPoint(plantModel.plantHead.targetGrowthOffset * fraction, 0, 0);
    }

    public void OnGrowth(EventData.ParamBase param)
    {
        UpdatePos();
    }

    public void OnGrowthEnd()
    {
        transform.up = plantModel.plantHead.transform.position - transform.position;

        //target.transform.parent = transform;
        target.transform.localPosition = new Vector3(0, (plantModel.plantPiecesActive.Count * plantModel.pieceHeight)); ;
    }

    public void UpdatePos()
    {
        transform.position = plantModel.plantPiecesActive[0].transform.position - plantModel.plantPiecesActive[0].transform.TransformDirection(0, plantModel.pieceHeight / 2, 0);
    }
}
