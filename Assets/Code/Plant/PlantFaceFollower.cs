﻿using UnityEngine;
using System.Collections;
using System;

public class PlantFaceFollower : MonoBehaviour 
{
    public float yOffset;

    public bool upOnly = true;
    public bool round = true;

    public int roundToDigit = 1;

    public Action<float, float> OnMove;

    // Update is called once per frame
    public virtual void Update()
    {
        float currentY = 0;
        float plantFaceY = 0;

        GetValues(out currentY, out plantFaceY);

        if (upOnly)
        {
            if (plantFaceY > currentY)
            {
                this.transform.position = new Vector3(this.transform.position.x, plantFaceY, this.transform.position.z);

                if(OnMove != null)
                    OnMove(currentY, plantFaceY);
            }
        }
        else
        {
            if (plantFaceY != currentY)
            {
                this.transform.position = new Vector3(this.transform.position.x, plantFaceY, this.transform.position.z);

                if (OnMove != null)
                    OnMove(currentY, plantFaceY);
            }
        }
    }

    private void GetValues(out float currentY, out float plantFaceY)
    {
        if (round)
        {
            plantFaceY = Mathf.Round((PlantModel.MainPlant.plantFace.transform.position.y + yOffset) * (10 * roundToDigit)) / (10 * roundToDigit);
            currentY = Mathf.Round(this.transform.position.y * (10 * roundToDigit)) / (10 * roundToDigit);
        }
        else
        {
            plantFaceY = PlantModel.MainPlant.plantFace.transform.position.y + yOffset;
            currentY = this.transform.position.y;
        }
    }
}
