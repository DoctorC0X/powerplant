﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using EventSystem;

public class PlantModel : MonoBehaviour, GameOverListener, PauseListener
{
    private static PlantModel _mainPlant;

    public static PlantModel MainPlant
    {
        get 
        {
            if (_mainPlant == null)
                _mainPlant = GameObject.FindGameObjectWithTag(Tags.mainPlant).GetComponent<PlantModel>();

            return _mainPlant;
        }
    }

    public PlantInputManager inputManager;
    public PlantHead plantHead;
    public SkillManager skillManager;
    public PlantNeck plantNeck;
    public PlantFace plantFace;
    public LandingPort landingPort;
    public Line line;
    public HighscoreWatcher highscoreWatcher;
    public StarDustMagnet dustMagnet;
    public Thrower numberThrower;

    public PlantData pData;

    public GameObject highscoreWatcherPrefab;
    public GameObject starDustMagnetPrefab;
    public GameObject prefabThrowerPrefab;

    public List<PlantpieceModel> plantPiecesActive;
    //public GameObject lastFixedPiece;

    public Queue<GameObject> plantPiecesPool;

    public GameObject parentPiecePool;
    public GameObject parentPieceFixed;

    public float pieceHeight;

    public bool growing;

    private bool paused;

    public bool slowMo;

    public bool allowHeightGrowth;
    public bool enableHelpBelow;

    private bool _broken;
    public bool Broken
    {
        get { return _broken; }
        set
        {
            _broken = value;

            if (_broken && OnPlantBroke != null)
                OnPlantBroke();
        }
    }

    public event Action OnPlantBroke;

    public int Pieces
    {
        get { return Progress.ValueManager.Instance.Values[EventData.ID.PIECES].currentRound; }
    }

    void Awake()
    {
        /*
        if (_mainPlant != null && tag.Equals(Tags.mainPlant))
        {
            _mainPlant = this;
        }
        */

        // create parents for non active pieces
        parentPiecePool = new GameObject("pooledPieces Parent");
        parentPiecePool.transform.position = Vector3.zero;

        parentPieceFixed = new GameObject("fixedPieces Parent");
        parentPieceFixed.transform.position = Vector3.zero;

        PlantpieceModel.brokePiece = null;

        // fill piece pool
        plantPiecesPool = new Queue<GameObject>(pData.pieceFixAtIndex + 1);
        for (int i = 0; i < pData.pieceFixAtIndex + 1; i++)
        {
            GameObject go = Instantiate(pData.piecePrefab) as GameObject;
            ReleasePieceToPool(go);
        }

        plantPiecesActive = new List<PlantpieceModel>(pData.pieceFixAtIndex + 1);
        pieceHeight = pData.piecePrefab.transform.localScale.y;
    }

    void Start()
    {
        if (!Application.isEditor)
        {
            pData = ScriptableObject.Instantiate<PlantData>(pData);
        }

        Time.timeScale = 1;

        FillExisting();

        GameManager.Instance.gameOverListener.Add(this);
        GameManager.Instance.pauseListener.Add(this);

        InitExisting();

        SpawnMissing();

        InitMissing();

        // allow initial growth only when menu cam is in mid position
        MenuCamera.Instance.OnStartSwitch += () => allowHeightGrowth = false;
        MenuCamera.Instance.OnEndSwitch += (MenuCamera.CamPosition pos) =>
        {
            if (pos == MenuCamera.CamPosition.MID)
                allowHeightGrowth = true;
        };

        _broken = false;

        //build all initial pieces, roots and leafs
        HandleStartValues(pData.startPieces);

        landingPort.Init(this);
    }

    public void BulletHit(Vector2 impulseVector)
    {
        // deactivate face indicator
        IndicatorManager.Instance.Activated = false;

        // stop cam from following
        Camera.main.GetComponent<IngameCamera>().follow = false;

        // break off and give impulse
        Break(0);

        NullifyInertia(true, true, true, true);

        plantHead.rBody.gravityScale = 0;
        plantHead.GiveAccImpulse(impulseVector);

        for(int i = 0; i < plantPiecesActive.Count; i++)
        {
            float fraction = ((float)i / (float)plantPiecesActive.Count);

            plantPiecesActive[i].rBody.gravityScale = 0;
            plantPiecesActive[i].rBody.velocity = plantHead.rBody.velocity * fraction;
        }
        //

        // show super smash effect
        Vector2 smashPos = (Vector2)plantHead.transform.position + plantHead.GetComponent<Rigidbody2D>().velocity * 10;

        smashPos = FollowCanvas.Instance.ClampToCanvas(smashPos);

        GameObject superSmashGo = Instantiate(Resources.Load("UI/superSmash")) as GameObject;
        superSmashGo.transform.SetParent(FollowCanvas.Instance.transform, false);
        superSmashGo.transform.position = smashPos;

        if (impulseVector.x > 0)
        {
            Vector3 oldScale = superSmashGo.transform.localScale;
            superSmashGo.transform.localScale = new Vector3(oldScale.x, oldScale.y * -1, oldScale.z);
        }

        plantFace.visibility.OnVisibilityChange += (bool visible) => 
        {
            if (!visible)
            {
                superSmashGo.SetActive(true);

                Camera.main.GetComponent<CamShake>().StartShake();
            }
        };
        //

        gameObject.AddComponent<Coworker>().StartDelayedAction(3, null, () => GameManager.Instance.TriggerGameOver());
    }

    public void Break(int pieceIndex)
    {
        plantPiecesActive[pieceIndex].GetComponent<PlantpieceModel>().Break();

        Broken = true;
    }

    public bool GrowHeight(bool forceGrow, bool pUserAction)
    {
        if ((allowHeightGrowth && !_broken && !GameManager.Instance.gameOver && !growing) || forceGrow)
        {
            NullifyInertia(true, true, false, true);

            // create plantPiece
            //GameObject newPiece = Instantiate(pData.piecePrefab) as GameObject;
            GameObject newPiece = GetPieceFromPool();

            // initialize plantPiece
            PlantpieceModel ppm = newPiece.GetComponent<PlantpieceModel>();
            ppm.Init(this);

            ppm.AddToPlant(transform, highestPiece);

            ppm.gameObject.SetActive(true);

            plantPiecesActive.Add(ppm);

            // lowest piece gets fixed
            if (plantPiecesActive.Count - pData.pieceFixAtIndex - 1 >= 0)
            {
                line.AddPoint(plantPiecesActive[0].transform.position + plantPiecesActive[0].transform.TransformDirection(0, pieceHeight / 2, 0));

                plantPiecesActive[0].FixPiece();
            }

            // growing is blocked until the previous growth is done
            growing = true;
            gameObject.AddComponent<Coworker>().StartDelayedAction(pData.growthDuration, null, () =>
                {
                    growing = false;

                    CheckBorder();
                });

            plantPiecesActive[0].joint.useLimits = false;

            EventManager.Instance.CallEvent(EventData.ID.PIECES);

            if (pUserAction && !GameManager.Instance.gameActive)
                EventManager.Instance.CallEvent(EventData.ID.GAMESTART);

            plantHead.OnGrowth();
            plantFace.OnGrowth();

            return true;
        }
        return false;
    }

    public void CheckBorder()
    {
        if (Mathf.Abs(plantHead.transform.localPosition.x) > pData.borderX)
        {
            float distance = pData.borderX - 1 + Mathf.Abs(plantHead.transform.localPosition.x);
            float dir = -1 * Mathf.Sign(plantHead.transform.localPosition.x);

            EventManager.Instance.CallEvent(EventData.ID.SCREEN_SWITCH);

            MovePlant(new Vector2(distance * dir, 0));
        }
    }

    // for switching sides when to far offscreen
    public void MovePlant(Vector3 vector, bool startNewLine = true)
    {
        plantHead.transform.Translate(vector, Space.World);
        plantFace.UpdatePos();

        foreach (PlantpieceModel current in plantPiecesActive)
        {
            current.transform.Translate(vector, Space.World);
        }

        Vector2 oldAnchor = plantPiecesActive[0].joint.connectedAnchor;
        plantPiecesActive[0].joint.connectedAnchor = oldAnchor + (Vector2)vector;

        //plantHead.pivot.UpdatePos();

        plantHead.pivot.transform.Translate(vector, Space.World);

        if (startNewLine)
        {
            line.StartNewLine();
            line.AddPoint(plantPiecesActive[0].transform.position);
        }
    }

    public void NullifyInertia(bool linear, bool angular, bool includeHead, bool includePieces)
    {
        if (linear || angular)
        {
            if (includeHead)
            {
                if (linear)
                    plantHead.rBody.velocity = Vector2.zero;

                if (angular)
                    plantHead.rBody.angularVelocity = 0;
            }

            if (includePieces)
            {
                foreach (PlantpieceModel current in plantPiecesActive)
                {
                    if (linear)
                        current.rBody.velocity = Vector2.zero;

                    if (angular)
                        current.rBody.angularVelocity = 0;
                }
            }
        }
    }

    public void SlowMo(bool desiredState)
    {
        if (slowMo != desiredState)
        {
            slowMo = desiredState;

            if(slowMo)
                Time.timeScale = 0.2f;
            else
                Time.timeScale = 1;
        }
    }

    public void GameOver()
    {
        Time.timeScale = 1;

        // break all hingeJoints when gameOver
        plantPiecesActive.Reverse();

        foreach (PlantpieceModel current in plantPiecesActive)
        {
            current.GameOver();
        }
    }

    public void SetPause(bool pause)
    {
        paused = pause;

        plantHead.SetPause(pause);

        foreach (PlantpieceModel current in plantPiecesActive)
        {
            current.SetPause(pause);
        }
    }

    public bool IsPaused()
    {
        return paused;
    }

    void OnDestroy()
    {
        if (Application.isEditor)
        {
            Resources.UnloadAsset(pData);
        }
    }

    public void ReleasePieceToPool(GameObject go)
    {
        go.SetActive(false);
        go.transform.parent = parentPiecePool.transform;
        go.transform.position = Vector3.zero;
        go.transform.rotation = Quaternion.identity;

        plantPiecesPool.Enqueue(go);
    }

    public GameObject GetPieceFromPool()
    {
        GameObject go = plantPiecesPool.Dequeue();

        return go;
    }

    public PlantpieceModel highestPiece
    {
        get
        {
            PlantpieceModel top = null;

            if (plantPiecesActive.Count - 1 >= 0)
                top = plantPiecesActive[plantPiecesActive.Count - 1];

            return top;
        }
    }

    private void FillExisting()
    {
        inputManager = GetComponent<PlantInputManager>();
        plantHead = GetComponentInChildren<PlantHead>();
        skillManager = GetComponent<SkillManager>();
        line = GetComponentInChildren<Line>();
        plantNeck = GetComponentInChildren<PlantNeck>();
        plantFace = GetComponentInChildren<PlantFace>();
        landingPort = GetComponentInChildren<LandingPort>();
    }

    private void InitExisting()
    {
        inputManager.Init(this);
        plantHead.Init(this);
        skillManager.Init(this);
        line.Init();
        plantNeck.Init(this);
        plantFace.Init(this);
    }

    private void SpawnMissing()
    {
        // Instantiate highscoreWatcher
        GameObject watcher = Instantiate(highscoreWatcherPrefab) as GameObject;
        watcher.transform.parent = plantNeck.transform;
        highscoreWatcher = watcher.GetComponent<HighscoreWatcher>();
        //

        // Instantiate starDustMagnet
        GameObject sMagnet = Instantiate(starDustMagnetPrefab) as GameObject;
        sMagnet.transform.parent = plantFace.transform;
        dustMagnet = sMagnet.GetComponent<StarDustMagnet>();
        //

        // Instantiate particleManager
        GameObject thrower = Instantiate(prefabThrowerPrefab) as GameObject;
        thrower.transform.parent = plantFace.transform;
        numberThrower = thrower.GetComponent<Thrower>();
        //
    }

    private void InitMissing()
    {
        highscoreWatcher.Init();
        dustMagnet.Init(this);
        numberThrower.Init();
    }

    private void HandleStartValues(int height)
    {
        // ----- grow start pieces
        for (int i = 0; i < height; i++)
        {
            GrowHeight(true, false);
        }
    }
}


/*
    public void FlipGravity()
    {
        breakEnabled = false;
        gravityFlipActive = true;
        gravityResetActive = false;

        plantFace.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        plantFace.GetComponent<Rigidbody2D>().angularVelocity = 0;

        foreach (GameObject current in plantPieces)
        {
            Rigidbody2D rb = current.GetComponent<Rigidbody2D>();

            if (rb != null)
            {
                current.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                current.GetComponent<Rigidbody2D>().angularVelocity = 0;
            }
        }

        gFlipper = new List<GravityFlipper>();

        gFlipper.Add(plantFace.gameObject.AddComponent<GravityFlipper>());

        foreach (GameObject current in plantPieces)
            if (current.GetComponent<PlantpieceModel>() != null)
                gFlipper.Add(current.AddComponent<GravityFlipper>());

        foreach (GravityFlipper current in gFlipper)
            current.GravityFlip(pData.rootBonusGravityScale, pData.rootBonusLinearDrag);
    }

    public void ResetGravitySlowly()
    {
        if (gravityFlipActive)
        {
            foreach (GravityFlipper current in gFlipper)
                current.ResetGravitySlowly(pData.rootBonusResetDuration);

            Action onEnd = () => { breakEnabled = true; gravityFlipActive = false; gFlipper.Clear(); };
            onEnd += onGravityFlipEnd;

            gravityFlipWorker = this.gameObject.AddComponent<Coworker>();
            gravityFlipWorker.StartDelayedAction(pData.rootBonusResetDuration, null, onEnd);
        }
    }

    public void ResetGravityImmediate()
    {
        if (gravityFlipActive)
        {
            foreach (GravityFlipper current in gFlipper)
                current.ResetGravityImmediate();

            breakEnabled = true;
            gravityFlipActive = false;
            gFlipper.Clear();

            onGravityFlipEnd();
        }
    }
    */
