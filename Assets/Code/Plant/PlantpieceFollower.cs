﻿using UnityEngine;
using System.Collections;

public class PlantpieceFollower : MonoBehaviour
{
    private bool reached;
    private float startTime;

    public Vector3 start;
    public Vector3 target;

    public bool useX;
    public bool useY;
    public bool useZ;
	
	// Update is called once per frame
	void Update () 
    {
        if (!reached)
        {
            float fraction = (Time.time - startTime) / PlantModel.MainPlant.pData.growthDuration;

            Vector3 oldPos = this.transform.position;
            Vector3 lerpedPos = Vector3.Lerp(start, target, fraction);
            float x = oldPos.x;
            float y = oldPos.y;
            float z = oldPos.z;

            if (useX)
                x = lerpedPos.x;

            if (useY)
                y = lerpedPos.y;

            if (useZ)
                z = lerpedPos.z;

            this.transform.position = new Vector3(x, y, z);

            if (fraction >= 1)
            {
                reached = true;
            }
        }
	}

    public void OnGrowth(bool userAction)
    {
        if (PlantModel.MainPlant.plantPiecesActive.Count == PlantModel.MainPlant.pData.pieceFixAtIndex)
        {
            start = this.transform.position;
            target = PlantModel.MainPlant.plantPiecesActive[0].transform.position;

            startTime = Time.time;

            reached = false;
        }
    }

    public void OnRootGrowth()
    {

    }
}
