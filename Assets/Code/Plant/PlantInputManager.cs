﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using EventSystem;

public class PlantInputManager : MonoBehaviour
{
    private PlantModel plantModel;

    private bool inputHandled;
    private bool pressing;

    private float pressTime;

    private CustomSlider slider;

    public bool debug;

    public void Init(PlantModel pm)
    {
        plantModel = pm;

        slider = FollowCanvas.Instance.growSlider;

        FollowCanvas.Instance.growButton.GetComponent<BasicButton>().OnButtonDown += OnButtonDown;
        FollowCanvas.Instance.growButton.GetComponent<BasicButton>().OnButtonUp += OnButtonUp;
    }

    void Update()
    {
        if (!GameManager.Instance.paused && !GameManager.Instance.gameOver)
        {
            if (Input.GetButtonDown("Up"))
            {
                OnButtonDown();
            }
            else if (Input.GetButtonUp("Up"))
            {
                OnButtonUp();
            }

            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                FollowCanvas.Instance.busButton.ButtonDown();
            }

            if (Input.GetButton("Left"))
            {
                slider.Value = -1;
            }

            if (Input.GetButton("Right"))
            {
                slider.Value = 1;
            }

            if (Input.GetButtonDown("Left") || Input.GetButtonDown("Right"))
            {
                EventManager.Instance.CallEvent(EventData.ID.INPUT_SLIDER_ENTER);
            }

            if (Input.GetButtonUp("Left") || Input.GetButtonUp("Right"))
            {
                slider.Value = 0;
                EventManager.Instance.CallEvent(EventData.ID.INPUT_SLIDER_EXIT);
            }

            if (pressing)
            {
                plantModel.GrowHeight(false, true);

                plantModel.plantHead.pivot.SetOffset(slider.Value);
            }
            else
            {
                plantModel.plantHead.pivot.Rotate(slider.Value * -1);
            }
        }
    }

    public void OnButtonDown()
    {
        inputHandled = false;
        pressing = true;
        pressTime = 0;

        EventManager.Instance.CallEvent(EventData.ID.INPUT_BUTTON_ENTER);

        if(debug)
            Time.timeScale = 0.1f;
    }

    public void OnButtonUp()
    {
        pressing = false;

        plantModel.plantHead.pivot.OnGrowthEnd();

        EventManager.Instance.CallEvent(EventData.ID.INPUT_BUTTON_EXIT);

        if (debug)
            Time.timeScale = 1;
    }
}
