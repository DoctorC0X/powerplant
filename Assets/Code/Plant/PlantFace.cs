﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using EventSystem;

public class PlantFace : MonoBehaviour
{
    private PlantModel plantModel;

    public RendererVisibility visibility;

    public SpriteRenderer sRenderer;
    public ShadowCaster shadowCaster;

    public Animator celebration;
    public Animator surprise;

    private Vector3 startPos;
    private Quaternion startRot;

    private Transform target;

    private float initZPos;
    private float startTime;
    
    private bool targetReached;

    public float happyTimer;
    public bool happy;

    public InterpolatorBase interpolator;

    public void Init(PlantModel pm)
    {
        targetReached = true;
        initZPos = this.transform.position.z;

        plantModel = pm;

        sRenderer = GetComponentInChildren<SpriteRenderer>();
        sRenderer.enabled = true;
        shadowCaster = GetComponentInChildren<ShadowCaster>();

        visibility = GetComponentInChildren<RendererVisibility>();
        visibility.OnVisibilityChange += (bool visible) =>
            {
                if (visible)
                    EventManager.Instance.CallEvent(EventData.ID.SCREEN_ENTER);
                else
                    EventManager.Instance.CallEvent(EventData.ID.SCREEN_EXIT);
            };

        ChangeToIdle();

        //joint.connectedAnchor = this.transform.position;

        plantModel.OnPlantBroke += () => ChangeToIdle();

        EventManager.Instance.AddHandler(EventData.ID.BULLET_DODGE, Dodge);

        interpolator = InterpolationManager.Instance.CreateInterpolator(InterpolationManager.Type.DURATION);

        interpolator.AddUpdateListener(OnInterpolationUpdate);
        interpolator.AddDoneListener(OnInterpolationDone);
    }

	// Update is called once per frame
	void Update () 
    {
        if (targetReached)
        {
            UpdatePos();
        }

        if (happy && !GameManager.Instance.paused)
        {
            happyTimer -= Time.deltaTime;

            if (happyTimer <= 0)
            {
                ChangeToIdle();
            }
        }
	}

    // initialize interpolation on growth
    public void OnGrowth()
    {
        targetReached = false;

        startPos = transform.position;
        startRot = transform.rotation;

        target = plantModel.plantHead.transform;

        interpolator.StartInterpolation(new DurationParameter(1, plantModel.pData.growthDuration));

        ChangeToHappy(1);
    }

    public void OnInterpolationUpdate(float fraction)
    {
        transform.position = Vector3.Lerp(startPos, target.position, fraction);
        transform.rotation = Quaternion.Lerp(startRot, target.rotation, fraction);
    }

    public void OnInterpolationDone()
    {
        targetReached = true;

        transform.position = new Vector3(transform.position.x, transform.position.y, initZPos);
    }

    public void UpdatePos()
    {
        transform.position = plantModel.plantHead.transform.position;
        transform.rotation = plantModel.plantHead.transform.rotation;
    }

    public void ChangeToHappy(float duration)
    {
        happyTimer = duration;

        if (!happy)
        {
            happy = true;

            SetFace(plantModel.pData.faceHappy);
        }
    }

    public void ChangeToIdle()
    {
        if (happy)
        {
            happy = false;

            SetFace(plantModel.pData.faceIdle);
        }
    }

    private void SetFace(Sprite s)
    {
        sRenderer.sprite = s;
        shadowCaster.CreateShadow();
    }

    public void Dodge()
    {
        surprise.SetTrigger("Trigger");
        ChangeToHappy(0.8f);

        if (!AudioSources.Instance.closedodge.isPlaying)
            AudioSources.Instance.closedodge.Play();
    }

    public void Celebrate()
    {
        ChangeToHappy(1);

        celebration.SetTrigger("Trigger");

        if(!AudioSources.Instance.missionComplete.isPlaying)
            AudioSources.Instance.missionComplete.Play();
    }
}

/*
public void InterpolateToMid()
{
    if (toMidInterpolator == null)
    {
        SetKinematic(true);

        toMidInterpolator = new Interpolator(Interpolator.InterpolationTiming.TIMED, plantModel.pData.toMidDuration, true);

        float targetY = plantModel.highestPlantPiece.GetComponent<PlantpieceModel>().desiredPos.y + plantModel.pieceHeight / 2;

        Vector3 startPos = this.transform.position;
        Vector3 targetPos = new Vector3(plantModel.transform.position.x, targetY, this.transform.position.z);

        targetY = (plantModel.highestPlantPiece.GetComponent<PlantpieceModel>().desiredPos.y + plantModel.pieceHeight / 2);
        //targetY -= this.transform.position.y;
        //targetY /= 2;
        //targetY += this.transform.position.y;

        Vector3 p1 = new Vector3(this.transform.localPosition.x * plantModel.pData.toMidDistanceMultiplier, this.transform.position.y, this.transform.position.z);
        Vector3 p2 = new Vector3(this.transform.localPosition.x * plantModel.pData.toMidDistanceMultiplier, targetY, this.transform.position.z);

        //Vector3 p1 = new Vector3(this.transform.position.x + 2, this.transform.position.y, this.transform.position.z);
        //Vector3 p2 = new Vector3(this.transform.position.x + 2, targetY, this.transform.position.z);

        toMidInterpolator.InitPositionInterpolation(startPos, p1, p2, targetPos);
        toMidInterpolator.InitRotationInterpolation(this.transform.rotation, Quaternion.Euler(0, 0, 0));

        //toMidInterpolator.SetDebugDraw(true);

        toMidInterpolator.SetAnimationCurve(plantModel.pData.toMidCurve);

        toMidInterpolator.OnInterpolate += (InterpolationData data) => { this.transform.position = data.currentPos; this.transform.rotation = data.currentRot; };
        //toMidInterpolator.OnInterpolate += SetPositionAndRotation;

        toMidInterpolator.OnReached += OnReachedMid;
        //toMidInterpolator.OnReached += () => this.transform.rotation = toMidInterpolator.data.currentRot;
        toMidInterpolator.OnReached += () => SetKinematic(false);
        toMidInterpolator.OnReached += () => toMidInterpolator = null;

        CubicSpline spline = new CubicSpline();

        int start = Mathf.Clamp(plantModel.plantPieces.Count - plantModel.pData.toMidPieceBorder, 0, plantModel.plantPieces.Count - 1);

        //Vector3 point1 = plantModel.plantPieces[start].transform.position;
        //Vector3 point2 = new Vector3(plantModel.transform.position.x, this.transform.position.y, this.transform.position.z);
        //Vector3 point3 = this.transform.position;

        for (int i = start; i < plantModel.plantPieces.Count; i++)
        {
            if (i % 2 == 0)
            {
                if (i + 1 < plantModel.plantPieces.Count)
                {
                    Vector3 point = plantModel.plantPieces[i].transform.position;
                    Vector3 tangent = plantModel.plantPieces[i + 1].transform.position - point;

                    spline.AddPointTangentPair(point, tangent);
                }
                else
                {
                    Vector3 point = plantModel.plantPieces[i].transform.position;
                    Vector3 tangent = plantModel.plantFace.transform.position - point;

                    spline.AddPointTangentPair(point, tangent);
                }
            }

            //plantModel.plantPieces[i].GetComponent<PlantpieceModel>().InterpolateToMid();
            plantModel.plantPieces[i].GetComponent<PlantpieceModel>().SetKinematic(true);
        }

        spline.AddPointTangentPair(plantModel.plantFace.transform.position, plantModel.plantFace.transform.up);

        for (int i = 0; i < spline.GetSegmentAmount(); i++)
        {
            for(int t = 0; t < 10; t++)
            {
                Debug.DrawLine(spline.GetPoint(i, ((float)t) / 10), spline.GetPoint(i, ((float)t+1)/10), Color.red + new Color(0, 0.1f * t, 0), 10);
            }
        }

        plantModel.highestPlantPiece.GetComponent<PlantpieceModel>().WatchUpAngleRecursive(plantModel.pData.toMidPieceBorder);
    }
}

public void SetPositionAndRotation(InterpolationData data)
{
    this.transform.position = data.currentPos;

    float side = Mathf.Sign(this.transform.position.x - plantModel.transform.position.x);

    this.transform.forward = new Vector3(0, 0, 1);
    this.transform.up = data.moveDirRight * side;
}
*/
