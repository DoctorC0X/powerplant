﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlantData : ScriptableObject 
{
    public GameObject piecePrefab;

    public int startPieces;

    public float borderX;

    [Tooltip("how long does it take for one piece to be grown (face & landingPort interpolation)")]
    public float growthDuration;

    public float faceMass;
    public float faceJointAngleLimit;

    [Tooltip("how much of the critters acc is added to the balance acc")]
    public float faceImpulseAccCritterFactor;

    public Sprite faceIdle;
    public Sprite faceHappy;

    public float starDustMagnetDistanceScale;
    public float starDustMagnetRadius;

    public Vector2 starDustCollectorSize;

    public float starDustCollectorTimerMax;
    public float starDustCollectorThrowDelay;

    public int[] starDustCollectorSubdivision;
    public Sprite[] starDustCollectorSubdivisionSprite;
    public Color[] starDustCollectorSubdivionColor;
    public float[] starDustCollectorSubdivisionSize;

    public Sprite starDustCollectorBonusSprite;
    public Color starDustCollectorBonusColor;
    public float starDustCollectorBonusSize;

    public float pieceMass;
    public float pieceJointAngleLimit;
    public int pieceFixAtIndex;

    public float pressTimeTillSkill;
    public SkillBase skill1;
    public SkillBase skill2;
    public PassiveBase passive;
}