﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Blomster_Skill_2 : Blomster_Skill_1 
{
    public float reductionDistance;
    public float speedReduction;

    public float rotateDistance;
    public float rotationBorder;
    public float rotationSpeed;

    public float startEndChangeSpeed;

    public override void Init(PlantModel pm)
    {
        base.Init(pm);
    }

    public override void PerformSkill()
    {
        base.PerformSkill();
    }

    public override void OnUpdate()
    {
        base.OnUpdate();

        foreach (GameObject current in BulletSpawner.Instance.activeBullets)
        {
            Bullet b = current.GetComponent<Bullet>();

            /*
            if ((plantModel.plantHead.Center - current.transform.position).sqrMagnitude < (reductionDistance * reductionDistance))
            {
                b.movement.SetMoveMode(InterpolatedMovement.MoveMode.SPEED, b.movement.movementCoefficient - speedReduction * Time.deltaTime);
            }

            if((plantModel.plantHead.Center - current.transform.position).sqrMagnitude < (rotateDistance * rotateDistance))
            {
                if (b.movement.movementCoefficient <= rotationBorder)
                {
                    float angleDir = 1;

                    if (b.type == Bullet.Type.PLANE)
                    {
                        float heightDiff = plantModel.plantHead.Center.y - b.transform.position.y;

                        angleDir = Mathf.Sign(heightDiff);

                        //b.SetFlightValues(b.transform.position, b.end);
                        b.TranslateStartPoint(Mathf.Sign(heightDiff) * startEndChangeSpeed * Time.deltaTime * -1);
                        b.TranslateEndPoint(  Mathf.Sign(heightDiff) * startEndChangeSpeed * Time.deltaTime);
                    }

                    b.transform.Rotate(0, 0, rotationSpeed * angleDir * Time.deltaTime);
                }
            }
            */
        }
    }
}
