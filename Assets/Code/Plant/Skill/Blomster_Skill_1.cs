﻿using UnityEngine;
using System.Collections;

public class Blomster_Skill_1 : SkillBase 
{
    public float leafRotDuration;
    public float leafRotAngle;

    public int moveAmount;
    private int moveCounter;

    public AnimationCurve animCurve;

    private float startRot;
    private float topRot;
    private float botRot;

    private float distanceMultiplier;
    private float currentStart;
    private float currentEnd;
    private float timePassed;

    public override void Init(PlantModel pm)
    {
        base.Init(pm);
    }

    public override void PerformSkill()
    {
        Activated = true;

        startRot = plantModel.landingPort.leafRight.transform.localEulerAngles.z;
        topRot = startRot + leafRotAngle / 2;
        botRot = startRot - leafRotAngle / 2;

        distanceMultiplier = 0.5f;
        currentStart = startRot;
        currentEnd = topRot;

        timePassed = 0;
        moveCounter = -1;
    }

    public override void OnUpdate()
    {
        base.OnUpdate();

        timePassed += Time.deltaTime;

        float fraction = timePassed / (leafRotDuration * distanceMultiplier);

        fraction = animCurve.Evaluate(fraction);

        plantModel.landingPort.leafRight.transform.localEulerAngles = new Vector3(0, 0, Mathf.Lerp(currentStart, currentEnd, fraction));
        plantModel.landingPort.leafLeft.transform.localEulerAngles = new Vector3(0, 0, Mathf.Lerp(currentStart, currentEnd, fraction) * -1);

        if (fraction >= 1)
        {
            moveCounter++;

            timePassed = 0;

            if (moveCounter == 0)
            {
                distanceMultiplier = 1;

                currentStart = currentEnd;
                currentEnd = botRot;
            }
            else if (moveCounter < moveAmount)
            {
                distanceMultiplier = 1;

                float tmp = currentStart;
                currentStart = currentEnd;
                currentEnd = tmp;
            }
            else if (moveCounter == moveAmount)
            {
                distanceMultiplier = 0.5f;

                currentStart = currentEnd;
                currentEnd = startRot;
            }
            else
            {
                Activated = false;
            }
        }
    }
}
