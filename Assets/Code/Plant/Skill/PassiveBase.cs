﻿using UnityEngine;
using System.Collections;

public abstract class PassiveBase : ScriptableObject 
{
    protected PlantModel plantModel;

    public virtual void Init(PlantModel pm)
    {
        plantModel = pm;
    }
}
