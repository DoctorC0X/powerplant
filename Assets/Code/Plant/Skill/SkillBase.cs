﻿using UnityEngine;
using System.Collections;

public abstract class SkillBase : ScriptableObject
{
    protected PlantModel plantModel;

    public float cooldownTime;

    protected bool _activated;

    public bool Activated
    {
        get { return _activated; }
        set
        {
            _activated = value;
            plantModel.skillManager.cooldownActive = !value;
        }
    }

    public virtual void Init(PlantModel pm)
    {
        plantModel = pm;
    }

    public virtual void OnUpdate()
    {

    }

    public abstract void PerformSkill();
}
