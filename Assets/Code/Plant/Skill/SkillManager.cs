﻿using UnityEngine;
using System.Collections;

public class SkillManager : MonoBehaviour 
{
    public PlantModel plantModel;

    public SkillBase currentSkill;
    public PassiveBase currentPassive;

    public float cooldownTimer;
    public bool cooldownActive;

    public void Init(PlantModel pm)
    {
        plantModel = pm;

        CheckUnlock();
        //InitSkill(plantModel.pData.skill1);

        currentPassive = ScriptableObject.Instantiate<PassiveBase>(plantModel.pData.passive);
        currentPassive.Init(plantModel);

        FollowCanvas.Instance.skillButton.GetComponent<BasicButton>().OnButtonDown += PerformSkill;
    }

	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            PerformSkill();
        }

        if (!GameManager.Instance.paused && !plantModel.Broken && currentSkill != null)
        {
            if (cooldownActive)
            {
                cooldownTimer -= Time.deltaTime;

                if (cooldownTimer <= 0)
                {
                    cooldownActive = false;
                }
            }

            if (currentSkill.Activated)
            {
                currentSkill.OnUpdate();
            }
        }
	}

    public bool IsSkillPerformable()
    {
        return (currentSkill != null && !currentSkill.Activated && !cooldownActive && !plantModel.Broken && !GameManager.Instance.paused && GameManager.Instance.gameActive);
    }

    public void PerformSkill()
    {
        if (IsSkillPerformable())
        {
            currentSkill.PerformSkill();

            cooldownTimer = currentSkill.cooldownTime;
        }
    }

    public void InitSkill(SkillBase skill)
    {
        currentSkill = ScriptableObject.Instantiate<SkillBase>(skill);
        currentSkill.Init(plantModel);
    }

    public void CheckUnlock()
    {
        if (!PlayerProfile.Instance.IsMasterSkillUnlocked())
        {
            InitSkill(plantModel.pData.skill1);
        }
        else
        {
            InitSkill(plantModel.pData.skill2);
        }
    }
}
