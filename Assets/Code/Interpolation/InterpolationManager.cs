﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class InterpolationManager : MonoBehaviour 
{
    private static InterpolationManager _instance;

    public static InterpolationManager Instance
    {
        get { return _instance; }
    }

    public long nextId;

    public int activeInterpolators;

    public Dictionary<long, InterpolatorBase> interpolators;

    public enum Type { SPEED, DURATION };

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
            return;
        }
        else
        {
            _instance = this;
        }

        interpolators = new Dictionary<long, InterpolatorBase>();
    }

	// Update is called once per frame
	void Update () 
    {
        if (interpolators.Count > 0)
        {
            foreach (KeyValuePair<long, InterpolatorBase> current in interpolators)
            {
                current.Value.Update();
            }
        }
	}

    public InterpolatorBase CreateInterpolator(Type t)
    {
        InterpolatorBase ib;

        switch (t)
        {
            case Type.DURATION:
                {
                    ib = new InterpolatorDuration(nextId);

                    break;
                }
            case Type.SPEED:
                {
                    ib = new InterpolatorSpeed(nextId);

                    break;
                }
            default:
                {
                    throw new ArgumentException("Invalid Type " + t + " in InterpolationManager");
                }
        }

        interpolators.Add(nextId, ib);

        activeInterpolators++;
        nextId++;

        return ib;
    }

    public InterpolatorBase GetInterpolator(long id)
    {
        return interpolators[id];
    }

    public void DestroyInterpolatorAfter(long id)
    {
        StartCoroutine(DestroyAfter(id));
    }

    public void DestroyInterpolatorImmediate(long id)
    {
        if (interpolators.ContainsKey(id))
        {
            activeInterpolators--;

            interpolators[id].StopInterpolation();
            interpolators.Remove(id);
        }
    }

    public IEnumerator DestroyAfter(long id)
    {
        yield return new WaitForEndOfFrame();

        DestroyInterpolatorImmediate(id);
    }

    void OnDestroy()
    {
        StopAllCoroutines();
    }
}
