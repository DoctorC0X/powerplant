﻿using UnityEngine;
using System.Collections;
using System;

public abstract class InterpolatorBase 
{
    public long id;

    public int dir;

    public float fraction;

    public AnimationCurve animCurve;

    public bool paused;

    public bool interpolating;

    public InterpolatorBase(long id)
    {
        this.id = id;
    }

    public virtual void StartInterpolation(InterpolationParameter param)
    {
        interpolating = true;

        this.dir = param.dir >= 0 ? 1 : -1;
        this.animCurve = param.animCurve;
    }

    public virtual void StopInterpolation()
    {
        interpolating = false;
    }

    public void ToggleDir()
    {
        dir *= -1;
    }

    public abstract void Update();

    public void SetPaused(bool paused)
    {
        this.paused = paused;
    }

    public abstract void AddUpdateListener(Action<float> OnUpdate);
    public abstract void RemoveUpdateListener(Action<float> OnUpdate);

    public abstract void AddDoneListener(Action OnDone);
    public abstract void RemoveDoneListener(Action OnDone);
}

public abstract class InterpolationParameter
{
    public int dir;
    public AnimationCurve animCurve;

    public InterpolationParameter(int dir, AnimationCurve curve = null)
    {
        this.dir = dir;
        animCurve = curve;
    }
}
