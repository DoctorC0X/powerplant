﻿using UnityEngine;
using System.Collections;
using System;

public class InterpolatorSpeed : InterpolatorBase 
{
    public float speed;
    public float distance;

    public float traveled;

    public event Action<float> OnUpdate;
    public event Action OnDone;

    public InterpolatorSpeed(long id)
        : base(id)
    {

    }

    public override void StartInterpolation(InterpolationParameter param)
    {
        base.StartInterpolation(param);

        StartInterpolation((SpeedParameter)param);
    }

    private void StartInterpolation(SpeedParameter param)
    {
        interpolating = true;

        this.dir = param.dir >= 0 ? 1 : -1;
        this.speed = param.speed;
        this.distance = param.distance;

        if (dir == 1)
        {
            traveled = 0;
        }
        else
        {
            traveled = distance;
        }
    }

    public override void Update()
    {
        if (!paused && interpolating)
        {
            traveled += (Time.deltaTime * speed * dir);

            fraction = traveled / distance;

            if (OnUpdate != null)
                OnUpdate(Mathf.Clamp(fraction, 0, 1));

            if (fraction < 0 || fraction > 1)
            {
                interpolating = false;

                if (OnDone != null)
                    OnDone();
            }
        }
    }

    public override void AddUpdateListener(System.Action<float> OnUpdate)
    {
        this.OnUpdate += OnUpdate;
    }

    public override void RemoveUpdateListener(System.Action<float> OnUpdate)
    {
        this.OnUpdate -= OnUpdate;
    }

    public override void AddDoneListener(System.Action OnDone)
    {
        this.OnDone += OnDone;
    }

    public override void RemoveDoneListener(System.Action OnDone)
    {
        this.OnDone -= OnDone;
    }
}

public class SpeedParameter : InterpolationParameter
{
    public float speed;
    public float distance;

    public SpeedParameter(int dir, float speed, float distance, AnimationCurve curve = null) : base(dir, curve)
    {
        this.speed = speed;
        this.distance = distance;
    }
}
