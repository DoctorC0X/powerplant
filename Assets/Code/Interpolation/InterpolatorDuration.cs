﻿using UnityEngine;
using System.Collections;
using System;

public class InterpolatorDuration : InterpolatorBase
{
    public float timePassed;

    public float duration;

    public event Action<float> OnUpdate;
    public event Action OnDone;

    public InterpolatorDuration(long id)
        : base(id)
    {

    }

    public override void StartInterpolation(InterpolationParameter param)
    {
        base.StartInterpolation(param);

        StartInterpolation((DurationParameter)param);
    }

    private void StartInterpolation(DurationParameter param)
    {
        duration = param.duration;

        if (dir == 1)
        {
            timePassed = 0;
        }
        else
        {
            timePassed = duration;
        }
    }

    public override void Update()
    {
        if (!paused && interpolating)
        {
            timePassed += (Time.deltaTime * dir);

            fraction = timePassed / duration;

            if(OnUpdate != null)
                OnUpdate(Mathf.Clamp(fraction, 0, 1));

            if (fraction < 0 || fraction > 1)
            {
                interpolating = false;

                if (OnDone != null)
                    OnDone();
            }
        }
    }

    public override void AddUpdateListener(Action<float> OnUpdate)
    {
        this.OnUpdate += OnUpdate;
    }

    public override void RemoveUpdateListener(Action<float> OnUpdate)
    {
        this.OnUpdate -= OnUpdate;
    }

    public override void AddDoneListener(Action OnDone)
    {
        this.OnDone += OnDone;
    }

    public override void RemoveDoneListener(Action OnDone)
    {
        this.OnDone -= OnDone;
    }
}

public class DurationParameter : InterpolationParameter
{
    public float duration;

    public DurationParameter(int dir, float duration, AnimationCurve curve = null) : base(dir, curve)
    {
        this.duration = duration;
    }
}
