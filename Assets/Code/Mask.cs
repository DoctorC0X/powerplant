﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Mask
{
    public int flags;

    public Mask()
    {
        flags = 0;
    }

    public Mask(int flags)
    {
        this.flags = flags;
    }

    public void SetFlag(int mask)
    {
        flags = flags | mask;
    }

    public void RemoveFlag(int mask)
    {
        flags = flags & ~mask;
    }

    public void SetFlag(Mask mask)
    {
        SetFlag(mask.flags);
    }

    public void RemoveFlag(Mask mask)
    {
        RemoveFlag(mask.flags);
    }

    public static bool Check(Mask a, Mask b)
    {
        return (a.flags & b.flags) == a.flags;
    }

    public static bool Check(Mask a, int b)
    {
        return (a.flags & b) == a.flags;
    }

    public static bool Check(Mask a, Mask b, Mask aInvert)
    {
        return (a.flags & (b.flags ^ aInvert.flags)) == a.flags;
    }
}
