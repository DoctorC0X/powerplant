﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(BulletWarning))]
public class BulletWarnungEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Toggle dir"))
        {
            BulletWarning warning = (BulletWarning)target;

            if (warning.dir == BulletSpawner.FlightDir.HORIZONTAL)
            {
                warning.SetDir(BulletSpawner.FlightDir.VERTICAL, warning.Side);
            }
            else
            {
                warning.SetDir(BulletSpawner.FlightDir.HORIZONTAL, warning.Side);
            }
        }

        if (GUILayout.Button("Toggle side"))
        {
            BulletWarning warning = (BulletWarning)target;

            warning.SetDir(warning.dir, warning.Side * -1);
        }
    }
}
