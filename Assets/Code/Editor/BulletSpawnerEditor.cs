﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(BulletSpawner))]
public class BulletSpawnerEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Vertical Upward"))
        {
            ((BulletSpawner)target).Spawn(BulletSpawner.FlightDir.VERTICAL, 1);
        }

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Horizontal Left"))
        {
            ((BulletSpawner)target).Spawn(BulletSpawner.FlightDir.HORIZONTAL, -1);
        }

        if (GUILayout.Button("Horizontal Right"))
        {
            ((BulletSpawner)target).Spawn(BulletSpawner.FlightDir.HORIZONTAL, 1);
        }

        GUILayout.EndHorizontal();

        if (GUILayout.Button("Vertical Below"))
        {
            ((BulletSpawner)target).Spawn(BulletSpawner.FlightDir.VERTICAL, -1);
        }
    }
}
