﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.IO;
using System.Xml;
using System.Text;

public class XmlAssetCreator
{
    public static string xmlFolderPath = "XML";

    [MenuItem("Assets/Create/XML/New Goal")]
    public static void CreateEmptyGoal()
    {
        CreateXml("Goal");
    }

    [MenuItem("Assets/Create/XML/New MissionQueue")]
    public static void CreateEmptyMission()
    {
        CreateXml("MissionQueue");
    }

    private static void CreateXml(string xmlName)
    {
        string newFilePath = GenerateNewFilePath(xmlName);

        StreamReader sReader = new StreamReader(string.Format("{0}/{1}/{2}Template.xml", Application.dataPath, xmlFolderPath, xmlName));
        StreamWriter sWriter = new StreamWriter(Application.dataPath.Replace("Assets", string.Empty) + newFilePath);

        string relativeXsdPath = GenerateRelativeXsdPath(newFilePath, xmlName);

        string replaceString = xmlName + ".xsd";

        string line;

        while ((line = sReader.ReadLine()) != null)
        {
            if (line.Contains(replaceString))
                line = line.Replace(replaceString, relativeXsdPath);

            sWriter.WriteLine(line);
        }

        sReader.Close();
        sWriter.Close();

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        //EditorUtility.FocusProjectWindow();
    }

    private static string GenerateNewFilePath(string newFileName)
    {
        // path to selected object
        StringBuilder stringB = new StringBuilder(AssetDatabase.GetAssetPath(Selection.activeObject));

        // if selected is a file, cut out the file extension
        if (Path.GetExtension(stringB.ToString()) != "")
            stringB = stringB.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");

        // add file name & extension of new file
        stringB.Append(string.Format("/New {0}.xml", newFileName));

        // ensure that file name does not exist already
        return AssetDatabase.GenerateUniqueAssetPath(stringB.ToString());
    }

    private static string GenerateRelativeXsdPath(string xmlPath, string newFileName)
    {
        StringBuilder stringB = new StringBuilder();

        for (int i = 0; i < xmlPath.Split('/').Length - 2; i++)
        {
            stringB.Append("../");
        }

        stringB.Append(xmlFolderPath);
        stringB.Append(string.Format("/{0}.xsd", newFileName));

        return stringB.ToString();
    }
}
