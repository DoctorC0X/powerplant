﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CamShake))]
public class CamShakeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CamShake shaker = ((CamShake)target);

        if (GUILayout.Button("Shake!") && Application.isPlaying && !shaker.shaking)
        {
            shaker.StartShake();
        }
    }
}
