﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(StarDustShapeDataCreator))]
[CanEditMultipleObjects]
public class StarDustShapeDataCreatorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Fill Point Array & Save Asset"))
        {
            FillArrayAndSave(false);
        }

        if (GUILayout.Button("Fill Point Array & Save Asset + Reverse"))
        {
            FillArrayAndSave(true);
        }
    }

    private void FillArrayAndSave(bool revert)
    {
        for (int i = 0; i < targets.Length; i++)
        {
            StarDustShapeDataCreator current = ((StarDustShapeDataCreator)targets[i]);

            StarDustShapeData data = new StarDustShapeData();

            current.FillPointArray(data, revert);

            Mesh dot = current.CreateDotMesh(data.points);
            Mesh trail = current.CreateTrailMesh(data.points);

            DataAssetCreator.SaveAsset<Mesh>(dot, "Assets/Resources/Props/StarDust/Meshes/" + current.gameObject.name + "_dot" + ".obj");
            DataAssetCreator.SaveAsset<Mesh>(trail, "Assets/Resources/Props/StarDust/Meshes/" + current.gameObject.name + "_trail" + ".obj");

            data.dotMesh = dot;
            data.trailMesh = trail;

            DataAssetCreator.SaveAsset<ScriptableObject>(data, "Assets/Resources/Props/StarDust/ProcessedShapes/" + current.gameObject.name + ".asset");
        }
    }
}
