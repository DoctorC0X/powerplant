﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.IO;

public class DataAssetCreator 
{
    public static void CreateCustomAsset<T>() where T : ScriptableObject
    {
        T cfg = ScriptableObject.CreateInstance<T>();

        String path = AssetDatabase.GetAssetPath(Selection.activeObject);

        if (Path.GetExtension(path) != "")
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");

        path = AssetDatabase.GenerateUniqueAssetPath(path + "/New ScriptableObject.asset");

        SaveAsset<T>(cfg, path);
    }

    public static void SaveAsset<T>(T obj, string path) where T : UnityEngine.Object
    {
        if (path == "")
        {
            path = "Assets";
        }

        AssetDatabase.CreateAsset(obj, path);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        //EditorUtility.FocusProjectWindow();
        Selection.activeObject = obj;
    }

    [MenuItem("Assets/Create/Empty ScriptableObject")]
    public static void CreateEmpty()
    {
        CreateCustomAsset<ScriptableObject>();
    }
}