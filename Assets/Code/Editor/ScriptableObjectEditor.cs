﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(ScriptableObject), true)]
public class ScriptableObjectEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUI.changed)
            EditorUtility.SetDirty(target);
    }
}
