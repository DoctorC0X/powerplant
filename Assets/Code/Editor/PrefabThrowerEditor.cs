﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(Thrower))]
public class PrefabThrowerEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Thrower thrower = (Thrower)target;

        if (Application.isPlaying && GUILayout.Button("Throw World"))
        {
            thrower.Throw(new ThrowableText.Params("TEST", 26, Color.grey, Color.black), 1);
        }

        if (Application.isPlaying && GUILayout.Button("Throw Local"))
        {
            thrower.Throw(new ThrowableText.Params("TEST", 26, Color.grey, Color.black), 1, false);
        }

        List<Vector2> points = new List<Vector2>();

        if (thrower.cStart != null && thrower.cHandle != null && thrower.cEnd != null)
        {
            for (int i = 0; i <= 10; i++)
            {
                float t = ((float)i) / 10;

                points.Add(thrower.GetPointOnCurve(t));
            }

            for (int i = 1; i < points.Count; i++)
            {
                Debug.DrawLine(points[i - 1], points[i], Color.yellow);
            }
        }
    }
}
