﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class OpenAdditive : MonoBehaviour 
{

    [MenuItem("File/Open Scene Additive", false, 4)]
    static void OpenSceneAdditive()
    {
        string path = EditorUtility.OpenFilePanel("Select Scene", "", "unity");
        EditorApplication.OpenSceneAdditive(path);
    }

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
