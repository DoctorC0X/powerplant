﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Accelerometer))]
public class AccelerometerEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (Application.isPlaying && GUILayout.Button("Shake shake"))
        {
            ((Accelerometer)target).Trigger();
        }
    }
}
