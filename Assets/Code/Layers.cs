﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Layers 
{
    public static KeyValuePair<string, int> starPiece   = new KeyValuePair<string, int>("StarPiece", 10);
    public static KeyValuePair<string, int> critter     = new KeyValuePair<string, int>("Critter", 11);
    public static KeyValuePair<string, int> hazard      = new KeyValuePair<string, int>("Hazard", 12);
    public static KeyValuePair<string, int> plantHead   = new KeyValuePair<string, int>("PlantHead", 13);
    public static KeyValuePair<string, int> plantPiece  = new KeyValuePair<string, int>("PlantPiece", 14);
    public static KeyValuePair<string, int> plantLeaf   = new KeyValuePair<string, int>("PlantLeaf", 15);
    public static KeyValuePair<string, int> stage       = new KeyValuePair<string, int>("Stage", 16);
}
