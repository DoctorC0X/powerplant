﻿using UnityEngine;
using System.Collections;
using EventSystem;

public class DestroyOnGamestart : MonoBehaviour
{
    public float delay;

    void Start()
    {
        if (delay > 0)
            EventManager.Instance.AddHandler(EventData.ID.GAMESTART, () => gameObject.AddComponent<Coworker>().StartDelayedAction(delay, null, PerformDestruction));
        else
            EventManager.Instance.AddHandler(EventData.ID.GAMESTART, () => PerformDestruction());
    }

    private void PerformDestruction()
    {
        if(gameObject != null)
            Destroy(gameObject);
    }
}
