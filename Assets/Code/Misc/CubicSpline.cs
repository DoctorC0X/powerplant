﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CubicSpline 
{
    public List<Vector3> points;
    public List<Vector3> tangents;

    public CubicSpline()
    {
        points = new List<Vector3>();
        tangents = new List<Vector3>();
    }

    public void AddPointTangentPair(Vector3 point, Vector3 tangent)
    {
        points.Add(point);
        tangents.Add(tangent);
    }

    public Vector3 GetPoint(int segment, float t)
    {
        t = Mathf.Clamp(t, 0, 1);

        if (segment >= 0 && segment < points.Count - 1)
        {
            Vector3 first  = points[segment]       * (1 + 2 * t) * Sqr(1 - t);
            Vector3 second = tangents[segment]     * t * Sqr(1 - t);
            Vector3 third  = points[segment + 1]   * Sqr(t) * (3 - 2 * t);
            Vector3 fourth = tangents[segment + 1] * Sqr(t) * (t - 1);

            return first + second + third + fourth;
        }
        else
        {
            throw new ArgumentException("Invalid segment index!");
        }
    }

    public int GetSegmentAmount()
    {
        return points.Count - 1;
    }

    public static float Sqr(float x)
    {
        return x * x;
    }

    public static int Sqr(int x)
    {
        return x * x;
    }
}
