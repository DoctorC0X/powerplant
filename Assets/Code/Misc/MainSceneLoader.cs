﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Uninspired.Core.Animation;

public class MainSceneLoader : MonoBehaviour 
{
    public static bool preMainScenesLoaded = false;

    public int[] preMainSceneIndices;

    public int mainSceneIndex;

    public SimpleSpriteAnimator loadingIcon;
    public float minLoadTime;

    void Start()
    {
        Application.backgroundLoadingPriority = ThreadPriority.Low;

        if(loadingIcon != null)
            loadingIcon.BeginAsCoroutine();

        StartCoroutine(PerformLoading());
    }

    private IEnumerator PerformLoading()
    {
        float estimateEndTime = Time.time + minLoadTime;

        if (!preMainScenesLoaded)
        {
            for (int i = 0; i < preMainSceneIndices.Length; i++)
            {
                Debug.Log("load next scene " + preMainSceneIndices[i]);

                yield return SceneManager.LoadSceneAsync(preMainSceneIndices[i], LoadSceneMode.Additive);
            }

            preMainScenesLoaded = true;
        }

        if (Time.time < estimateEndTime)
            yield return new WaitForSeconds(estimateEndTime - Time.time);

        yield return SceneManager.LoadSceneAsync(mainSceneIndex);
    }
}
