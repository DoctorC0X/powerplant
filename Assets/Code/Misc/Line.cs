﻿using UnityEngine;
using System.Collections;

public class Line : MonoBehaviour 
{
    private LineRenderer lineRenderer;
    private int pointCount;

    private GameObject oldLinesParent;

    public float lineZ;

	public void Init() 
    {
        lineRenderer = GetComponentInChildren<LineRenderer>();
        pointCount = 1;

        oldLinesParent = new GameObject("old lines");
        oldLinesParent.transform.parent = transform;
        oldLinesParent.transform.localPosition = Vector3.zero;

        lineRenderer.sharedMaterial = Instantiate(lineRenderer.sharedMaterial);
        lineRenderer.SetPosition(0, new Vector2(transform.position.x, transform.position.y));
	}
	
    public void AddPoint(Vector2 p)
    {
        lineRenderer.SetVertexCount(pointCount + 1);
        lineRenderer.SetPosition(pointCount, new Vector3(p.x, p.y, lineZ));
        pointCount++;

        lineRenderer.sharedMaterial.mainTextureScale = new Vector2(pointCount, 1);
    }

    public void StartNewLine()
    {
        lineRenderer.transform.parent = oldLinesParent.transform;

        GameObject newLineRenderer = Instantiate(lineRenderer.gameObject) as GameObject;

        newLineRenderer.name = lineRenderer.gameObject.name;

        newLineRenderer.transform.parent = transform;
        newLineRenderer.transform.localPosition = Vector3.zero;

        pointCount = 0;

        lineRenderer = newLineRenderer.GetComponent<LineRenderer>();
        lineRenderer.SetVertexCount(0);
        lineRenderer.sharedMaterial = Instantiate(lineRenderer.sharedMaterial);
    }

    public void DestroyOldLines()
    {
        Destroy(oldLinesParent);

        oldLinesParent = new GameObject("old lines");
        oldLinesParent.transform.parent = transform;
        oldLinesParent.transform.localPosition = Vector3.zero;
    }
}
