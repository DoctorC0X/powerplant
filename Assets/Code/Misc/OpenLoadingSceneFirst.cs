﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class OpenLoadingSceneFirst : MonoBehaviour 
{
    void Awake()
    {
        if (!MainSceneLoader.preMainScenesLoaded)
        {
            SceneManager.LoadScene(Scenes.loading.Key);
        }
    }
}
