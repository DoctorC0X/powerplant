﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class LogoFade : MonoBehaviour 
{
    public Image fader;

    private float passedTime;

    public float waitDuration;
    public float fadeDuration;

    private float currentDuration;

    private float startAlpha;
    private float endAlpha;

    public bool fading;

    private int state;

    public bool startActive;

	// Use this for initialization
	void Start () 
    {
        if (startActive)
        {
            StartFade();
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && state != 4)
        {
            SwitchState(4);
        }

        if (fading)
        {
            passedTime += Time.deltaTime;

            float fraction = passedTime / currentDuration;

            Color oldColor = fader.color;
            fader.color = new Color(oldColor.r, oldColor.g, oldColor.b, Mathf.Lerp(startAlpha, endAlpha, fraction));

            if (fraction >= 1)
            {
                SwitchState(state + 1);
            }
        }
	}

    public void StartFade()
    {
        fading = true;

        SwitchState(0);
    }

    private void SwitchState(int nr)
    {
        state = nr;

        passedTime = 0;

        switch (nr)
        {
            case 0:
                {
                    startAlpha = fader.color.a;
                    endAlpha = 0;

                    currentDuration = fadeDuration;

                    break;
                }
            case 1:
                {
                    startAlpha = fader.color.a;
                    endAlpha = fader.color.a;

                    currentDuration = waitDuration;

                    break;
                }
            case 2:
                {
                    startAlpha = fader.color.a;
                    endAlpha = 1;

                    currentDuration = fadeDuration;

                    break;
                }
            case 3:
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

                    break;
                }
            case 4:
                {
                    startAlpha = fader.color.a;
                    endAlpha = 1;

                    currentDuration = fadeDuration / 5;

                    break;
                }
            case 5:
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

                    break;
                }
        }
    }
}
