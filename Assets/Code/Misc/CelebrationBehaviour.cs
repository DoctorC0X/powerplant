﻿using UnityEngine;
using System.Collections;

public class CelebrationBehaviour : StateMachineBehaviour 
{
    public Transform oldParent;
    public Vector3 oldLocalPos;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    {
        oldParent = animator.transform.parent;
        oldLocalPos = animator.transform.localPosition;

        animator.transform.parent = null;
        animator.transform.rotation = Quaternion.identity;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    //{
    //
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    {
        animator.transform.parent = oldParent;
        animator.transform.localPosition = oldLocalPos;
        animator.transform.localRotation = Quaternion.identity;
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
