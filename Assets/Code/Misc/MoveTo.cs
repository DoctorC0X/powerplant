﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MoveTo : MonoBehaviour 
{
    private Vector3 _initialPos;
    private Vector3 _initialEnd;

    public Vector3 InitialPos
    {
        get { return _initialPos; }
    }

    private Vector3 start;
    public Vector3 end;

    public Transform target;

    public bool initOnAwake;

    public float duration;

    public AnimationCurve animCurve;

    private InterpolatorBase interpolator;

    public enum PosType { WORLD, LOCAL, ANCHOR };
    public PosType type;

    public event Action OnReachedEnd;

    public virtual void Awake()
    {
        if (initOnAwake)
        {
            if (target == null)
                Init(type, end);
            else
                Init(type, target);
        }

        _initialPos = GetCurrentPos();
    }

    public virtual void Start()
    {
        InitInterpolator();
    }

    private void InitInterpolator()
    {
        interpolator = InterpolationManager.Instance.CreateInterpolator(InterpolationManager.Type.DURATION);
    }

    private void Init(PosType type)
    {
        this.type = type;

        OnReachedEnd = null;
    }

    public void ReInit()
    {
        Init(type, _initialEnd);
    }

    public void Init(PosType type, Vector3 endPos)
    {
        Init(type);

        _initialEnd = endPos;

        SetStartEnd(endPos);
    }

    public void Init(PosType type, Transform target)
    {
        Init(type);

        _initialEnd = GetPos(target);

        SetStartEnd(target);
    }

    public void Begin()
    {
        if (interpolator == null)
            InitInterpolator();

        interpolator.StartInterpolation(new DurationParameter(1, duration, animCurve));

        interpolator.AddUpdateListener(UpdatePos);
        interpolator.AddDoneListener(CallListener);
    }

    public void Back()
    {
        SetStartEnd(_initialPos);

        OnReachedEnd += ReInit;

        Begin();
    }

    private void UpdatePos(float fraction)
    {
        float oldFraction = fraction;

        fraction = animCurve.Evaluate(fraction);

        switch (type)
        {
            case PosType.WORLD:
                {
                    transform.position = Vector3.Lerp(start, end, fraction);
                    break;
                }
            case PosType.LOCAL:
                {
                    transform.localPosition = Vector3.Lerp(start, end, fraction);
                    break;
                }
            case PosType.ANCHOR:
                {
                    GetComponent<RectTransform>().anchoredPosition3D = Vector3.Lerp(start, end, fraction);
                    break;
                }
        }
    }

    private void CallListener()
    {
        if (OnReachedEnd != null)
            OnReachedEnd();

        OnReachedEnd = null;
    }

    public void Skip()
    {
        if (interpolator != null && interpolator.interpolating)
        {
            interpolator.StopInterpolation();

            UpdatePos(1);

            CallListener();
        }
    }

    private Vector3 GetPos(Transform t)
    {
        switch (type)
        {
            case PosType.WORLD:
                {
                    return t.position;
                }
            case PosType.LOCAL:
                {
                    return t.localPosition;
                }
            case PosType.ANCHOR:
                {
                    return t.GetComponent<RectTransform>().anchoredPosition3D;
                }
            default:
                {
                    throw new ArgumentException("Unknown type " + type + " in MoveTo Script");
                }
        }
    }

    private Vector3 GetCurrentPos()
    {
        return GetPos(transform);
    }

    private void SetStartEnd(Vector3 end)
    {
        start = GetCurrentPos();

        this.end = end;
    }

    private void SetStartEnd(Transform target)
    {
        SetStartEnd(GetPos(target));
    }

    public void Pause(bool paused)
    {
        if(interpolator != null)
            interpolator.SetPaused(paused);
    }

    public void OnDestroy()
    {
        if (interpolator != null && InterpolationManager.Instance != null)
        {
            InterpolationManager.Instance.DestroyInterpolatorAfter(interpolator.id);
            interpolator = null;
        }
    }
}
