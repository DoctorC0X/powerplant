﻿using UnityEngine;
using System.Collections;
using System;

public class RendererVisibility : MonoBehaviour 
{
    public bool currentlyVisible;

    public event Action<bool> OnVisibilityChange;

    public bool activated = true;

    // used for face indicator
    void OnBecameInvisible()
    {
        if (GameManager.Instance.gameActive && activated && currentlyVisible)
        {
            currentlyVisible = false;

            if (OnVisibilityChange != null)
                OnVisibilityChange(false);
        }
    }

    void OnBecameVisible()
    {
        if (GameManager.Instance.gameActive && activated && !currentlyVisible)
        {
            currentlyVisible = true;

            if (OnVisibilityChange != null)
                OnVisibilityChange(true);
        }
    }
    //

    void OnApplicationQuit()
    {
        activated = false;
    }
}
