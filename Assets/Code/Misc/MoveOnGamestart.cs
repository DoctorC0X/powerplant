﻿using UnityEngine;
using System.Collections;
using EventSystem;

public class MoveOnGamestart : MoveTo 
{
    public bool destroyGoOnReach;
    public bool destroyThisOnReach;

	// Use this for initialization
	public override void Awake()
    {
        base.Awake();

 	    if (destroyGoOnReach)
            OnReachedEnd += () => Destroy(gameObject);    
    
        if(destroyThisOnReach && !destroyGoOnReach)
            OnReachedEnd += () => Destroy(this);   
    }

    public override void Start()
    {
        base.Start();

        EventManager.Instance.AddHandler(EventData.ID.GAMESTART, Begin);
    }
}
