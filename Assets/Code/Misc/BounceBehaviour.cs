﻿using UnityEngine;
using System.Collections;

public class BounceBehaviour : MonoBehaviour 
{
    private Vector3 initialScale;

    private float scaleFactor;

    private Vector3 startScale;
    private Vector3 endScale;

    private float duration;
    private int loops;

    public bool bouncing;

    // 0 = up, 1 = down, 2 = back, 3 = done
    private int state;

    private float passedTime;

    void Start()
    {
        initialScale = transform.localScale;
    }

    void Update()
    {
        if (bouncing)
        {
            passedTime += Time.deltaTime;

            float fraction = passedTime / duration;

            transform.localScale = Vector3.Lerp(startScale, endScale, fraction);

            if (fraction >= 1)
            {
                SwitchState(state + 1);
            }
        }
    }

    public void StartBounce(float scaleFactor, int loops, float duration)
    {
        if (!bouncing)
        {
            passedTime = 0;

            this.scaleFactor = scaleFactor;
            this.duration = duration;
            this.loops = loops;

            bouncing = true;

            SwitchState(0);
        }
    }

    private void SwitchState(int s)
    {
        passedTime = 0;
        state = s;

        switch (s)
        {
            case 0:
                {
                    startScale = transform.localScale;
                    endScale = initialScale * (1 + scaleFactor);

                    break;
                }
            case 1:
                {
                    startScale = transform.localScale;
                    endScale = initialScale * (1 - scaleFactor);

                    break;
                }
            case 2:
                {
                    loops--;

                    if (loops > 0)
                    {
                        SwitchState(0);
                    }
                    else
                    {
                        startScale = transform.localScale;
                        endScale = initialScale;
                    }

                    break;
                }
            case 3:
                {
                    bouncing = false;

                    break;
                }
        }
    }
}
