﻿using UnityEngine;
using System.Collections;

public class PlatformDependant : MonoBehaviour 
{
    public bool useOnMobile;
    public bool useOnWeb;

    void Awake()
    {
        CheckActive();
    }

    void OnEnabled()
    {
        CheckActive();
    }

    private void CheckActive()
    {
        if ((Application.isMobilePlatform && !useOnMobile) || (IsWebGL() && !useOnWeb))
            gameObject.SetActive(false);
    }

    public static bool IsWebGL()
    {
        return !Application.isMobilePlatform && !Application.isConsolePlatform && !Application.isEditor;
    }
}
