﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Utility
{
    public static string AddColorTags(string text, Color c)
    {
        return string.Format("<color={1}{2}>{0}</color>", text, "#", ColorToHexString(c));
    }

    public static string ColorToHexString(Color32 c)
    {
        return c.r.ToString("X2") + c.g.ToString("X2") + c.b.ToString("X2") + c.a.ToString("X2");
    }

    public static List<T> ShuffleList<T>(List<T> unshuffled)
    {
        return ShuffleList<T>(unshuffled.ToArray());
    }

    public static List<T> ShuffleList<T>(T[] unshuffled)
    {
        List<T> shuffled = new List<T>();

        if (unshuffled.Length > 0)
        {
            List<int> indices = new List<int>();

            // fill indices list
            for (int i = 0; i < unshuffled.Length; i++)
            {
                indices.Add(i);
            }

            // take random index and add its object to shuffle. delete index afterwards
            while (indices.Count > 0)
            {
                int randomIndex = UnityEngine.Random.Range(0, indices.Count);

                shuffled.Add(unshuffled[indices[randomIndex]]);

                indices.RemoveAt(randomIndex);
            }
        }

        return shuffled;
    }

    public static Vector3 MultiplyComponents(Vector3 left, Vector3 right)
    {
        return new Vector3(left.x * right.x, left.y * right.y, left.z * right.z);
    }

    public static Vector2 MultiplyComponents(Vector2 left, Vector2 right)
    {
        return new Vector2(left.x * right.x, left.y * right.y);
    }

    public static T LazyGetComponent<T>(GameObject go) where T : Component
    {
        T component = go.GetComponent<T>();

        if (component == null)
            component = go.AddComponent<T>();

        return component;
    }
}
