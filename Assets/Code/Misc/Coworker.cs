﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Coworker : MonoBehaviour
{
    public bool paused;

    public Func<bool> anchor;

    public event Action onRepeat;
    public event Action onEnd;

    public Coroutine coroutine;

    public void Stop(bool execute_onEnd)
    {
        StopCoroutine(coroutine);

        if (execute_onEnd && onEnd != null)
            onEnd();

        Destroy(this);
    }

    public void SetPause(bool pause)
    {
        this.paused = pause;
    }

    public void StartDelayedAction(Func<bool> anchor, Action onRepeat, Action onEnd)
    {
        this.anchor = anchor;

        this.onRepeat = onRepeat;
        this.onEnd = onEnd;

        coroutine = StartCoroutine(DelayedAction());
    }

    public void StartDelayedAction(float delay, Action onRepeat, Action onEnd)
    {
        onRepeat += (() => delay -= Time.deltaTime);

        StartDelayedAction(() => { return (delay < 0); }, onRepeat, onEnd);
    }

    public void StartRepeatAction(float delay, Action onRepeat, Action onEnd, int iterations = -1)
    {
        this.onRepeat = onRepeat;
        this.onEnd = onEnd;

        coroutine = StartCoroutine(Repeat(delay, iterations));
    }

    private IEnumerator DelayedAction()
    {
        while (true)
        {
            if (!paused)
            {
                if (onRepeat != null)
                    onRepeat();
            }

            if (anchor())
                break;

            yield return new WaitForFixedUpdate();
        }

        if(onEnd != null)
            onEnd();

        Destroy(this);
    }

    private IEnumerator Repeat(float delay, int iterations)
    {
        float timer = 0;
        float counter = 0;

        while (true)
        {
            if (!paused)
                timer += Time.deltaTime;

            if (timer >= delay)
            {
                timer = 0;

                if(onRepeat != null)
                    onRepeat();

                if (iterations > 0)
                {
                    counter++;

                    if (counter >= iterations)
                        break;
                }
            }

            yield return new WaitForFixedUpdate();
        }

        if (onEnd != null)
            onEnd();

        Destroy(this);
    }
}
