﻿using UnityEngine;
using System.Collections;

public class BumperSpawner : HeightSpawner
{
    public GameObject bumperPrefab;

    public float despawnHeight;

    public float bumperZ;

	// Update is called once per frame
	void Update () 
    {
	
	}

    public override void Spawn(Vector2 pos)
    {
        GameObject go = Instantiate(bumperPrefab) as GameObject;

        go.transform.parent = transform;
        go.transform.position = new Vector3(pos.x, pos.y, bumperZ);

        go.GetComponent<Bumper>().Init(despawnHeight + spawnOffsetMin.y);
    }    
}
