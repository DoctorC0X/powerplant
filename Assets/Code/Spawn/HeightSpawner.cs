﻿using UnityEngine;
using System.Collections;

public abstract class HeightSpawner : MonoBehaviour, HeightListener
{
    public bool activated;

    [Tooltip("In which height is the thing spawned the first time")]
    public float minHeight;

    [Tooltip("On which height (above current head pos) is the thing spawned again (min)")]
    public float nextHeightTriggerMin;
    [Tooltip("On which height (above current head pos) is the thing spawned again (max)")]
    public float nextHeightTriggerMax;

    public enum SpawnPosMode { RANDOM_RANGE, RANDOM_FIX };

    [Tooltip("RANDOM_RANGE = a value between spawn offset min and max. RANDOM_FIX = either spawn offset min or max")]
    public SpawnPosMode modeX;
    [Tooltip("RANDOM_RANGE = a value between spawn offset min and max. RANDOM_FIX = either spawn offset min or max")]
    public SpawnPosMode modeY;

    [Tooltip("When the height is reached, where is the thing spawned (min)")]
    public Vector2 spawnOffsetMin;
    [Tooltip("When the height is reached, where is the thing spawned (max)")]
    public Vector2 spawnOffsetMax;

    public virtual void Start()
    {
        // inital height marker
        HeightCheckManager.Instance.AddRelativeHeightListener(this, minHeight + Random.Range(nextHeightTriggerMin, nextHeightTriggerMax));
    }

    public virtual void HeightReached(float height)
    {
        // either get a range or fixed value for X
        float posX = 0;
        if(modeX == SpawnPosMode.RANDOM_RANGE)
            posX = Random.Range(spawnOffsetMin.x, spawnOffsetMax.x);
        else
            posX = Random.value >= 0.5f ? spawnOffsetMin.x : spawnOffsetMax.x;

        // same for Y
        float posY = 0;
        if (modeY == SpawnPosMode.RANDOM_RANGE)
            posY = Random.Range(spawnOffsetMin.y, spawnOffsetMax.y);
        else
            posY = Random.value >= 0.5f ? spawnOffsetMin.y : spawnOffsetMax.y;

        // posX and posY basically work as offset from the reached height
        if(activated)
            Spawn(new Vector2(posX, height + posY));

        // set next height marker to spawn again
        HeightCheckManager.Instance.AddRelativeHeightListener(this, Random.Range(nextHeightTriggerMin, nextHeightTriggerMax));
    }

    // we leave it to the derived class how to spawn
    public abstract void Spawn(Vector2 pos);
}
