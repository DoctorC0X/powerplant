﻿using UnityEngine;
using System.Collections;
using Uninspired.Core.Interpolation.Movement;

public class StageAnimation : MonoBehaviour 
{
    private InterpolatedMovement interpolation;
    private Vector3 startPos;

    private Coroutine delayedStartRoutine;

    public Vector2 yMinOffsetRange;
    public Vector2 yMaxOffsetRange;

    public Vector2 delayRange;

    private int dir;

    public void Init()
    {
        interpolation = GetComponent<InterpolatedMovement>();
        startPos = transform.position;

        dir = 1;

        SetTargetOffsetAndBegin();
    }

    private void SetTargetOffsetAndBegin()
    {
        Vector3 targetOffset = new Vector3();

        if (dir == 1)
            targetOffset.y = Random.Range(yMaxOffsetRange[0], yMaxOffsetRange[1]);
        else if (dir == -1)
            targetOffset.y = Random.Range(yMinOffsetRange[0], yMinOffsetRange[1]);

        dir *= -1;

        interpolation.Target = new IMTarget(startPos + targetOffset);
        interpolation.onReachedOnce += DelayedStart;
        interpolation.Begin();
    }

    private void DelayedStart()
    {
        delayedStartRoutine = StartCoroutine(WaitAndStart(Random.Range(delayRange[0], delayRange[1])));
    }

    private IEnumerator WaitAndStart(float delay)
    {
        yield return new WaitForSeconds(delay);

        SetTargetOffsetAndBegin();
    }

    public void Reset()
    {
        StopCoroutine(delayedStartRoutine);
        interpolation.Pause();
    }
}
