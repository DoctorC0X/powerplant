﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Stage : MonoBehaviour, HeightListener
{
    private StageSpawner manager;
    public SpringJoint2D balloonJoint;

    public Vector3 startPos;
    public TextMesh text;

    public CritterLandingSpot[] landingSpots;

    private int _nr;
    public int Nr
    {
        get { return _nr; }
        set
        {
            if (_nr != value)
            {
                _nr = value;
                text.GetComponent<TextMesh>().text = _nr.ToString();
            }
        }
    }

    private bool initialised;

    public void Init(StageSpawner m, int stageNr, float height)
    {
        manager = m;
        Nr = stageNr;
        SetHeight(height);

        HeightCheckManager.Instance.AddWorldHeightListener(this, height + manager.despawnHeight);

        balloonJoint.enabled = true;
        balloonJoint.connectedAnchor = balloonJoint.transform.position - new Vector3(0, 0.6f);

        initialised = true;
    }

    private void SetHeight(float height)
    {
        transform.position = new Vector3(startPos.x, height, startPos.z);
    }

    public void HeightReached(float height)
    {
        ReturnToPool();
    }

    public void ReturnToPool()
    {
        Reset();

        foreach (CritterLandingSpot current in landingSpots)
        {
            if (current.IsOccupied())
            {
                Critter c = current.attached;

                current.Detach();
                c.ReturnToPool();
            }
        }

        manager.stagePool.Push(gameObject);
    }

    public void Reset()
    {
        initialised = false;

        balloonJoint.enabled = false;

        Nr = -1;
    }

    public CritterLandingSpot GetFreeLandingSpot()
    {
        return landingSpots.First(spot => !spot.IsOccupied() && !spot.IsReserved());
    }
}
