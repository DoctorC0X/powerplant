﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Uninspired.Core.Pooling;

public class StageSpawner : MonoBehaviour, HeightListener
{
    private static StageSpawner _instance;

    public static StageSpawner Instance
    {
        get { return _instance; }
    }

    public GameObjectPool stagePool;

    public float spawnHeight;
    public float spawnGap;

    public float despawnHeight;

    public int currentStageNr;

    public event Action<Stage> OnStageSpawn;

    public bool activated;

    public void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
            return;
        }
        else
        {
            _instance = this;
        }

        stagePool.Init(this);

        despawnHeight = (stagePool.poolSize - 1) * spawnHeight;
    }

    public void Start()
    {
        HeightCheckManager.Instance.AddWorldHeightListener(this, spawnHeight - spawnGap);
    }

    public void HeightReached(float height)
    {
        currentStageNr++;
        height += spawnGap;

        if (activated && IsOutsideZone(height))
        {
            Spawn(currentStageNr, height);
        }

        HeightCheckManager.Instance.AddWorldHeightListener(this, height + spawnHeight - spawnGap);
    }

    private Stage Spawn(int nr, float height)
    {
        Stage stage = stagePool.Pop().GetComponent<Stage>();

        stage.Init(this, nr, height);

        if (OnStageSpawn != null)
            OnStageSpawn(stage);

        return stage;
    }

    public float GetStageHeight(int nr)
    {
        return nr * spawnHeight;
    }

    private bool IsOutsideZone(float height)
    {
        try
        {
            return !TrafficzoneSpawner.Instance.IsHeightInZone(height);
        }
        catch (NullReferenceException e)
        {
            Debug.LogError("TrafficzoneSpawner was null!");
            return true;
        }
    }
}
