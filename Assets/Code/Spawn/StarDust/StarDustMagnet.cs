﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using EventSystem;

public class StarDustMagnet : MonoBehaviour 
{
    public PlantModel plantModel;

    private float audioLastPlayTime;
    public float audioCooldown;

    public Vector3 offset;

    public float dotProximityRadius;
    public float dotAttractDuration;

    public float attractRange;

    public int caughtAmount;
    public float catchTimer;
    public bool catchTimerActive;

    public float throwDelayTimer;
    public bool throwing;

    public Queue<ThrowableImage.Params> throwQueue;

    public void Init(PlantModel pm)
    {
        plantModel = pm;

        this.transform.localPosition = offset;
        this.transform.localRotation = Quaternion.identity;
        this.transform.localScale = Vector3.one;
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (!GameManager.Instance.paused)
        {
            // calc numbers to throw
            if (catchTimerActive)
            {
                catchTimer -= Time.deltaTime;

                if (catchTimer <= 0)
                {
                    catchTimerActive = false;

                    if (caughtAmount > 0)
                    {
                        FillThrowQueue(caughtAmount);

                        throwing = true;
                        throwDelayTimer = plantModel.pData.starDustCollectorThrowDelay;

                        PlayerProfile.Instance.DepositStarDust(caughtAmount);

                        caughtAmount = 0;
                    }
                }
            }

            // throw numbers with a short delay
            if (throwing)
            {
                throwDelayTimer -= Time.deltaTime;

                if (throwDelayTimer < 0)
                {
                    plantModel.numberThrower.Throw(throwQueue.Dequeue(), 0);

                    if (throwQueue.Count > 0)
                    {
                        throwDelayTimer = plantModel.pData.starDustCollectorThrowDelay;
                    }
                    else
                    {
                        throwing = false;
                    }
                }
            }
        }
	}

    public void Collect(int pValue)
    {
        catchTimerActive = true;
        catchTimer = plantModel.pData.starDustCollectorTimerMax;

        // add up the caught amount and added to collected when timer ran out
        caughtAmount += pValue;

        EventManager.Instance.CallEvent(EventData.ID.STARDUST_COLLECTED, new EventData.ParamBase() { increase = pValue });

        if (Time.time - audioLastPlayTime > audioCooldown)
        {
            AudioSources.Instance.starDustCollect[UnityEngine.Random.Range(0, AudioSources.Instance.starDustCollect.Length - 1)].Play();
            audioLastPlayTime = Time.time;
        }
    }

    public void Bonus()
    {
        StarDustManager.Instance.RainSpawn();
        plantModel.numberThrower.Throw(new ThrowableImage.Params
            (
                plantModel.pData.starDustCollectorBonusSprite,
                plantModel.pData.starDustCollectorBonusColor,
                plantModel.pData.starDustCollectorBonusSize
            ), 0);
        AudioSources.Instance.starDustShapeBonus.Play();
    }

    private void FillThrowQueue(int collectedAmount)
    {
        throwQueue = new Queue<ThrowableImage.Params>();

        int[] subdivision = plantModel.pData.starDustCollectorSubdivision;

        int[] subdivisionCount = new int[subdivision.Length];

        // starting with the biggest subdivision
        for (int i = 0; i < subdivision.Length; i++)
        {
            // try to subtract that amount from collected
            while (collectedAmount - subdivision[i] >= 0)
            {
                // if possible, add up that subdivions count, else go one subdivision down
                collectedAmount -= subdivision[i];
                subdivisionCount[i]++;
            }
        }

        for (int i = 0; i < subdivisionCount.Length; i++)
        {
            for (int j = 0; j < subdivisionCount[i]; j++)
            {
                //Action onEnd = () => plantModel.numberThrower.Throw("+" + nr + suffix, c, fontSize);
                //gameObject.AddComponent<Coworker>().StartDelayedAction(delay * throwCount, null, onEnd);

                throwQueue.Enqueue(new ThrowableImage.Params
                    (
                        plantModel.pData.starDustCollectorSubdivisionSprite[i],
                        plantModel.pData.starDustCollectorSubdivionColor[i],
                        plantModel.pData.starDustCollectorSubdivisionSize[i])
                    );
            }
        }
    }
}
