﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StarDustShapeData : ScriptableObject 
{
    public Type type;

    public StarDustManager.Rarity rarity;

    public Vector2[] points;

    public Mesh dotMesh;
    public Mesh trailMesh;

    public Vector2 bbTopLeft;
    public Vector2 bbBotRight;

    public enum Type
    {
        NONE,
        ARROW,
        BATCARDI,
        BUG,
        BURGER,
        BUTTERFLY,
        CACTUS,
        CAR,
        CHERRY,
        CONTROLLER,
        CRAB,
        CUBE_45,
        CUBE_9,
        CUP,
        CUPCAKE,
        DOLLAR,
        DOLPHIN,
        EXCLAMATIONMARK,
        FIRE,
        FISH,
        FLOWER,
        GUITAR,
        HEARTBALLOON,
        HOUSE,
        ICECREAM,
        JELLYFISH,
        LEAF,
        MELON,
        PEACE,
        PEDDOKOPF,
        PENGUIN,
        ROUND,
        SEAHORSE,
        SHELL,
        SMILEY,
        SNAIL,
        SPACEINVADER,
        SPIRAL,
        STAR,
        STRAWBERRY,
        SWORD,
        TREE,
        TRIFORCE,
        TROLLFACE,
        TURTLE,
        UNINSPIRED,
        WINDELEMENT,
        YINGYANG,
    }
}
