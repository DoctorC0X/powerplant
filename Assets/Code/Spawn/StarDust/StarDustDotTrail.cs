﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StarDustDotTrail : MonoBehaviour 
{
    public LineRenderer LineRenderer
    {
        get { return GetComponent<LineRenderer>(); }
    }

    public List<Vector2> points;
    public int pointAmount;

    private float setTimer;
    public float setDelay;

	// Use this for initialization
	void Start () 
    {
        Reset();
	}

    // Update is called once per frame
    void Update() 
    {
        setTimer -= Time.deltaTime;

        LineRenderer.SetPosition(0, transform.position);
        points[0] = transform.position;

        if (setTimer <= 0)
        {
            setTimer = setDelay;

            for (int i = pointAmount - 1; i >= 1; i--)
            {
                LineRenderer.SetPosition(i, points[i - 1]);
                points[i] = points[i - 1];
            }
        }
	}

    public void Reset()
    {
        LineRenderer.SetVertexCount(pointAmount);

        points = new List<Vector2>();

        for (int i = 0; i < pointAmount; i++)
        {
            LineRenderer.SetPosition(i, transform.position);
            points.Add(transform.position);
        }

        setTimer = setDelay;
    }
}
