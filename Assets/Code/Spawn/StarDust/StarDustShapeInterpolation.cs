﻿using UnityEngine;
using System.Collections;
using System;

namespace StarDustShapeInterpolation
{
    public class Factory
    {
        public enum Type { VORTEX, BURST, CURL, CURLBURST };

        public ShapeInterpolatorAbstract Create(Type t, StarDustShape s, float radius)
        {
            switch (t)
            {
                case Type.VORTEX:
                    {
                        return new VortexInterpolator(s, radius);
                    }
                case Type.BURST:
                    {
                        return new BurstInterpolator(s, radius);
                    }
                case Type.CURL:
                    {
                        return new CurlInterpolation(s, radius);
                    }
                case Type.CURLBURST:
                    {
                        return new CurlBurstInterpolation(s, radius);
                    }
                default:
                    {
                        throw new ArgumentException("Unknown Shape");
                    }
            }
        }

        public ShapeInterpolatorAbstract CreateRandom(StarDustShape s, float radius)
        {
            Array values = Enum.GetValues(typeof(Type));
            int randomIndex = UnityEngine.Random.Range(0, values.Length);

            return Create((Type)values.GetValue(randomIndex), s, radius);
        }
    }

    public abstract class ShapeInterpolatorAbstract
    {
        public StarDustShape shape;
        public float maxRadius;

        public ShapeInterpolatorAbstract(StarDustShape s, float radius)
        {
            this.shape = s;
            this.maxRadius = radius;
        }

        public abstract void Interpolate(float fraction);
    }

    public class VortexInterpolator : ShapeInterpolatorAbstract
    {
        public VortexInterpolator(StarDustShape s, float radius)
            : base(s, radius)
        {
        }

        public override void Interpolate(float fraction)
        {
            float radius = 0;
            float currentRadiusMax = Mathf.Lerp(0, maxRadius, fraction);

            for (int pointIndex = 0; pointIndex < shape.starDustCount; pointIndex++)
            {
                Vector2 pos = new Vector2(radius * Mathf.Cos(pointIndex), radius * Mathf.Sin(pointIndex));

                shape.SetPoint(pointIndex, pos);

                radius = Mathf.Lerp(0, currentRadiusMax, (float)pointIndex / shape.starDustCount);
            }
        }
    }

    public class BurstInterpolator : ShapeInterpolatorAbstract
    {
        private int subdivisions;
        private float[] angles;
        private float dotsPerDivision;

        public BurstInterpolator(StarDustShape s, float radius, int subdivisions = 16)
            : base(s, radius)
        {
            this.subdivisions = subdivisions;

            dotsPerDivision = (shape.starDustCount / subdivisions);

            angles = new float[subdivisions];
            float anglePiece = 360 / (float)subdivisions;

            for (int a = 0; a < angles.Length; a++)
            {
                angles[a] = anglePiece * a;
            }
        }

        public override void Interpolate(float fraction)
        {
            float radius = 0;

            int pointIndex = 0;
            int angleIndex = 0;
            int round = 0;

            float deg2rad = 3.1415f / 180;

            while (pointIndex < shape.starDustCount)
            {
                Vector2 pos = new Vector2(radius * Mathf.Cos(angles[angleIndex] * deg2rad), radius * Mathf.Sin(angles[angleIndex] * deg2rad));

                shape.SetPoint(pointIndex, pos);

                pointIndex++;
                angleIndex += 2;

                if (angleIndex >= subdivisions)
                {
                    round++;

                    if (round % 2 == 0)
                    {
                        angleIndex = 0;
                    }
                    else
                    {
                        angleIndex = 1;
                    }

                    radius = Mathf.Lerp(0, maxRadius, round / dotsPerDivision);
                }
            }
        }
    }

    public class CurlInterpolation : ShapeInterpolatorAbstract
    {
        public CurlInterpolation(StarDustShape s, float radius)
            : base(s, radius)
        {
        }

        public override void Interpolate(float fraction)
        {
            float radius = 0;
            float currentRadiusMax = Mathf.Lerp(0, maxRadius, fraction);

            int pointIndex = 0;
            int angle = 0;

            float deg2rad = 3.1415f / 180;

            while (pointIndex < shape.starDustCount)
            {
                Vector2 pos = new Vector2(radius * Mathf.Cos(angle * deg2rad), radius * Mathf.Sin(angle * deg2rad));

                shape.SetPoint(pointIndex, pos);

                radius = Mathf.Lerp(0, currentRadiusMax, (float)pointIndex / shape.starDustCount);

                pointIndex++;
                angle += 30;
            }
        }
    }

    public class CurlBurstInterpolation : ShapeInterpolatorAbstract
    {
        private int subdivisions;
        private float[] angles;

        public CurlBurstInterpolation(StarDustShape s, float radius, int subdivisions = 16)
            : base(s, radius)
        {
            this.subdivisions = subdivisions;

            angles = new float[subdivisions];
            float anglePiece = 360 / (float)subdivisions;

            for (int a = 0; a < angles.Length; a++)
            {
                angles[a] = anglePiece * a;
            }
        }

        public override void Interpolate(float fraction)
        {
            float radius = 0;
            float currentRadiusMax = Mathf.Lerp(0, maxRadius, fraction);

            int pointIndex = 0;
            int angleIndex = 0;

            float deg2rad = 3.1415f / 180;

            while (pointIndex < shape.starDustCount)
            {
                Vector2 pos = new Vector2(radius * Mathf.Cos(angles[angleIndex] * deg2rad), radius * Mathf.Sin(angles[angleIndex] * deg2rad));

                shape.SetPoint(pointIndex, pos);

                radius = Mathf.Lerp(0, currentRadiusMax, (float)pointIndex / shape.starDustCount);

                pointIndex++;
                angleIndex++;

                angleIndex = angleIndex % angles.Length;
            }
        }
    }
}