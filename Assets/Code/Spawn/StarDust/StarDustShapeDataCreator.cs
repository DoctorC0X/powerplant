﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StarDustShapeDataCreator : MonoBehaviour 
{
    public void FillPointArray(StarDustShapeData data, bool revert)
    {
        List<Vector2> newPoints = new List<Vector2>();

        int childCount = transform.childCount;

        Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
        Vector2 max = Vector2.zero;

        for (int i = childCount - 1; i >= 0; i--)
        {
            Vector2 localPos = transform.GetChild(i).localPosition;

            if (localPos.x < min.x)
                min.x = localPos.x;

            if (localPos.x > max.x)
                max.x = localPos.x;

            if (localPos.y < min.y)
                min.y = localPos.y;

            if (localPos.y > max.y)
                max.y = localPos.y;

            newPoints.Add(localPos);
            //DestroyImmediate(transform.GetChild(i).gameObject);
        }

        data.bbBotRight = new Vector2(max.x, min.y);
        data.bbTopLeft = new Vector2(min.x, max.y);

        if (revert)
            newPoints.Reverse();

        data.points = newPoints.ToArray();

        if (data.points.Length < 100)
            data.rarity = StarDustManager.Rarity.COMMON;
        else if (data.points.Length <= 150)
            data.rarity = StarDustManager.Rarity.UNCOMMON;
        else if (data.points.Length < 200)
            data.rarity = StarDustManager.Rarity.RARE;
        else
            data.rarity = StarDustManager.Rarity.LEGENDARY;
    }

    public Mesh CreateDotMesh(Vector2[] points)
    {
        Vector3[] vertices = new Vector3[points.Length * 4];
        Vector2[] uv = new Vector2[points.Length * 4];

        List<int>[] triangles = new List<int>[3];
        int currentSubMesh = 0;

        for (int i = 0; i < triangles.Length; i++)
            triangles[i] = new List<int>();

        for (int i = 0; i < points.Length; i++)
        {
            int vertexIndex = i * 4;

            vertices[vertexIndex + 0].x = points[i].x - 1;
            vertices[vertexIndex + 0].y = points[i].y - 1;
                                                        
            vertices[vertexIndex + 1].x = points[i].x - 1;
            vertices[vertexIndex + 1].y = points[i].y + 1;
                                                        
            vertices[vertexIndex + 2].x = points[i].x + 1;
            vertices[vertexIndex + 2].y = points[i].y + 1;
                                                        
            vertices[vertexIndex + 3].x = points[i].x + 1;
            vertices[vertexIndex + 3].y = points[i].y - 1;

            /*
            uv[vertexIndex + 0] = new Vector2(0, 0);
            uv[vertexIndex + 1] = new Vector2(0, 1);
            uv[vertexIndex + 2] = new Vector2(1, 1);
            uv[vertexIndex + 3] = new Vector2(1, 0);
            */

            uv[vertexIndex + 0].x = 0;
            uv[vertexIndex + 0].y = 0;

            uv[vertexIndex + 1].x = 0;
            uv[vertexIndex + 1].y = 1;

            uv[vertexIndex + 2].x = 1;
            uv[vertexIndex + 2].y = 1;

            uv[vertexIndex + 3].x = 1;
            uv[vertexIndex + 3].y = 0;

            triangles[currentSubMesh].Add(vertexIndex + 0);
            triangles[currentSubMesh].Add(vertexIndex + 1);
            triangles[currentSubMesh].Add(vertexIndex + 2);
            triangles[currentSubMesh].Add(vertexIndex + 2);
            triangles[currentSubMesh].Add(vertexIndex + 3);
            triangles[currentSubMesh].Add(vertexIndex + 0);

            currentSubMesh++;

            currentSubMesh = currentSubMesh % 3;
        }

        Mesh dotMesh = new Mesh();
        dotMesh.subMeshCount = 3;
        dotMesh.vertices = vertices;
        dotMesh.uv = uv;

        //dotFilter.mesh.triangles = triangles;
        for (int i = 0; i < dotMesh.subMeshCount; i++)
            dotMesh.SetTriangles(triangles[i], i);

        dotMesh.RecalculateBounds();
        dotMesh.RecalculateNormals();
        dotMesh.Optimize();

        return dotMesh;
    }

    public Mesh CreateTrailMesh(Vector2[] points)
    {
        Vector3[] vertices = new Vector3[points.Length * 3];
        Vector2[] uv = new Vector2[points.Length * 3];
        int[] triangles = new int[points.Length * 3];

        for (int i = 0; i < points.Length; i++)
        {
            int vertexIndex = i * 3;

            vertices[vertexIndex + 0].x = points[i].x + 1 * -1;
            vertices[vertexIndex + 0].y = points[i].y + 0 * -1;

            vertices[vertexIndex + 1].x = points[i].x + 1;
            vertices[vertexIndex + 1].y = points[i].y + 0;

            vertices[vertexIndex + 2] = points[i];

            /*
            uv[vertexIndex + 0] = new Vector2(0, 0);
            uv[vertexIndex + 1] = new Vector2(0, 1);
            uv[vertexIndex + 2] = new Vector2(1, 0.5f);
            */

            uv[vertexIndex + 0].x = 0;
            uv[vertexIndex + 0].y = 0;

            uv[vertexIndex + 1].x = 0;
            uv[vertexIndex + 1].y = 1;

            uv[vertexIndex + 2].x = 1;
            uv[vertexIndex + 2].y = 0.5f;

            int triIndex = i * 3;

            triangles[triIndex + 0] = vertexIndex + 0;
            triangles[triIndex + 1] = vertexIndex + 1;
            triangles[triIndex + 2] = vertexIndex + 2;
        }

        Mesh trailMesh = new Mesh();
        trailMesh.vertices = vertices;
        trailMesh.uv = uv;
        trailMesh.triangles = triangles;

        trailMesh.RecalculateBounds();
        trailMesh.RecalculateNormals();
        trailMesh.Optimize();

        return trailMesh;
    }
}
