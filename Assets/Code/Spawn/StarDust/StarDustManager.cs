﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Uninspired.Core.Interpolation.Movement;
using Uninspired.Core.Pooling;

public class StarDustManager : HeightSpawner
{
    private static StarDustManager _instance;

    public static StarDustManager Instance
    {
        get { return _instance; }
    }

    public float spawnAreaGap;

    public enum Rarity { COMMON, UNCOMMON, RARE, LEGENDARY };
    private float[] rarityChances = { 0.66f, 0.22f, 0.1f, 0.02f };

    public GameObjectPool shapePool;

    public List<StarDustShape> activeShapes;
    public float shapeZ;

    public string shapeDataPath;
    public Dictionary<Rarity, List<StarDustShapeData>> shapeDataDict;

    public float dotSpeed;
    public float dotSize;
    public float trailSpeed;
    public float trailSize;
    public float trailTime;

    private float dotMaterialTimer;
    public float dotMaterialDelay;
    public Material[] dotMaterials;

    public float shapeDespawnHeight;

    public StarDustShapeData shapeData50;
    public StarDustShapeData shapeData100;

    public float fallSpeed1;
    public float fallSpeed2;

    public Action<bool> OnShapeDespawn;

    public GameObject starDustDummyPrefab;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            _instance = this;
        }

        // instantiate material
        dotMaterialTimer = dotMaterialDelay;

        for (int i = 0; i < dotMaterials.Length; i++)
        {
            dotMaterials[i] = Instantiate(dotMaterials[i]);
        }
        //

        //create and fill shape pool
        shapePool.OnInit += (GameObject go) => go.GetComponent<StarDustShape>().dotFilter.GetComponent<MeshRenderer>().sharedMaterials = dotMaterials;
        shapePool.Init(this);

        shapePool.OnPop += (GameObject go) => activeShapes.Add(go.GetComponent<StarDustShape>());
        shapePool.OnPush += (GameObject go) => activeShapes.Remove(go.GetComponent<StarDustShape>());

        //fill shapeDict for different spawn chances
        shapeDataDict = new Dictionary<Rarity, List<StarDustShapeData>>();

        foreach (Rarity current in Enum.GetValues(typeof(Rarity)))
        {
            shapeDataDict.Add(current, new List<StarDustShapeData>());
        }

        foreach (StarDustShapeData current in Resources.LoadAll<ScriptableObject>(shapeDataPath))
        {
            shapeDataDict[current.rarity].Add(current);
        }
        //
    }

    public override void Start()
    {
        base.Start();

        spawnOffsetMin = new Vector2(FollowCanvas.Instance.topLeft.transform.position.x, spawnOffsetMin.y);
        spawnOffsetMax = new Vector2(FollowCanvas.Instance.botRight.transform.position.x, spawnOffsetMax.y);
    }

    void Update()
    {
        // animate dots
        dotMaterialTimer -= Time.deltaTime;

        if (dotMaterialTimer <= 0)
        {
            dotMaterialTimer = dotMaterialDelay;

            UpdateMaterials();
        }
    }

    private void UpdateMaterials()
    {
        float xTiling = dotMaterials[0].GetTextureScale("_MainTex").x;

        for (int i = 0; i < dotMaterials.Length; i++)
        {
            Vector2 offset = dotMaterials[i].GetTextureOffset("_MainTex");

            offset.x += xTiling;

            if (offset.x >= 1)
            {
                offset.x = 0;
            }

            dotMaterials[i].SetTextureOffset("_MainTex", offset);
        }
    }

    private void Spawn(StarDustShapeData data, Vector2 pos)
    {
        //StarDustShapeData shapePoints = Instantiate(sP) as StarDustShapeData;

        StarDustShape newShape = GetShapeFromPool();

        newShape.Init(this, PlantModel.MainPlant.dustMagnet, data, shapeDespawnHeight + spawnOffsetMax.y);

        newShape.transform.position = new Vector3(pos.x, pos.y, shapeZ);

        float distanceLeft = newShape.topLeft.position.x - FollowCanvas.Instance.topLeft.transform.position.x + spawnAreaGap;
        float distanceRight = FollowCanvas.Instance.botRight.transform.position.x - spawnAreaGap - newShape.botRight.position.x;

        if (distanceLeft < 0)
        {
            newShape.transform.Translate(distanceLeft * -1, 0, 0);
        }
        else if (distanceRight < 0)
        {
            newShape.transform.Translate(distanceRight, 0, 0);
        }

        newShape.SetAllDotsActive(true);
    }

    //spawn random shape in group
    public void Spawn(Rarity group, Vector2 pos)
    {
        int randomIndex = UnityEngine.Random.Range(0, shapeDataDict[group].Count);

        Spawn(shapeDataDict[group][randomIndex], pos);
    }

    // spawn random shape (used by HeightSpawner base class)
    public override void Spawn(Vector2 pos)
    {
        Rarity r = GetRarityByChance();

        Spawn(r, pos);
    }

    /*
    // spawn specific shape
    public void Spawn(int index, Vector2 pos)
    {
        Spawn(shapes[index], pos);
    }
    */

    private Rarity GetRarityByChance()
    {
        Rarity result = Rarity.COMMON;
        float value = UnityEngine.Random.value;
        float currentRarity = 0;

        for (int i = 0; i < rarityChances.Length; i++)
        {
            currentRarity += rarityChances[i];

            if (value <= currentRarity)
            {
                result = (Rarity)Enum.GetValues(typeof(Rarity)).GetValue(i);
                break;
            }
        }

        return result;
    }

    public StarDustShape GetShapeFromPool()
    {
        StarDustShape shape = shapePool.Pop().GetComponent<StarDustShape>();

        shape.gameObject.SetActive(true);

        shape.transform.parent = transform;

        return shape;
    }

    public void ReturnShapeToPool(StarDustShape shape)
    {
        if(shape.topLeft != null)
            Destroy(shape.topLeft.gameObject);

        if (shape.botRight != null)
            Destroy(shape.botRight.gameObject);

        shape.Reset();
        shapePool.Push(shape.gameObject);
    }

    // spawn bonus rain
    public void RainSpawn()
    {
        // shape one
        StarDustShape newShape = GetShape50();
        newShape.transform.position = new Vector3(transform.position.x, PlantModel.MainPlant.dustMagnet.transform.position.y + spawnOffsetMin.y, shapeZ);
        newShape.SetAllDotsActive(true);

        float xStep = (spawnOffsetMax.x - spawnOffsetMin.x) / (shapeData50.points.Length * 2);

        for (int i = 0, j = 0; i < shapeData50.points.Length; i++, j += 2)
        {
            newShape.SetPoint(i, spawnOffsetMin.x + (xStep * j), UnityEngine.Random.Range(-1.5f, 1.5f), true);
        }

        newShape.SetBounds(new Vector2(spawnOffsetMin.x, 1.5f), new Vector2(spawnOffsetMax.x, -1.5f));
        newShape.SetFalling(fallSpeed1);
        
        // shape two
        newShape = GetShape50();
        newShape.transform.position = new Vector3(transform.position.x, PlantModel.MainPlant.dustMagnet.transform.position.y + spawnOffsetMin.y, shapeZ);
        newShape.SetAllDotsActive(true);

        for (int i = 0, j = 1; i < shapeData50.points.Length; i++, j += 2)
        {
            newShape.SetPoint(i, spawnOffsetMin.x + (xStep * j), UnityEngine.Random.Range(-1.5f, 1.5f), true);
        }

        newShape.SetBounds(new Vector2(spawnOffsetMin.x, 1.5f), new Vector2(spawnOffsetMax.x, -1.5f));
        newShape.SetFalling(fallSpeed2);
    }

    public StarDustShape GetShape50(float shapeDespawnOffset = 0)
    {
        StarDustShape newShape = GetShapeFromPool();

        newShape.Init(this, PlantModel.MainPlant.dustMagnet, shapeData50, shapeDespawnHeight + shapeDespawnOffset);

        return newShape;
    }

    public StarDustShape GetShape100(float shapeDespawnOffset = 0)
    {
        StarDustShape newShape = GetShapeFromPool();

        newShape.Init(this, PlantModel.MainPlant.dustMagnet, shapeData100, shapeDespawnHeight + shapeDespawnOffset);

        return newShape;
    }

    private void SpawnStarDustDummys(int amount, Vector3 startPos, Action<InterpolatedMovement> settingTarget, Vector2 spawnRange, Vector2 durationRange)
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject newDummy = Instantiate(starDustDummyPrefab) as GameObject;

            newDummy.transform.parent = transform;
            newDummy.transform.position = startPos;
            newDummy.transform.Translate(UnityEngine.Random.Range(-spawnRange.x, spawnRange.x), UnityEngine.Random.Range(-spawnRange.y, spawnRange.y), -3);

            InterpolatedMovement dummyMovement = newDummy.GetComponent<InterpolatedMovement>();
            settingTarget(dummyMovement);
            dummyMovement.Motor = new IMMotor(IMMotor.MotorMode.DURATION, UnityEngine.Random.Range(durationRange[0], durationRange[1]));
            dummyMovement.onReachedOnce += () => Destroy(dummyMovement.gameObject);
            dummyMovement.Begin();
        }
    }

    public void SpawnStarDustDummys(int amount, Vector3 startPos, Vector3 targetPos, Vector2 spawnRange, Vector2 durationRange)
    {
        SpawnStarDustDummys(amount, startPos, (InterpolatedMovement im) => im.Target = new IMTarget(targetPos), spawnRange, durationRange);
    }

    public void SpawnStarDustDummys(int amount, Vector3 startPos, Transform target, Vector2 spawnRange, Vector2 durationRange)
    {
        SpawnStarDustDummys(amount, startPos, (InterpolatedMovement im) => im.Target = new IMTarget(target), spawnRange, durationRange);
    }

    /*
    public void HeadRainSpawn(float xPos, int amount, int valuePerDot, float delay)
    {
        StartCoroutine(HeadRain(xPos, amount, valuePerDot, delay));
    }

    private IEnumerator HeadRain(float xPos, int amount, int valuePerDot, float delay)
    {
        for (int i = amount; i > 0; i--)
        {
            GameObject go = GetDotFromPool(DotUsage.RAINING);

            go.transform.parent = transform;

            float x = Mathf.Lerp(-1f, 1f, UnityEngine.Random.value);

            Vector3 headPos = PlantModel.MainPlant.plantHead.transform.position;

            go.transform.position = new Vector3(xPos + x, headPos.y + 8, headPos.z);

            go.SetActive(true);

            go.GetComponent<StarDustDot>().Init(null, valuePerDot, true, rainFallSpeedRange[1]);

            yield return new WaitForSeconds(delay);
        }
    }
    */
}
