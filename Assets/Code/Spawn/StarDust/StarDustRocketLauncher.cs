﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Uninspired.Core.Pooling;

public class StarDustRocketLauncher : MonoBehaviour, PauseListener
{
    private static StarDustRocketLauncher _instance;
    public static StarDustRocketLauncher Instance
    {
        get { return _instance; }
    }

    public GameObjectPool pool;

    public float[] xLanes;
    public float[] yLanes;

    private List<float> xPos;
    private List<float> yPos;

    public float launchDelayMin;
    public float launchDelayMax;

    private float currentDelayTimer;

    private int fireCount;

    public List<StarDustRocket> activeRockets;

    private bool firing;
    private bool paused;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
            return;
        }
        else
        {
            _instance = this;
        }

        pool.Init(this);

        if (pool.poolSize != xLanes.Length || pool.poolSize != yLanes.Length)
            throw new System.ArgumentException();

        activeRockets = new List<StarDustRocket>();
        pool.OnPop  += (GameObject go) => { activeRockets.Add(go.GetComponent<StarDustRocket>()); };
        pool.OnPush += (GameObject go) => { activeRockets.Remove(go.GetComponent<StarDustRocket>()); };

        GameManager.Instance.pauseListener.Add(this);
    }

    public void Fire()
    {
        if (!firing)
        {
            xPos = Utility.ShuffleList<float>(xLanes);
            yPos = Utility.ShuffleList<float>(yLanes);

            firing = true;
            fireCount = pool.poolSize;
        }
    }

    void Update()
    {
        if (firing && !paused)
        {
            currentDelayTimer -= Time.deltaTime;

            if (currentDelayTimer <= 0)
            {
                GameObject rocketGo = pool.Pop();

                StarDustRocket rocket = rocketGo.GetComponent<StarDustRocket>();
                rocket.Init(this, transform.position + new Vector3(xPos[fireCount - 1], yPos[fireCount - 1], 0));
                rocket.Launch();

                currentDelayTimer = Random.Range(launchDelayMin, launchDelayMax);
                fireCount--;
            }

            if (fireCount == 0)
            {
                firing = false;
                currentDelayTimer = 0;
            }
        }
    }

    public void SetPause(bool pause)
    {
        paused = pause;

        for (int i = 0; i < activeRockets.Count; i++)
        {
            activeRockets[i].SetPause(pause);
        }
    }

    public bool IsPaused()
    {
        return paused;
    }
}
