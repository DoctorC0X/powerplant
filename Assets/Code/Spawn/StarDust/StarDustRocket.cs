﻿using UnityEngine;
using System.Collections;
using StarDustShapeInterpolation;
using System;
using Uninspired.Core.Interpolation.Movement;

public class StarDustRocket : MonoBehaviour 
{
    private StarDustRocketLauncher launcher;
    private InterpolatedMovement movement;

    public StarDustShape shape;

    public AnimationCurve curve;

    private ShapeInterpolatorAbstract explosion;

    public float explosionRadius;
    public float explosionDuration;

    public bool exploding;
    private float passedExplosionTime;

    public float fallSpeed;

    private bool paused;

    public void Init(StarDustRocketLauncher l, Vector3 target)
    {
        launcher = l;

        exploding = false;
        paused = false;

        shape = StarDustManager.Instance.GetShape50();
        shape.transform.parent = transform;
        shape.transform.localPosition = Vector3.zero;

        shape.SetBounds(Vector2.zero, Vector2.zero);
        shape.SetAllDotsActive(true);
        shape.collectable = false;
        shape.OnDespawn += Reset;

        for (int i = 0; i < shape.starDustCount; i++)
        {
            shape.SetPoint(i, Vector2.zero, true);
        }

        Factory factory = new Factory();
        explosion = factory.CreateRandom(shape, explosionRadius);

        movement = GetComponent<InterpolatedMovement>();
        movement.Target.Point = target;
        movement.onReachedOnce += OnReached;
    }

    private void Reset()
    {
        movement.Pause();
        exploding = false;
        explosion = null;
        shape = null;

        launcher.pool.Push(gameObject);
    }

    public void Launch()
    {
        AudioSources.Instance.starDustRocket_launch.Play();

        movement.Begin();
    }

    void Update()
    {
        if (exploding && !paused)
        {
            passedExplosionTime += Time.deltaTime;

            float fraction = passedExplosionTime / explosionDuration;

            fraction = curve.Evaluate(fraction);

            explosion.Interpolate(fraction);

            shape.SetBounds(new Vector2(-explosionRadius, explosionRadius), new Vector2(explosionRadius, -explosionRadius));

            if (fraction >= 1)
            {
                shape.SetFalling(fallSpeed);
                shape.transform.parent = null;

                Reset();
            }
        }
    }

    private void OnReached()
    {
        AudioSources.Instance.starDustRocket_explode.Play();

        exploding = true;
        shape.collectable = true;

        passedExplosionTime = 0;

        shape.SetAllDotsActive(true);
    }

    public void SetPause(bool paused)
    {
        this.paused = paused;

        if (paused)
            movement.Pause();
        else
            movement.Unpause();
    }
}