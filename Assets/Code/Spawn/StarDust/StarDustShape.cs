﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using EventSystem;

public class StarDustShape : MonoBehaviour, HeightListener
{
    public StarDustManager manager;

    public StarDustMagnet dustMagnet;

    public MeshFilter dotFilter;
    public MeshFilter trailFilter;

    public string currentShapeName;

    public enum DotState { INACTIVE, WAITING, CAUGHT };

    public Vector2[] points;
    private Vector3[] dotVertices;
    public DotState[] dotState;
    private bool[] dotMoving;
    private bool dotMeshManipulated;

    private Vector2[] trailPoints;
    private Vector3[] trailVertices;
    private bool trailMeshManipulated;

    private float[] trailTimer;

    public int starDustCount;

    public static Vector2 gap = new Vector2(1, 1);

    public Transform topLeft;
    public Transform botRight;

    public int starDustValue = 1;

    public bool falling;
    public float fallSpeed;

    public bool initialised;
    public bool collectable = true;

    public StarDustShapeData.Type type;

    public event Action OnDespawn;

    public void Init(StarDustManager m, StarDustMagnet magnet, StarDustShapeData data, float despawnHeight)
    {
        manager = m;
        dustMagnet = magnet;

        currentShapeName = data.name;

        GameObject tl = new GameObject("top left");
        tl.transform.parent = transform;
        topLeft = tl.transform;

        GameObject br = new GameObject("bot right");
        br.transform.parent = transform;
        botRight = br.transform;

        SetBounds(data.bbTopLeft, data.bbBotRight);

        starDustCount = data.points.Length;

        points = new Vector2[data.points.Length];

        dotFilter.mesh = data.dotMesh;
        trailFilter.mesh = data.trailMesh;

        dotVertices = data.dotMesh.vertices;
        trailVertices = data.trailMesh.vertices;

        dotState = new DotState[data.points.Length];
        dotMoving = new bool[data.points.Length];

        trailPoints = new Vector2[data.points.Length];
        trailTimer = new float[data.points.Length];

        for (int i = 0; i < data.points.Length; i++)
        {
            dotState[i] = DotState.INACTIVE;
            points[i] = data.points[i];
            trailPoints[i] = data.points[i];

            SetDotVertices(i);
            SetTrailPointVertex(i);
        }

        dotFilter.mesh.vertices = dotVertices;
        trailFilter.mesh.vertices = trailVertices;

        type = data.type;

        HeightCheckManager.Instance.AddRelativeHeightListener(this, despawnHeight);

        //CreateDotMesh();
        //CreateTrailMesh();

        initialised = true;
    }

    public void Reset()
    {
        points = null;
        dotState = null;
        trailPoints = null;
        trailTimer = null;
        dotMoving = null;
        dotVertices = null;
        trailVertices = null;

        falling = false;

        initialised = false;

        collectable = true;

        if (OnDespawn != null)
        {
            OnDespawn();
            OnDespawn = null;
        }
    }

    void Update()
    {
        // debug draw proximity area
        Debug.DrawLine(topLeft.position, new Vector2(botRight.position.x, topLeft.position.y), new Color(0.5f, 0, 0));
        Debug.DrawLine(new Vector2(botRight.position.x, topLeft.position.y), botRight.position, new Color(0.5f, 0, 0));
        Debug.DrawLine(topLeft.position, new Vector2(topLeft.position.x, botRight.position.y), new Color(0.5f, 0, 0));
        Debug.DrawLine(new Vector2(topLeft.position.x, botRight.position.y), botRight.position, new Color(0.5f, 0, 0));

        if (initialised)
        {
            // debug controls
            Vector2 debugDir = Vector2.zero;

            if (Input.GetKey(KeyCode.I))
            {
                debugDir += Vector2.up;
            }

            if (Input.GetKey(KeyCode.K))
            {
                debugDir += Vector2.up * -1;
            }

            if (Input.GetKey(KeyCode.L))
            {
                debugDir += Vector2.right;
            }

            if (Input.GetKey(KeyCode.J))
            {
                debugDir += Vector2.right * -1;
            }

            if (debugDir.sqrMagnitude > 0)
                MoveShape(debugDir, manager.dotSpeed * Time.deltaTime);
            //

            // handle falling
            if (falling && !GameManager.Instance.paused)
            {
                MoveShape(Vector2.down, fallSpeed * Time.deltaTime);
            }
            //

            // update trail point
            for (int i = 0; i < points.Length; i++)
            {
                if (dotMoving[i] && !GameManager.Instance.paused)
                {
                    if (trailTimer[i] > 0)
                    {
                        trailTimer[i] -= Time.deltaTime;
                    }
                    else
                    {
                        trailMeshManipulated = true;

                        float step = Time.deltaTime * manager.trailSpeed;

                        trailPoints[i] = Vector2.Lerp(trailPoints[i], points[i], step);

                        if ((points[i] - trailPoints[i]).sqrMagnitude < step * step)
                        {
                            trailPoints[i] = points[i];

                            dotMoving[i] = false;
                        }

                        SetTrailPointVertex(i);
                    }
                }
            }
            //

            // check proximity & move dots
            if (dustMagnet != null && collectable && starDustCount > 0)
            {
                bool magnetInBoundingBox =
                    dustMagnet.transform.position.x - dustMagnet.attractRange < botRight.position.x &&
                    dustMagnet.transform.position.x + dustMagnet.attractRange > topLeft.position.x &&
                    dustMagnet.transform.position.y - dustMagnet.attractRange < topLeft.position.y &&
                    dustMagnet.transform.position.y + dustMagnet.attractRange > botRight.position.y;

                for (int i = 0; i < points.Length; i++)
                {
                    Vector2 dir = (Vector2)dustMagnet.transform.position - ((Vector2)transform.position + points[i]);

                    if (magnetInBoundingBox && dotState[i] == DotState.WAITING)
                    {
                        if (dir.sqrMagnitude <= dustMagnet.attractRange * dustMagnet.attractRange)
                        {
                            dotState[i] = DotState.CAUGHT;
                        }
                    }

                    if (dotState[i] == DotState.CAUGHT)
                    {
                        float step = manager.dotSpeed * Time.deltaTime;

                        if (dir.sqrMagnitude >= step * step)
                        {
                            MovePoint(i, dir.normalized, step);
                        }
                        else
                        {
                            SetPoint(i, dustMagnet.transform.position - transform.position);

                            SetDotActive(i, false);

                            dotMoving[i] = false;
                            trailPoints[i] = points[i];

                            dustMagnet.Collect(starDustValue);

                            starDustCount--;

                            if (starDustCount <= 0 && type != StarDustShapeData.Type.NONE)
                            {
                                dustMagnet.Bonus();
                                EventManager.Instance.CallEvent(EventData.ID.STARDUST_BONUS, new EventData.ParamShape() { increase = 1, shape = this });
                            }
                        }
                    }
                }

                //debug
                if (magnetInBoundingBox)
                    Debug.DrawLine(transform.position, dustMagnet.transform.position, Color.green);
                else
                    Debug.DrawLine(transform.position, dustMagnet.transform.position, Color.red);
            }

            if (dotMeshManipulated)
            {
                dotFilter.mesh.vertices = dotVertices;
                dotMeshManipulated = false;
            }

            if (trailMeshManipulated)
            {
                trailFilter.mesh.vertices = trailVertices;
                trailMeshManipulated = false;
            }
            //
        }
    }

    public void SetFalling(float speed)
    {
        falling = true;
        fallSpeed = speed;
    }

    public void CreateDotMesh()
    {
        dotVertices = new Vector3[points.Length * 4];
        Vector2[] uv = new Vector2[dotVertices.Length];

        List<int>[] triangles = new List<int>[3];
        int currentSubMesh = 0;

        for (int i = 0; i < triangles.Length; i++)
            triangles[i] = new List<int>();

        for (int i = 0; i < points.Length; i++)
        {
            int vertexIndex = i * 4;

            SetDotVertices(i);

            /*
            uv[vertexIndex + 0] = new Vector2(0, 0);
            uv[vertexIndex + 1] = new Vector2(0, 1);
            uv[vertexIndex + 2] = new Vector2(1, 1);
            uv[vertexIndex + 3] = new Vector2(1, 0);
            */

            uv[vertexIndex + 0].x = 0;
            uv[vertexIndex + 0].y = 0;

            uv[vertexIndex + 1].x = 0;
            uv[vertexIndex + 1].y = 1;

            uv[vertexIndex + 2].x = 1;
            uv[vertexIndex + 2].y = 1;

            uv[vertexIndex + 3].x = 1;
            uv[vertexIndex + 3].y = 0;

            triangles[currentSubMesh].Add(vertexIndex + 0);
            triangles[currentSubMesh].Add(vertexIndex + 1);
            triangles[currentSubMesh].Add(vertexIndex + 2);
            triangles[currentSubMesh].Add(vertexIndex + 2);
            triangles[currentSubMesh].Add(vertexIndex + 3);
            triangles[currentSubMesh].Add(vertexIndex + 0);

            currentSubMesh++;

            currentSubMesh = currentSubMesh % 3;
        }

        dotFilter.mesh = new Mesh();
        dotFilter.mesh.subMeshCount = 3;
        dotFilter.mesh.vertices = dotVertices;
        dotFilter.mesh.uv = uv;
        
        //dotFilter.mesh.triangles = triangles;
        for (int i = 0; i < dotFilter.mesh.subMeshCount; i++)
            dotFilter.mesh.SetTriangles(triangles[i], i);

        dotFilter.mesh.RecalculateNormals();
    }

    public void CreateTrailMesh()
    {
        trailVertices = new Vector3[points.Length * 3];
        Vector2[] uv = new Vector2[trailVertices.Length];
        int[] triangles = new int[trailVertices.Length];

        for (int i = 0; i < points.Length; i++)
        {
            int vertexIndex = i * 3;

            SetTrailVertices(i);

            /*
            uv[vertexIndex + 0] = new Vector2(0, 0);
            uv[vertexIndex + 1] = new Vector2(0, 1);
            uv[vertexIndex + 2] = new Vector2(1, 0.5f);
            */

            uv[vertexIndex + 0].x = 0;
            uv[vertexIndex + 0].y = 0;

            uv[vertexIndex + 1].x = 0;
            uv[vertexIndex + 1].y = 1;

            uv[vertexIndex + 2].x = 1;
            uv[vertexIndex + 2].y = 0.5f;

            int triIndex = i * 3;

            triangles[triIndex + 0] = vertexIndex + 0;
            triangles[triIndex + 1] = vertexIndex + 1;
            triangles[triIndex + 2] = vertexIndex + 2;
        }

        trailFilter.mesh = new Mesh();
        trailFilter.mesh.vertices = trailVertices;
        trailFilter.mesh.uv = uv;
        trailFilter.mesh.triangles = triangles;

        trailFilter.mesh.RecalculateNormals();
    }

    public void SetPoint(int index, float x, float y, bool ignoreTrail = false)
    {
        //trailDistance[index] += (pos - points[index]).magnitude;

        if (dotState[index] != DotState.INACTIVE)
        {
            points[index].x = x;
            points[index].y = y;

            if (ignoreTrail)
            {
                trailPoints[index] = points[index];
                SetTrailPointVertex(index);
            }
            else
            {
                if (!dotMoving[index])
                {
                    dotMoving[index] = true;
                    trailTimer[index] = manager.trailTime;
                }
            }

            dotMeshManipulated = true;
            trailMeshManipulated = true;
        }

        SetDotVertices(index);
    }

    public void SetPoint(int index, Vector2 pos, bool ignoreTrail = false)
    {
        SetPoint(index, pos.x, pos.y, ignoreTrail);
    }

    public void MovePoint(int index, Vector2 dir, float speed)
    {
        if(dotState[index] != DotState.INACTIVE)
        {
            points[index] += (dir * speed);

            if (!dotMoving[index])
            {
                dotMoving[index] = true;
                trailTimer[index] = manager.trailTime;
            }

            dotMeshManipulated = true;
            trailMeshManipulated = true;
        }

        SetDotVertices(index);
    }

    public void MoveShape(Vector2 dir, float speed)
    {
        transform.position += (Vector3)(dir * speed);

        for (int i = 0; i < points.Length; i++)
        {
            if (dotState[i] != DotState.INACTIVE)
            {
                if (!dotMoving[i])
                {
                    dotMoving[i] = true;
                    trailTimer[i] = manager.trailTime;
                }

                trailPoints[i] -= (dir * speed);

                SetTrailPointVertex(i);
            }

            SetTrailVertices(i);
        }
    }

    public void SetDotActive(int index, bool active)
    {
        if (active)
        {
            dotState[index] = DotState.WAITING;
        }
        else
        {
            dotState[index] = DotState.INACTIVE;
        }

        SetDotVertices(index);
    }

    public void SetAllDotsActive(bool active)
    {
        for (int i = 0; i < points.Length; i++)
        {
            SetDotActive(i, active);
        }
    }

    public void SetBounds(Vector2 topLeft, Vector2 botRight)
    {
        this.topLeft.localPosition = new Vector2(topLeft.x - gap.x, topLeft.y + gap.y);
        this.botRight.localPosition = new Vector2(botRight.x + gap.x, botRight.y - gap.y);
    }

    private void SetDotVertices(int pointIndex)
    {
        float dotSize = manager.dotSize;
        
        if (dotState[pointIndex] == DotState.INACTIVE)
            dotSize = 0;

        int vertexIndex = pointIndex * 4;

        dotVertices[vertexIndex + 0].x = points[pointIndex].x - dotSize;
        dotVertices[vertexIndex + 0].y = points[pointIndex].y - dotSize;

        dotVertices[vertexIndex + 1].x = points[pointIndex].x - dotSize;
        dotVertices[vertexIndex + 1].y = points[pointIndex].y + dotSize;

        dotVertices[vertexIndex + 2].x = points[pointIndex].x + dotSize;
        dotVertices[vertexIndex + 2].y = points[pointIndex].y + dotSize;

        dotVertices[vertexIndex + 3].x = points[pointIndex].x + dotSize;
        dotVertices[vertexIndex + 3].y = points[pointIndex].y - dotSize;

        dotMeshManipulated = true;

        SetTrailVertices(pointIndex);
    }

    private void SetTrailVertices(int pointIndex)
    {
        float trailSize = manager.trailSize;

        if (dotState[pointIndex] == DotState.INACTIVE)
            trailSize = 0;

        int vertexIndex = pointIndex * 3;

        Vector3 dir = (trailPoints[pointIndex] - points[pointIndex]);

        dir.y = dir.y * -1;

        dir.Normalize();

        dir *= trailSize;

        trailVertices[vertexIndex + 0].x = points[pointIndex].x + dir.y * -1;
        trailVertices[vertexIndex + 0].y = points[pointIndex].y + dir.x * -1;

        trailVertices[vertexIndex + 1].x = points[pointIndex].x + dir.y;
        trailVertices[vertexIndex + 1].y = points[pointIndex].y + dir.x;

        trailMeshManipulated = true;
    }

    private void SetTrailPointVertex(int pointIndex)
    {
        int vertexIndex = pointIndex * 3;

        trailVertices[vertexIndex + 2] = trailPoints[pointIndex];

        trailMeshManipulated = true;
    }

    public void HeightReached(float height)
    {
        manager.ReturnShapeToPool(this);
    }
}
