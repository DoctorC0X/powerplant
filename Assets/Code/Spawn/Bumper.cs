﻿using UnityEngine;
using System.Collections;

public class Bumper : MonoBehaviour, HeightListener
{
    public float impulse;

	public void Init(float despawnHeight)
    {
        HeightCheckManager.Instance.AddRelativeHeightListener(this, despawnHeight);
	}

    void OnTriggerStay2D(Collider2D c)
    {
        Vector2 dir = (c.transform.position - transform.position).normalized;

        c.gameObject.GetComponent<PlantHead>().GiveAccImpulse(dir * impulse);
    }

    public void HeightReached(float height)
    {
        Destroy(this.gameObject);
    }
}
