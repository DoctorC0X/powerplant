﻿using UnityEngine;
using System.Collections;
using System;
using Uninspired.Core.Interpolation.Movement;
using EventSystem;

public class Critter : MonoBehaviour, HeightListener, PauseListener
{
    public enum State { INACTIVE, STAGE, PLANT, FLYING };
    public State currentState;

    private CritterSpawner spawner;

    private Animator animator;
    private SpriteRenderer sRenderer;
    private InterpolatedMovement movement;
    private CritterBubble bubble;

    public CritterData cData;

    private Collider2D trigger;
    private GameObject despawnTarget;

    public bool isDelivered;

    public int targetStageNr;
    public Stage targetStage;

    public CritterLandingSpot currentLandingSpot;

    public int passedStagesTillReturn;

    public float infoTimerMax;
    public float infoTimer;
    public bool infoTimerActive;

    private bool paused;

    void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        sRenderer = animator.GetComponent<SpriteRenderer>();
        trigger = GetComponentInChildren<Collider2D>();
        movement = GetComponent<InterpolatedMovement>();

        bubble = GetComponentInChildren<CritterBubble>();
        bubble.Init(this);
    }

    public void Init(CritterSpawner sp, CritterData cData, int target)
    {
        spawner = sp;

        gameObject.name = cData.name;

        this.cData = cData;
        
        animator.runtimeAnimatorController = cData.controller;
        animator.transform.localScale = cData.scale;

        sRenderer.flipX = cData.flipX;

        targetStageNr = target;
        currentState = State.STAGE;

        bubble.SetOutside();
        bubble.Alternate(bubble.ShowUp, bubble.ShowStageNr, 1);

        GameManager.Instance.pauseListener.Add(this);
        SetPause(GameManager.Instance.paused);
    }

    private void SetAnimatorToFlying(bool flying)
    {
        animator.SetBool("Flying", flying);
    }

    public void Update()
    {
        trigger.transform.position = new Vector3(0, transform.position.y, transform.position.z);
        trigger.transform.up = Vector3.up;

        if (infoTimerActive && !paused)
        {
            infoTimer -= Time.deltaTime;

            if (infoTimer <= 0)
            {
                float phaseDuration = 1;
                int phases = 4;

                bubble.Alternate(bubble.ShowPointAt, bubble.ShowStageNr, phaseDuration, phases);
                infoTimer = infoTimerMax + (phaseDuration * phases);
            }
        }
    }

    public void Reset()
    {
        cData = null;

        currentLandingSpot = null;

        targetStageNr = -1;

        bubble.Hide();
        infoTimerActive = false;

        if (despawnTarget != null)
            Destroy(despawnTarget);

        currentState = State.INACTIVE;
        isDelivered = false;

        GameManager.Instance.pauseListener.Remove(this);
    }

    public void ReturnToPool()
    {
        Reset();

        spawner.critterPool.Push(gameObject);
    }

    public void OnTriggerEnter2D(Collider2D c)
    {
        switch (currentState)
        {
            case State.PLANT:
                {
                    if (c.gameObject.layer == Layers.stage.Value)
                    {
                        Stage s = c.gameObject.GetComponent<Stage>();

                        OnStageEnter(s);
                    }

                    break;
                }
            case State.STAGE:
                {
                    if (c.gameObject.layer == Layers.plantHead.Value)
                    {
                        OnPlantEnter();
                    }

                    break;
                }
        }
    }

    public void OnTriggerExit2D(Collider2D c)
    {
        switch (currentState)
        {
            case State.PLANT:
                {
                    if (c.gameObject.layer == Layers.stage.Value)
                    {
                        Stage s = c.gameObject.GetComponent<Stage>();

                        OnStageExit(s);
                    }

                    break;
                }
            case State.STAGE:
                {
                    if (c.gameObject.layer == Layers.plantHead.Value)
                    {
                        OnPlantExit();
                    }

                    break;
                }
        }
    }

    private void OnStageEnter(Stage s)
    {
        if (s.Nr == targetStageNr)
        {
            FlipX();

            infoTimerActive = false;
            infoTimer = infoTimerMax;
            bubble.Hide();

            targetStage = s;
            BusButtonAddAction(FlyToTargetStage);
        }
    }

    private void OnStageExit(Stage s)
    {
        if (s.Nr == targetStageNr)
        {
            FlipX();

            infoTimerActive = true;

            BusButtonRemoveAction(FlyToTargetStage);
        }
    }

    private void OnPlantEnter()
    {
        if (!isDelivered)
        {
            BusButtonAddAction(FlyToPlant);
        }
    }

    private void OnPlantExit()
    {
        if (!isDelivered)
        {
            BusButtonRemoveAction(FlyToPlant);
        }
    }

    private void BusButtonAddAction(Action a)
    {
        FollowCanvas.Instance.busButton.SetInteractable(true);
        FollowCanvas.Instance.busButton.OnButtonDown += a;
    }

    private void BusButtonRemoveAction(Action a)
    {
        FollowCanvas.Instance.busButton.SetInteractable(false);
        FollowCanvas.Instance.busButton.GetComponent<BasicButton>().OnButtonDown -= a;
    }

    private void FlyToTargetStage()
    {
        BusButtonRemoveAction(FlyToTargetStage);

        infoTimerActive = false;

        HeightCheckManager.Instance.RemoveListener(this, CalcDespawnTriggerHeight());
        EventManager.Instance.RemoveHandler(EventData.ID.TRAFFICZONE_ENTER, FlyToOffscreenDespawn);

        CritterLandingSpot freeSpot = targetStage.GetFreeLandingSpot();
        freeSpot.Reserve();
        FlyTo(freeSpot.transform, () => AttachToTargetStage(freeSpot));
    }

    private void FlyToPlant()
    {
        LandingSpot spot = PlantModel.MainPlant.landingPort.GetFreeLandingSpot();

        if (spot != null)
        {
            spot.Reserve();

            OnPlantExit();

            FlyTo(spot.transform, () => AttachToPlant(spot));
        }
    }

    private void FlyToOffscreenDespawn()
    {
        if (currentState == State.PLANT)
        {
            despawnTarget = new GameObject(gameObject.name + "_despawnTarget");

            Vector3 pos = transform.position;
            pos.x = FollowCanvas.Instance.topLeft.position.x - 4;

            despawnTarget.transform.position = pos;

            FlipX();

            FlyTo(despawnTarget.transform, ReturnToPool);
        }
    }

    private void FlyTo(Transform target, Action onReached)
    {
        currentLandingSpot.Detach();

        transform.parent = null;
        transform.up = Vector3.up;

        bubble.Hide();

        SetAnimatorToFlying(true);

        movement.Target = new IMTarget(target);
        movement.onReachedOnce += onReached;
        movement.Begin();

        currentState = State.FLYING;
    }

    private void AttachToTargetStage(CritterLandingSpot spot)
    {
        AttachTo(spot);

        bubble.SetInside();
        bubble.ShowLove(2);

        currentState = State.STAGE;

        StarDustRocketLauncher.Instance.transform.position = transform.position;
        StarDustRocketLauncher.Instance.Fire();

        isDelivered = true;
        EventManager.Instance.CallEvent(EventData.ID.CRITTER_DELIVERED, new EventData.ParamCritter() { increase = 1, critter = this });
    }

    private void AttachToPlant(LandingSpot spot)
    {
        AttachTo(spot);

        infoTimerActive = true;
        infoTimer = infoTimerMax;

        HeightCheckManager.Instance.AddWorldHeightListener(this, CalcDespawnTriggerHeight());
        EventManager.Instance.AddHandler(EventData.ID.TRAFFICZONE_ENTER, FlyToOffscreenDespawn);

        bubble.SetInside();
        bubble.ShowSmile(2);

        currentState = State.PLANT;
    }

    private void AttachTo(CritterLandingSpot cls)
    {
        cls.Attach(this);

        SetAnimatorToFlying(false);
    }

    private void FlipX()
    {
        sRenderer.flipX = !sRenderer.flipX;
    }

    private float CalcDespawnTriggerHeight()
    {
        return StageSpawner.Instance.spawnHeight * (targetStageNr + passedStagesTillReturn);
    }

    public void HeightReached(float height)
    {
        if (currentState == State.PLANT)
        {
            FlyToOffscreenDespawn();
        }
    }

    public void SetPause(bool pause)
    {
        if (paused != pause)
        {
            paused = pause;

            if (paused)
                movement.Pause();
            else
                movement.Unpause();
        }
    }

    public bool IsPaused()
    {
        return paused;
    }
}
