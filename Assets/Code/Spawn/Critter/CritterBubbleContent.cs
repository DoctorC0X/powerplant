﻿using UnityEngine;
using System.Collections;

public interface CritterBubbleContent 
{
    bool IsShowing();
    void Hide();
    void Show();
}
