﻿using UnityEngine;
using System.Collections;

public class CritterBubbleSprite : MonoBehaviour, CritterBubbleContent 
{
    private SpriteRenderer sRenderer;

    void Awake()
    {
        sRenderer = GetComponent<SpriteRenderer>();
    }

    public bool IsShowing()
    {
        return sRenderer.enabled;
    }

    public void Hide()
    {
        sRenderer.enabled = false;
    }

    public void Show()
    {
        sRenderer.enabled = true;
    }

    public void SetSprite(Sprite s, Color c)
    {
        sRenderer.sprite = s;
        sRenderer.color = c;
        transform.localRotation = Quaternion.identity;
    }

    public void SetSprite(Sprite s)
    {
        SetSprite(s, sRenderer.color);
    }
}
