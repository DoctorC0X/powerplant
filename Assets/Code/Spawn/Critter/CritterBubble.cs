﻿using UnityEngine;
using System.Collections;
using System;

public class CritterBubble : MonoBehaviour 
{
    private Critter critter;

    public SpriteRenderer bubble;

    public GameObject content;

    private CritterBubbleSprite sprite;
    private CritterBubbleText text;

    public Sprite insideBubble;
    public Sprite outsideBubble;

    public Sprite up;
    public Sprite smile;
    public Sprite love;

    public Vector2 insidePos;
    public Vector2 outsidePos;

    private bool alternating;
    private float alternateDuration;
    private int alternateCount;
    private Action alternateAction;

    private Vector3 pointContentAt;
    private bool pointing;

    private bool showing;
    private float showingDuration;

    void Awake()
    {
        sprite = content.GetComponentInChildren<CritterBubbleSprite>();
        text = content.GetComponentInChildren<CritterBubbleText>();
    }

    public void Init(Critter c)
    {
        critter = c;
    }

    void Update()
    {
        if (pointing)
            content.transform.up = pointContentAt - content.transform.position;

        if (showing && showingDuration > 0)
        {
            showingDuration -= Time.deltaTime;

            if (showingDuration < 0)
            {
                if (alternating)
                {
                    alternateAction();
                }
                else
                {
                    Hide();
                }
            }
        }
    }

    public void Hide()
    {
        bubble.enabled = false;
        sprite.Hide();
        text.Hide();

        alternating = false;
        alternateDuration = 0;

        showing = false;
        showingDuration = 0;
    }

    public void ShowStageNr(float duration = 0)
    {
        StopPointing();
        ShowText(critter.targetStageNr.ToString(), Color.black, duration);
    }

    public void ShowSmile(float duration = 0)
    {
        StopPointing();
        ShowSprite(smile, Color.white, duration);
    }

    public void ShowLove(float duration = 0)
    {
        StopPointing();
        ShowSprite(love, Color.white, duration);
    }

    public void ShowUp(float duration = 0)
    {
        StopPointing();
        ShowSprite(up, Color.blue, duration);
    }

    public void ShowPointAt(float duration = 0)
    {
        PointAt(new Vector3(-7, StageSpawner.Instance.GetStageHeight(critter.targetStageNr), content.transform.position.z));
        ShowSprite(up, Color.blue, duration);
    }

    private void ShowSprite(Sprite s, Color c, float duration = 0)
    {
        text.Hide();

        sprite.SetSprite(s, c);
        sprite.Show();
        bubble.enabled = true;

        showing = true;
        showingDuration = duration;
    }

    private void ShowText(string t, Color c, float duration = 0)
    {
        sprite.Hide();

        text.SetText(t, c);
        text.Show();
        bubble.enabled = true;

        showing = true;
        showingDuration = duration;
    }

    public void Alternate(Action<float> first, Action<float> second, float duration, int count = 0)
    {
        alternateDuration = duration;
        alternateCount = count;
        alternating = true;

        Alternate(first, second);
    }

    private void Alternate(Action<float> first, Action<float> second)
    {
        if (alternateCount == 1)
            alternating = false;
        else if(alternateCount != 0)
            alternateCount--;

        first(alternateDuration);

        alternateAction = () => Alternate(second, first);
    }

    private void PointAt(Vector3 target)
    {
        pointContentAt = target;
        pointing = true;
    }

    private void StopPointing()
    {
        content.transform.localRotation = Quaternion.identity;
        pointing = false;
    }

    public void SetInside()
    {
        bubble.sprite = insideBubble;
        transform.localPosition = new Vector3(insidePos.x, insidePos.y, transform.localPosition.z);
    }

    public void SetOutside()
    {
        bubble.sprite = outsideBubble;
        transform.localPosition = new Vector3(outsidePos.x, outsidePos.y, transform.localPosition.z);
    }
}
