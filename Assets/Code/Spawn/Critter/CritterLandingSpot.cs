﻿using UnityEngine;
using System.Collections;

public class CritterLandingSpot : MonoBehaviour
{
    public Critter attached;

    private bool reserved;

    public virtual bool IsOccupied()
    {
        return attached != null;
    }

    public virtual bool IsReserved()
    {
        return reserved;
    }

    public virtual void Attach(Critter c)
    {
        attached = c;
        attached.currentLandingSpot = this;

        c.transform.parent = transform;
        c.transform.localPosition = Vector3.zero;
        c.transform.localRotation = Quaternion.identity;
    }

    public virtual void Detach()
    {
        attached.currentLandingSpot = null;
        attached = null;

        reserved = false;
    }

    public virtual void Reserve()
    {
        reserved = true;
    }
}
