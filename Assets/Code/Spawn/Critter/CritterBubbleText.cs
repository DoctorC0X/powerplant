﻿using UnityEngine;
using System.Collections;

public class CritterBubbleText : MonoBehaviour, CritterBubbleContent 
{
    private MeshRenderer mRenderer;
    private TextMesh text;

    void Awake()
    {
        mRenderer = GetComponent<MeshRenderer>();
        text = GetComponent<TextMesh>();
    }

    public bool IsShowing()
    {
        return mRenderer.enabled;
    }

    public void Hide()
    {
        mRenderer.enabled = false;
    }

    public void Show()
    {
        mRenderer.enabled = true;
    }

    public void SetText(string t, Color c)
    {
        text.text = t;
        text.color = c;
    }

    public void SetText(string t)
    {
        SetText(t, text.color);
    }
}
