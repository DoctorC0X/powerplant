﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Uninspired.Core.Pooling;
using EventSystem;

public class CritterSpawner : MonoBehaviour
{
    private static CritterSpawner _instance;

    public static CritterSpawner Instance
    {
        get { return _instance; }
    }

    public GameObjectPool critterPool;

    public int currentSector;
    public bool increaseSector;
    public int changeSectorHeight;

    public CritterData[] critterDataAll;
    public List<List<CritterData>> critterDataInSector;

    public int critterStageTravelDistanceMin;
    public int critterStageTravelDistanceMax;

    public List<Critter> activeCritter;
    public int activeCritterMax;
    public float spawnChance;

    public bool activated;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            DestroyImmediate(this.gameObject);
            return;
        }
        else
        {
            _instance = this;
        }

        critterPool.Init(this);
        critterPool.OnPush += (GameObject go) => activeCritter.Remove(go.GetComponent<Critter>());

        currentSector = 0;
        critterDataInSector = new List<List<CritterData>>();

        foreach (CritterData current in critterDataAll)
        {
            if (current.sector >= critterDataInSector.Count)
                critterDataInSector.Add(new List<CritterData>());

            CritterData newData = ScriptableObject.Instantiate<CritterData>(current);

            newData.name = newData.name.Split('(')[0];

            critterDataInSector[current.sector].Add(newData);
        }

        activeCritter = new List<Critter>(activeCritterMax);
    }

	// Use this for initialization
    private void Start() 
    {
        if (StageSpawner.Instance == null || !StageSpawner.Instance.activated)
            activated = false;
        else
            StageSpawner.Instance.OnStageSpawn += OnStageSpawn;

        EventManager.Instance.AddHandler(EventData.ID.PIECES, OnGrowth);
	}

    public void OnStageSpawn(Stage stage)
    {
        if (activated && PlantModel.MainPlant.landingPort.GetFreeLandingSpotCount() > 0 && activeCritter.Count < activeCritterMax && Random.value <= spawnChance)
        {
            Spawn(stage);
        }
    }

    private void Spawn(CritterData cData, Stage stage)
    {
        Critter critter = critterPool.Pop().GetComponent<Critter>();

        int targetStage = stage.Nr + Random.Range(critterStageTravelDistanceMin, critterStageTravelDistanceMax);
        critter.Init(this, cData, targetStage);

        stage.GetFreeLandingSpot().Attach(critter);

        activeCritter.Add(critter);
    }

    private void Spawn(Stage targetStage)
    {
        Spawn(GetRandomCritterDataInSector(currentSector), targetStage);
    }

    public void OnGrowth()
    {
        // check sector change
        if (increaseSector && PlantModel.MainPlant.plantHead.transform.position.y > changeSectorHeight)
        {
            changeSectorHeight *= 2;

            currentSector++;

            if (currentSector + 1 >= critterDataInSector.Count)
            {
                increaseSector = false;
            }            
        }
    }

    private CritterData GetRandomCritterDataInSector(int s)
    {
        int randInt = Random.Range(0, critterDataInSector[s].Count);

        return critterDataInSector[s][randInt];
    }

    private CritterData GetRandomCritterData()
    {
        return critterDataAll[Random.Range(0, critterDataAll.Length)];
    }

    public int[] GetTargetStages()
    {
        int[] stages = new int[activeCritter.Count];

        for (int i = 0; i < activeCritter.Count; i++)
        {
            stages[i] = activeCritter[i].targetStageNr;
        }

        return stages;
    }
}
