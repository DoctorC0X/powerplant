﻿using UnityEngine;
using System.Collections;
using System;

public class CritterData : ScriptableObject
{
    public enum Type
    {
        LIBELLE,
        KAEFER,
        HUMMEL,
        ROTKEHLCHEN,
        RABE,
        HUHN,
        ALIEN_KLEIN,
        ALIEN_MITTEL,
        ALIEN_DICK,
    }

    public Type type;

    public int sector;

    public float speed;

    public RuntimeAnimatorController controller;

    public Vector3 scale;

    public bool flipX;
}
