﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CloudSpawner : HeightSpawner
{
    private GameObject cloudParent;

    public GameObject cloudPrefab;

    public List<Sprite> cloudSprites;

    public Vector2 windForceRange;
    public Vector2 distanceScaleRange;

    public float cloudZ;
    public float borderX;

    public float cloudDespawnHeight;

    void Awake()
    {
        cloudParent = new GameObject("Cloud Parent");
        cloudParent.transform.parent = transform;
        cloudParent.transform.localPosition = Vector3.zero;
    }

    public override void Spawn(Vector2 pos)
    {
        GameObject newCloud = Instantiate(cloudPrefab) as GameObject;

        newCloud.transform.parent = cloudParent.transform;
        newCloud.transform.position = new Vector3(pos.x, pos.y, cloudZ);

        newCloud.GetComponent<SpriteRenderer>().sprite = cloudSprites[Random.Range(0, cloudSprites.Count - 1)];

        newCloud.GetComponent<WindVictim>().maxAcceleration = Random.Range(windForceRange.x, windForceRange.y);

        newCloud.GetComponent<Cloud>().Init(this, Random.Range(distanceScaleRange.x, distanceScaleRange.y), cloudDespawnHeight + spawnOffsetMax.y);

        //return newCloud;
    }    
}
