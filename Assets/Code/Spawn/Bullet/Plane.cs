﻿using UnityEngine;
using System.Collections;
using Uninspired.Core.Interpolation.Movement;

public class Plane : Bullet 
{
    public override void Init(Vector3 bStart, Vector3 bEnd, int side)
    {
        base.Init(bStart, bEnd, side);

        type = Type.PLANE;

        audioSource.Play();
    }

    public override void Terminate()
    {
        base.Terminate();

        spawner.planePool.Push(gameObject);
    }

    protected override int CalcHitImpulseDirFactor(Collision2D c)
    {
        // is the bullet left or right?
        int currentSide = (int)Mathf.Sign(transform.localPosition.x);

        // the checkpoint for directions
        float colliderSize = GetComponent<BoxCollider2D>().bounds.extents.x;

        return (c.transform.position.x > transform.position.x + (currentSide * colliderSize * -1)) ? 1 : -1;
    }

    public override void TranslateStartPoint(float amount)
    {
        movement.startPos += new Vector3(0, amount, 0);
    }

    public override void TranslateEndPoint(float amount)
    {
        movement.Target = new IMTarget(movement.Target.GetTargetPosition() + new Vector3(0, amount, 0));
    }
}
