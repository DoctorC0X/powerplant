﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class BulletWarning : MonoBehaviour 
{
    private BulletSpawner spawner;

    public RectTransform rectT;
    public Image background;

    public Image[] arrows;

    public Color backgroundColorStart;
    public Color backgroundColorEnd;

    public Color arrowColorStart;
    public Color arrowColorEnd;

    public BulletSpawner.FlightDir dir;
    public float Side
    {
        get { return background.transform.localScale.x; }
        set
        {
            Vector3 localScale = background.transform.localScale;
            background.transform.localScale = new Vector3(value, value, localScale.z);
        }
    }

    public float areaSize;

    private Vector3 stayPosition;

    private bool _stay;
    public bool Stay
    {
        get { return _stay; }
        set
        {
            _stay = value;

            stayPosition = transform.position;
        }
    }

    private float warningDuration;
    private float passedTime;
    private bool initialized;

    public float timeOffset;

    public void Init(BulletSpawner sp, BulletSpawner.FlightDir newDir, float warningDuration, float side)
    {
        spawner = sp;
        Stay = false;

        transform.SetParent(FollowCanvas.Instance.backgroundCanvas.transform, false);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;

        SetDir(newDir, side);

        initialized = true;
        passedTime = 0;

        this.warningDuration = warningDuration + timeOffset;
    }

    void Update()
    {
        if (Stay)
        {
            transform.position = stayPosition;
        }

        if (initialized && !GameManager.Instance.paused)
        {
            passedTime += Time.deltaTime;

            float fraction = passedTime / warningDuration;

            background.color = Color.Lerp(backgroundColorStart, backgroundColorEnd, fraction);

            for (int i = 0; i < arrows.Length; i++)
            {
                arrows[i].color = Color.Lerp(arrowColorStart, arrowColorEnd, fraction);
            }

            if (fraction >= 1)
            {
                initialized = false;

                spawner.warningPool.Push(gameObject);
            }
        }
    }

    public void SetDir(BulletSpawner.FlightDir newDir, float newSide)
    {
        Side = newSide;

        if (newDir == BulletSpawner.FlightDir.HORIZONTAL)
        {
            transform.localRotation = Quaternion.identity;

            rectT.sizeDelta = new Vector2(FollowCanvas.Instance.GetComponent<RectTransform>().sizeDelta.x, areaSize);
        }
        else
        {
            transform.localEulerAngles = new Vector3(0, 0, 90);

            rectT.sizeDelta = new Vector2(FollowCanvas.Instance.GetComponent<RectTransform>().sizeDelta.y, areaSize);
        }

        dir = newDir;

        for (int i = 0; i < arrows.Length; i++)
        {
            Vector3 scale = arrows[i].transform.localScale;

            arrows[i].transform.localScale = new Vector3(Mathf.Abs(scale.x) * newSide, scale.y, scale.z); 
        }
    }

    public void SetPos(float x, float y)
    {
        transform.position = new Vector3(x, y, transform.position.z);
    }
}
