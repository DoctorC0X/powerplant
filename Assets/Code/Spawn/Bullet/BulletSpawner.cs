﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Uninspired.Core.Pooling;

public class BulletSpawner : MonoBehaviour, HeightListener
{
    private static BulletSpawner _instance;

    public static BulletSpawner Instance
    {
        get { return _instance; }
    }

    public PlantHead head;

    public enum FlightDir { HORIZONTAL = 0, VERTICAL = 1 };

    public GameObjectPool warningPool;
    public GameObjectPool rocketPool;
    public GameObjectPool planePool;

    public float warningDuration;
    public List<GameObject> activeBullets;

    public float spawnAreaGap;
    private Vector2 spawnArea;

    public Vector2 spawnTimerRange;

    public float spawnTimerRangeDecreaseHeight;
    public Vector2 spawnTimerRangeDecrease;

    private float spawnTimerMultiplier;
    private float spawnTimer;

    public float bulletDelay;
    public Vector2 horizontalYOffsetRange;

    public float bulletZ;

    public float closeDodgeDistance;

    public float playerImpulse;

    public float minSpawnHeight;

    public bool activated;
    private bool canHitPlayer = true;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            _instance = this;
        }

        activeBullets = new List<GameObject>();
        spawnTimerMultiplier = 1;

        warningPool.Init(this);
        rocketPool.Init(this);
        planePool.Init(this);

        rocketPool.OnPop += OnBulletPop;
        planePool.OnPop += OnBulletPop;

        rocketPool.OnPush += OnBulletPush;
        planePool.OnPush += OnBulletPush;

        if (!Application.isEditor)
            canHitPlayer = true;
    }

    void Start()
    {
        head = PlantModel.MainPlant.plantHead;

        FollowCanvas canvas = FollowCanvas.Instance;

        spawnArea = new Vector2(canvas.botRight.transform.position.x - spawnAreaGap, canvas.topLeft.transform.position.y - spawnAreaGap);

        HeightCheckManager.Instance.AddWorldHeightListener(this, spawnTimerRangeDecreaseHeight);

        SetSpawnTimer();
    }

    void Update()
    {
        if (activated && !GameManager.Instance.paused && head.transform.position.y > minSpawnHeight)
        {
            spawnTimer -= Time.deltaTime;

            if (spawnTimer <= 0)
            {
                Spawn();

                SetSpawnTimer();
            }
        }
    }

    public GameObject Spawn(FlightDir dir, int side, bool useHorizontalYOffset = true)
    {
        GameObject bullet = null;

        BulletWarning warning = warningPool.Pop().GetComponent<BulletWarning>();

        warning.Init(this, dir, bulletDelay, side * -1);

        if (dir == FlightDir.HORIZONTAL)
        {
            bullet = SpawnPlane(side);

            warning.SetPos(0, bullet.transform.position.y); ;
            warning.Stay = true;
        }
        else
        {
            bullet = SpawnRocket(side);

            warning.SetPos(bullet.transform.position.x, FollowCanvas.Instance.transform.position.y);
        }

        return bullet;
    }

    private GameObject SpawnPlane(int side)
    {
        GameObject bullet = planePool.Pop();
        Bullet bulletScript = bullet.GetComponent<Bullet>();

        float xPos = (spawnArea.x + (bulletDelay * bulletScript.standardSpeed)) * side;
        float yPos = head.transform.position.y;

        yPos += UnityEngine.Random.Range(horizontalYOffsetRange[0], horizontalYOffsetRange[1]);

        Vector3 bulletStart = new Vector3(xPos, yPos, bulletZ);
        Vector3 bulletEnd = new Vector3(xPos * -1, yPos, bulletZ);

        bulletScript.Init(bulletStart, bulletEnd, side);

        return bullet;
    }

    private GameObject SpawnRocket(int side)
    {
        GameObject bullet = rocketPool.Pop();
        Bullet bulletScript = bullet.GetComponent<Bullet>();

        float xPos = head.transform.position.x;
        float yOffset = (spawnArea.y + (bulletDelay * bulletScript.standardSpeed)) * side;

        Vector3 bulletStart = new Vector3(xPos, head.transform.position.y + yOffset, bulletZ);
        Vector3 bulletEnd = new Vector3(xPos, head.transform.position.y + (yOffset * -1), bulletZ);

        bulletScript.Init(bulletStart, bulletEnd, side);

        return bullet;
    }

    public GameObject Spawn(FlightDir dir)
    {
        int side = UnityEngine.Random.value >= 0.5f ? 1 : -1;

        return Spawn(dir, side);
    }

    public GameObject Spawn(int side)
    {
        FlightDir dir = FlightDir.HORIZONTAL;

        if(PlantModel.MainPlant.plantFace.visibility.currentlyVisible)
            dir = UnityEngine.Random.value >= 0.5f ? FlightDir.HORIZONTAL : FlightDir.VERTICAL;

        return Spawn(dir, side);
    }

    public GameObject Spawn()
    {
        int side = UnityEngine.Random.value >= 0.5f ? 1 : -1;

        return Spawn(side);
    }

    public void SetSpawnTimer()
    {
        spawnTimer = UnityEngine.Random.Range(spawnTimerRange[0], spawnTimerRange[1]) * spawnTimerMultiplier;
    }

    public void OnBulletPop(GameObject go)
    {
        activeBullets.Add(go);
    }

    public void OnBulletPush(GameObject go)
    {
        if (activeBullets.Contains(go))
            activeBullets.Remove(go);
    }

    public void ToggleCanHitPlayer()
    {
        canHitPlayer = !canHitPlayer;
    }

    public bool CanHitPlayer()
    {
        return canHitPlayer;
    }

    public void HeightReached(float height)
    {
        spawnTimerRange -= spawnTimerRangeDecrease;

        if (spawnTimerRange.x - spawnTimerRangeDecrease.x > 0 && spawnTimerRange.y - spawnTimerRangeDecrease.y > 0)
        {
            HeightCheckManager.Instance.AddWorldHeightListener(this, height + spawnTimerRangeDecreaseHeight);
        }
    }

    public void SetSpawnTimerMultiplier(float multiplier)
    {
        spawnTimerMultiplier = multiplier;
    }
}
