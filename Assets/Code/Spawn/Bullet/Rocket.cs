﻿using UnityEngine;
using System.Collections;
using Uninspired.Core.Interpolation.Movement;

public class Rocket : Bullet
{
    public override void Init(Vector3 bStart, Vector3 bEnd, int side)
    {
        base.Init(bStart, bEnd, side);

        type = Type.ROCKET;

        audioSource.PlayDelayed(spawner.bulletDelay);
    }

    public override void Terminate()
    {
        base.Terminate();

        spawner.rocketPool.Push(gameObject);
    }

    protected override int CalcHitImpulseDirFactor(Collision2D c)
    {
        return (int)Mathf.Sign((c.transform.position - transform.position).x);
    }

    public override void TranslateStartPoint(float amount)
    {
        movement.startPos += new Vector3(amount, 0, 0);
    }

    public override void TranslateEndPoint(float amount)
    {
        movement.Target = new IMTarget(movement.Target.GetTargetPosition() + new Vector3(amount, 0, 0));
    }
}
