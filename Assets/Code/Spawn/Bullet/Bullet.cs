﻿using UnityEngine;
using System.Collections;
using Uninspired.Core.Interpolation.Movement;
using EventSystem;

public abstract class Bullet : MonoBehaviour, PauseListener
{
    public enum Type
    {
        PLANE = 1,
        ROCKET = 2,
    }

    public Type type;

    protected BulletSpawner spawner;
    public InterpolatedMovement movement;

    public GameObject target;

    public SpriteRenderer sRenderer;
    public AudioSource audioSource;

    public float standardSpeed;

    protected bool hit;
    protected bool dodged;
    public int side;

    private bool paused;

    public virtual void Init(Vector3 bStart, Vector3 bEnd, int side)
    {
        spawner = BulletSpawner.Instance;

        SetFlightValues(bStart, bEnd);

        GameManager.Instance.pauseListener.Add(this);
        SetPause(GameManager.Instance.paused);

        this.side = side;

        sRenderer.flipX = (side == -1);

        // for debugging
        foreach (BoxCollider2D current in GetComponents<BoxCollider2D>())
        {
            if(!current.isTrigger)
                current.enabled = spawner.CanHitPlayer();
        }
    }

    public virtual void Terminate()
    {
        Reset();
    }

    public void OnCollisionEnter2D(Collision2D c)
    {
        if (!hit && !PlantModel.MainPlant.Broken)
        {
            hit = true;

            // plane dir factor has some edge cases
            int dirFactor = CalcHitImpulseDirFactor(c);

            PlantModel.MainPlant.BulletHit(new Vector2(dirFactor * spawner.playerImpulse, 0));

            EventManager.Instance.CallEvent(EventData.ID.BULLET_HIT, new EventData.ParamBullet() { increase = 1, bullet = this });

            //c.gameObject.GetComponent<PlantHead>().GiveAccImpulse(new Vector2(dirFactor * spawner.playerImpulse, 0), false);
            //PlantModel.MainPlant.plantHead.GiveArcAccImpulse(spawner.playerImpulse, dirFactor, false);
            //c.gameObject.GetComponent<PlantHead>().plantModel.Stun(spawner.playerStunTime);
        }
    }

    protected abstract int CalcHitImpulseDirFactor(Collision2D c);

    public void OnTriggerExit2D(Collider2D c)
    {
        if (!hit && !dodged && !PlantModel.MainPlant.Broken)
        {
            EventManager.Instance.CallEvent(EventData.ID.BULLET_DODGE, new EventData.ParamBullet() { increase = 1, bullet = this });
            dodged = true;
        }
    }

    public void SetFlightValues(Vector3 s, Vector3 e)
    {
        transform.position = s;

        target = new GameObject(gameObject.name + " target");

        if (type == Type.PLANE)
            target.transform.parent = spawner.transform;
        else
            target.transform.parent = Camera.main.transform;

        target.transform.position = e;

        movement.Init(target.transform, IMMotor.MotorMode.SPEED, standardSpeed);
        movement.onReachedOnce += Terminate;
        movement.Begin();
    }

    public abstract void TranslateStartPoint(float amount);

    public abstract void TranslateEndPoint(float amount);

    public void Reset()
    {
        hit = false;
        dodged = false;

        GameManager.Instance.pauseListener.Remove(this);
        side = 0;
        Destroy(target);
    }

    public void SetPause(bool pause)
    {
        paused = pause;

        movement.paused = paused;
    }

    public bool IsPaused()
    {
        return paused;
    }
}
