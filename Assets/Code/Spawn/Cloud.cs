﻿using UnityEngine;
using System.Collections;

public class Cloud : MonoBehaviour, HeightListener
{
    public CloudSpawner spawner;

    private GameObject mainCam;
    private float oldCamY;

    public float distanceScale;

    public void Init(CloudSpawner spawner, float distanceScale, float despawnHeight)
    {
        this.spawner = spawner;
        this.distanceScale = distanceScale;

        mainCam = Camera.main.gameObject;
        oldCamY = mainCam.transform.position.y;

        HeightCheckManager.Instance.AddRelativeHeightListener(this, despawnHeight);
    }

	// Update is called once per frame
	void Update () 
    {
        // 
        if (this.transform.position.x > spawner.borderX)
        {
            this.transform.position = new Vector3(-spawner.borderX, this.transform.position.y, this.transform.position.z);
        }
        else if (this.transform.position.x < -spawner.borderX)
        {
            this.transform.position = new Vector3(spawner.borderX, this.transform.position.y, this.transform.position.z);
        }

        this.transform.Translate(0, (mainCam.transform.position.y - oldCamY) * -1 * distanceScale, 0);
        oldCamY = mainCam.transform.position.y;
	}

    public void HeightReached(float height)
    {
        Destroy(gameObject);
    }
}
