﻿using UnityEngine;
using System.Collections;
using Uninspired.Core.Interpolation.Movement;
using EventSystem;

public class StarPiece : MonoBehaviour 
{
    public StarPieceSpawner manager;

    public bool collected;

    public float flightDuration;
    public AnimationCurve flightCurve;

    public void Init(StarPieceSpawner spm, Sprite s)
    {
        manager = spm;
        GetComponent<SpriteRenderer>().sprite = s;
    }

    public void Reset()
    {
        collected = false;
    }

    void Update()
    {
        if (collected)
        {
            transform.rotation = Quaternion.identity;
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (!collected)
        {
            collected = true;

            AudioSources.Instance.starPieceCollect.Play();

            InterpolatedMovement starPieceMovement = GetComponent<InterpolatedMovement>();
            starPieceMovement.Target.Transform = FollowCanvas.Instance.display.starShape.transform;

            starPieceMovement.onReached_DestroyThis = true;
            starPieceMovement.onReached_SetTargetAsParent = true;
            
            starPieceMovement.Begin();

            manager.StarPieceCollected();

            EventManager.Instance.CallEvent(EventData.ID.STARPIECE_COLLECTED);
        }
    }
}
