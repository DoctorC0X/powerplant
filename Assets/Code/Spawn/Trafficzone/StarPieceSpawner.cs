﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Uninspired.Core.Pooling;

public class StarPieceSpawner : MonoBehaviour 
{
    private static StarPieceSpawner _instance;

    public static StarPieceSpawner Instance
    {
        get { return _instance; }
    }

    private List<StarPiece> starPieces;
    private int collectedPieces;
    private StarPiece currentStarPiece;

    public GameObjectPool starPiecesPool;
    
    public Sprite[] starPieceSprites;
    public Vector2 starPieceXRange;
    public float starPieceZ;

    public int starBonusAmount;

    public GameObject starPieceIndicatorPrefab;
    private FreeIndicator starPieceIndicator;
    private GameObject starShape;

    public int starDustDummysAmount;
    public Vector2 starDustDummysSpawnOffset;
    public Vector2 starDustDummysDurationRange;

    private float startHeight;
    private float endHeight;

    public bool activated;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
            return;
        }
        else
        {
            _instance = this;
        }
    }

	void Start () 
    {
        starPiecesPool.Init(this);

        starShape = FollowCanvas.Instance.display.starShape;

        CreateStarPieceIndicator();
	}

    public void Activate(float startH, float endH)
    {
        if (!activated)
        {
            startHeight = startH;
            endHeight = endH;

            collectedPieces = 0;
            starPieces = new List<StarPiece>();

            starPieceIndicator.GetComponentInChildren<SpriteRenderer>().color = Color.yellow;

            FollowCanvas.Instance.display.Open();

            SpawnNext();

            activated = true;
        }
    }

	void Update () 
    {
        if (starPieceIndicator != null && collectedPieces >= starPiecesPool.poolSize)
            starPieceIndicator.transform.up = Vector3.up;
	}

    private void CreateStarPieceIndicator()
    {
        GameObject starPieceIndicatorGO = Instantiate(starPieceIndicatorPrefab) as GameObject;

        starPieceIndicator = starPieceIndicatorGO.GetComponent<FreeIndicator>();

        starPieceIndicatorGO.transform.parent = PlantModel.MainPlant.plantFace.transform;
        starPieceIndicatorGO.transform.localPosition = Vector3.zero;
        starPieceIndicatorGO.SetActive(false);
    }

    private void SpawnNext()
    {
        StarPiece starPiece = starPiecesPool.Pop().GetComponent<StarPiece>();

        starPiece.transform.parent = transform;
        starPiece.transform.position = new Vector3(Random.Range(starPieceXRange[0], starPieceXRange[1]), Random.Range(startHeight, endHeight), starPieceZ);

        starPieceIndicator.target = starPiece.transform;

        currentStarPiece = starPiece;

        starPieces.Add(starPiece);

        starPiece.Init(this, starPieceSprites[collectedPieces]);
    }

    public void StarPieceCollected()
    {
        collectedPieces++;

        if (collectedPieces < starPiecesPool.poolSize)
        {
            SpawnNext();
        }
        else
        {
            starPieceIndicator.target = null;
            starPieceIndicator.GetComponentInChildren<SpriteRenderer>().color = Color.green;
        }
    }

    public void SetIndicatorActive(bool active)
    {
        starPieceIndicator.gameObject.SetActive(active);
    }

    public void SetShapeActive(bool active)
    {
        starShape.SetActive(active);
    }

    public bool AllPiecesCollected()
    {
        return collectedPieces >= starPiecesPool.poolSize;
    }

    public void ClaimReward()
    {
        starShape.GetComponentInChildren<Animator>().SetTrigger("Trigger");

        StarDustManager.Instance.SpawnStarDustDummys(starDustDummysAmount, starShape.transform.position, StarDustCollectCounter.Instance.icon.transform, starDustDummysSpawnOffset, starDustDummysDurationRange);
        PlayerProfile.Instance.DepositStarDust(starBonusAmount);
    }

    public void Deactivate()
    {
        if (activated)
        {
            for (int i = 0; i < starPieces.Count; i++)
            {
                starPieces[i].Reset();
                starPiecesPool.Push(starPieces[i].gameObject);
            }

            starPieces.Clear();

            SetIndicatorActive(false);
            SetShapeActive(false);

            FollowCanvas.Instance.display.Close();

            activated = false;
        }
    }
}
