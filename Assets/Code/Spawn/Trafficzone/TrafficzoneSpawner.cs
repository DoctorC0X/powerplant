﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using EventSystem;

public class TrafficzoneSpawner : MonoBehaviour, HeightListener
{
    private static TrafficzoneSpawner _instance;

    public static TrafficzoneSpawner Instance
    {
        get { return _instance; }
    }

    public enum Side { TOP, BOTTOM };

    public float[] spawnHeights;
    public float[] heights;

    public float spawnGap;
    public float despawnGap;

    public float bulletSpawnTimeMultiplier;

    public GameObject beginMarker;
    public GameObject endMarker;
    public GameObject background;

    public GameObject groundCollider;
    public GameObject cloudBorder;

    public float currentStart;
    public float currentEnd;

    public bool spawned;
    public bool playerInside;
    public bool looted;
    public bool activated;

    private void Awake()
    {
        if (_instance != null & _instance != this)
        {
            Destroy(this);
            return;
        }
        else
        {
            _instance = this;
        }
    }

	private void Start () 
    {
        if(activated)
            SetSpawnTrigger(0);

        spawned = false;
	}

    private void SetSpawnTrigger(float currentHeight)
    {
        int randomIndex = UnityEngine.Random.Range(0, spawnHeights.Length);
        HeightCheckManager.Instance.AddWorldHeightListener(this, (currentHeight + spawnHeights[randomIndex]) - spawnGap);
    }
	
	private void Update () 
    {
        if (spawned)
        {
            UpdateZonePos();

            if (!playerInside)
            {
                if (PlantModel.MainPlant.plantHead.transform.position.y >= currentStart && 
                    PlantModel.MainPlant.plantHead.transform.position.y <= currentEnd)
                {
                    OnTrafficZoneEnter();
                }
            }
            else
            {
                if (PlantModel.MainPlant.plantHead.transform.position.y < currentStart)
                {
                    OnTrafficZoneExitBottom();
                }
                else if (PlantModel.MainPlant.plantHead.transform.position.y > currentEnd)
                {
                    OnTrafficZoneExitTop();
                }
            }
        }
	}

    public bool IsHeightInZone(float height)
    {
        if (!spawned)
            return false;

        if (height >= currentStart && height <= currentEnd)
            return true;

        return false;
    }

    public void HeightReached(float height)
    {
        if (!spawned)
        {
            float highestTargetStageHeight = GetHighestTargetStageHeight();

            if (highestTargetStageHeight < height)
            {
                SpawnTrafficZone(height);
            }
            else
            {
                HeightCheckManager.Instance.AddWorldHeightListener(this, height + StageSpawner.Instance.spawnHeight);
                CritterSpawner.Instance.activated = false;
            }
        }
        else
        {
            DespawnTrafficZone(height);
        }
    }

    private float GetHighestTargetStageHeight()
    {
        float highestHeight = 0;

        try
        {
            foreach (int stageNr in CritterSpawner.Instance.GetTargetStages())
            {
                float currentHeight = StageSpawner.Instance.GetStageHeight(stageNr);

                if (highestHeight < currentHeight)
                {
                    highestHeight = currentHeight;
                }
            }
        }
        catch (NullReferenceException e)
        {
            Debug.LogWarning(e.Message);
        }

        return highestHeight;
    }

    private void SpawnTrafficZone(float currentHeight)
    {
        SetAssetsActive(true);

        currentHeight += spawnGap;
        currentStart = currentHeight;

        int randomIndex = UnityEngine.Random.Range(0, heights.Length);
        currentEnd = currentStart + heights[randomIndex];

        SetCloudBorderHeight(currentEnd);

        UpdateZonePos();

        background.GetComponent<RectTransform>().sizeDelta =
            new Vector2(0, endMarker.GetComponent<RectTransform>().anchoredPosition.y - beginMarker.GetComponent<RectTransform>().anchoredPosition.y);

        RemoveStardustShapesInZone();

        StarPieceSpawner.Instance.Activate(currentStart, currentEnd);

        StarDustManager.Instance.activated = false;

        PlantModel.MainPlant.line.StartNewLine();

        // set despawn trigger
        HeightCheckManager.Instance.AddWorldHeightListener(this, currentEnd + despawnGap);

        looted = false;
        spawned = true;
        playerInside = false;
    }

    private void OnTrafficZoneEnter()
    {
        Debug.Log("trafficZone Enter");

        BulletSpawner.Instance.SetSpawnTimerMultiplier(bulletSpawnTimeMultiplier);
        BulletSpawner.Instance.SetSpawnTimer();

        if (!looted)
        {
            StarPieceSpawner.Instance.SetIndicatorActive(true);
            StarPieceSpawner.Instance.SetShapeActive(true);
        }

        float midHeight = (currentStart + currentEnd) / 2;

        if(PlantModel.MainPlant.plantHead.transform.position.y < midHeight)
            EventManager.Instance.CallEvent(EventData.ID.TRAFFICZONE_ENTER, new EventData.ParamTrafficzone() { increase = 1, side = Side.BOTTOM });
        else
            EventManager.Instance.CallEvent(EventData.ID.TRAFFICZONE_ENTER, new EventData.ParamTrafficzone() { increase = 1, side = Side.TOP });

        playerInside = true;
    }

    private void OnTrafficZoneExit()
    {
        Debug.Log("trafficZone Exit");

        BulletSpawner.Instance.SetSpawnTimerMultiplier(1);

        playerInside = false;
    }

    private void OnTrafficZoneExitBottom()
    {
        OnTrafficZoneExit();

        StarPieceSpawner.Instance.SetIndicatorActive(false);

        EventManager.Instance.CallEvent(EventData.ID.TRAFFICZONE_EXIT, new EventData.ParamTrafficzone() { increase = 1, side = Side.BOTTOM });
    }

    private void OnTrafficZoneExitTop()
    {
        OnTrafficZoneExit();

        if (StarPieceSpawner.Instance.AllPiecesCollected() && !looted)
        {
            StarPieceSpawner.Instance.ClaimReward();
            StarPieceSpawner.Instance.Deactivate();

            looted = true;
        }

        EventManager.Instance.CallEvent(EventData.ID.TRAFFICZONE_EXIT, new EventData.ParamTrafficzone() { increase = 1, side = Side.TOP });
    }

    private void DespawnTrafficZone(float currentHeight)
    {
        SetAssetsActive(false);

        LockCameraHeight(currentEnd);

        SetColliderHeight(currentEnd);
        //SetCloudBorderHeight(currentHeight + 2);

        BackgroundManager.Instance.DestroyPassed();
        PlantModel.MainPlant.line.DestroyOldLines();

        StarDustManager.Instance.activated = true;
        CritterSpawner.Instance.activated = true;

        StarPieceSpawner.Instance.Deactivate();

        // next traffic zone trigger
        SetSpawnTrigger(currentEnd);

        spawned = false;
    }

    private void SetAssetsActive(bool active)
    {
        beginMarker.SetActive(active);
        endMarker.SetActive(active);
        background.SetActive(active);
    }

    private void RemoveStardustShapesInZone()
    {
        for (int i = StarDustManager.Instance.activeShapes.Count - 1; i >= 0; i--)
        {
            if (StarDustManager.Instance.activeShapes[i] != null && StarDustManager.Instance.activeShapes[i].topLeft.position.y >= currentStart)
            {
                if (!StarDustManager.Instance.activeShapes[i].falling)
                    StarDustManager.Instance.ReturnShapeToPool(StarDustManager.Instance.activeShapes[i]);
            }
        }
    }

    private void LockCameraHeight(float height)
    {
        Camera.main.GetComponent<IngameCamera>().SetLowestBorder(height);
    }

    private void SetCloudBorderHeight(float height)
    {
        cloudBorder.SetActive(true);
        cloudBorder.transform.position = new Vector3(cloudBorder.transform.position.x, height, cloudBorder.transform.position.z);
    }

    private void SetColliderHeight(float height)
    {
        groundCollider.transform.position = new Vector3(0, height, groundCollider.transform.position.z);
    }

    private void UpdateZonePos()
    {
        beginMarker.transform.position = new Vector3(0, currentStart, beginMarker.transform.position.z);
        endMarker.transform.position = new Vector3(0, currentEnd, endMarker.transform.position.z);

        background.transform.position = new Vector3(0, currentStart);
        background.transform.localPosition = new Vector3(background.transform.localPosition.x, background.transform.localPosition.y, 0);
    }
}
