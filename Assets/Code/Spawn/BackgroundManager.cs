﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BackgroundManager : MonoBehaviour, HeightListener
{
    private static BackgroundManager _instance;

    public static BackgroundManager Instance
    {
        get { return _instance; }
    }

    public List<GameObject> passed;

    public GameObject backgroundTile;

    public Vector3 offset;

    public float triggerHeight;

    public int deleteOffset;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
            return;
        }
        else
        {
            _instance = this;
        }

        HeightCheckManager.Instance.AddWorldHeightListener(this, backgroundTile.transform.position.y - triggerHeight);
    }

    public void Spawn()
    {
        Vector3 pos = backgroundTile.transform.position;

        passed.Add(backgroundTile);

        backgroundTile = Instantiate(backgroundTile) as GameObject;

        backgroundTile.name = backgroundTile.name.Split('(')[0];

        backgroundTile.transform.parent = transform;
        backgroundTile.transform.position = pos + offset;
    }

    public void HeightReached(float height)
    {
        Spawn();

        HeightCheckManager.Instance.AddWorldHeightListener(this, backgroundTile.transform.position.y - triggerHeight);
    }

    public void DestroyPassed()
    {
        for (int i = 0; i < passed.Count - deleteOffset; i++)
            Destroy(passed[i]);

        passed = new List<GameObject>();
    }
}
